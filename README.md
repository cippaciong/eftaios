#Escape From The Aliens in Outer Space
***

## How to run the game
* Run the server Main class GameEngine located in package gameengine
* Run the view Main class View located in package view

## Choose network protocol and user interface
Ad soon as the view is started you will be asked to choose either rmi or socket as network protocol and the gui or cli as user interface.

## Gameplay (GUI)
##### Login
The first thing to do in order to play the game is signup for a new user, you can use any username and a password of at least 8 characters.
After the first signup, credentials are saved in a file and you can simply login in future runs.

##### Enter Room
As soon as you have signed up or logged in you can choose the map, clicking on the relative button.
You will be put on wait mode until there are 8 players or at least 2 players and a time of two minutes has passed.

##### Actions
Once the match is started you can play the real game, clicking with che left mouse button on any sector will show a popup containing all the actions you can do.
Each action will take the sector where the mouse event occurred as target sector (eg. Noise in any sector or move).
Clicking with the right mouse button will close the popup.

##### Items
When you find an item, its icon will become white meaning that you can use the item, click on its icon to activate it.
Note: you are supposed to use the sedatives item after you have moved

##### Sidebar
In the sidebar on the right you will find serveral thigs
* A chat area to talk to other players
* The list of all usernames, useful if you want to mark positions
* A log area to see what the other players are doing during their turns
* A list of all charachters playing the current match and their statuses (active, disconnected, dead or escaped)
* The item list.

## Gameplay (CLI)
The gameplay in CLI mode is somehow similar to the GUI gameplay, you still have to signup or login (with signup or login commands) and choose the preferred map with command enterRoom.
Please note that commands composed by multiple words are called *inThisWay*
You can press *Enter* whenever you want to get a list of all the available commands and the commands you can use in that specific moment.

