package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.EnterRoom;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Signup;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class TestEndTurn {

    private Match match;
    private MatchController matchController;
    private User currentUser;
    private User notCurrentUser;
    private Server server = Server.getInstance();
    private Action action;
    private Output output;
    private Launcher l = new Launcher();
    private Message msg;

    @Before
    public void setUp() throws Exception {
        // setUp match by entering 8 users in room
        String userKnwown = "";
        List<String> args;
        for (int i = 0; i < 8; i++) {
            args = new ArrayList<>();
            args.add(UUID.randomUUID().toString());
            args.add("passwordaaaa");
            userKnwown = UUID.randomUUID().toString();
            action = new Signup(userKnwown, args);
            action.execute();
            action = new EnterRoom(userKnwown, "Galilei");
            action.execute();
        }
        matchController = server.getUserIdMatchControllerMap().get(userKnwown);
        match = matchController.getMatch();
        for (User user : match.getMapUserPlayer().keySet()) {
            if (match.getMapUserPlayer().get(user).equals(match.getCurrentPlayer()))
                currentUser = user;
            else
                notCurrentUser = user;
        }
    }

    @Test
    public void testNotCurrentUser() {
        match.getPossibleActions().clear();
        msg = new Message(notCurrentUser.getMyId(), "endturn");
        output = l.launch(msg);
        assertEquals("Questa azione la puoi fare solo quando è il tuo turno", output.getDescriptions().get(0));
    }
    
    @Test
    public void testFakeUser() {
        match.getPossibleActions().clear();
        match.getPossibleActions().add("endturn");
        msg = new Message("Nonesisto", "endturn");
        output = l.launch(msg);
        assertEquals("Azione disponibile solo a partita iniziata!", output.getDescriptions().get(0));
    }

    @Test
    public void testCantEndTurn() {
        match.getPossibleActions().clear();
        msg = new Message(currentUser.getMyId(), "endturn");
        output = l.launch(msg);
        assertEquals("Non puoi passare il turno ora", output.getDescriptions().get(0));
    }
    

    @Test
    public void testSuccessEndTurn() {
        match.getPossibleActions().add("endturn");
        msg = new Message(currentUser.getMyId(), "endturn");
        output = l.launch(msg);
        assertEquals("Turno Passato, aspetta fin quando ritocca a te", output.getDescriptions().get(0));
    }
    
}
