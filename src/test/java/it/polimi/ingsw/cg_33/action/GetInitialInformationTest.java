package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.EnterRoom;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Signup;
import it.polimi.ingsw.cg_33.gameengine.Launcher;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class GetInitialInformationTest {

    String userKnown;
    private Action action;
    private Output output;

    @Before
    public void setUp() throws Exception {
        // setUp match by entering 8 users in room
        List<String> args;
        for (int i = 0; i < 8; i++) {
            args = new ArrayList<>();
            args.add(UUID.randomUUID().toString());
            args.add("passwordaaaa");
            userKnown = UUID.randomUUID().toString();
            action = new Signup(userKnown, args);
            action.execute();
            action = new EnterRoom(userKnown, "Galilei");
            action.execute();
        }
    }

    @Test
    public void test() {
        Launcher l = new Launcher();
        Message msg = new Message(userKnown,"getinitialinformations");
        output = l.launch(msg);
        assertTrue(!output.getCurrentPosition().isEmpty());
        assertTrue(!output.getName().isEmpty());
        assertTrue(!output.getTitle().isEmpty());
        assertTrue(output.getPossibleActions().contains("move"));
        msg = new Message("nonEsisto","getinitialinformations");
        output = l.launch(msg);
        assertTrue(output.getDescriptions().contains("You can't ask me this before starting match!"));
    }

}
