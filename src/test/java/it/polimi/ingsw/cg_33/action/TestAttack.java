package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.EnterRoom;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Signup;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.player.Alien;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class TestAttack {

    private Match match;
    private MatchController matchController;
    private User currentUser;
    private User notCurrentUser;
    private Server server = Server.getInstance();
    private Action action;
    private Output output;
    private Launcher l = new Launcher();
    private Message msg;

    @Before
    public void setUp() throws Exception {
        // setUp match by entering 8 users in room
        String userKnwown = "";
        List<String> args;
        for (int i = 0; i < 8; i++) {
            args = new ArrayList<>();
            args.add(UUID.randomUUID().toString());
            args.add("passwordaaaa");
            userKnwown = UUID.randomUUID().toString();
            action = new Signup(userKnwown, args);
            action.execute();
            action = new EnterRoom(userKnwown, "Galilei");
            action.execute();
        }
        matchController = server.getUserIdMatchControllerMap().get(userKnwown);
        match = matchController.getMatch();
        // save in a var who is the current user and one of the users that is
        // not current user.
        for (User user : match.getMapUserPlayer().keySet()) {
            if (match.getMapUserPlayer().get(user).equals(match.getCurrentPlayer()))
                currentUser = user;
            else
                notCurrentUser = user;
        }
    }

    @Test
    public void testNotCurrentUserAttack() {
        match.getPossibleActions().clear();
        match.getPossibleActions().add("move");
        msg = new Message(notCurrentUser.getMyId(), "attack");
        output = l.launch(msg);
        assertEquals("Questa azione la puoi fare solo quando è il tuo turno", output.getDescriptions().get(0));
    }

    @Test
    public void testAttackBeforeMatch() {
        msg = new Message("nonEsistooo", "attack");
        output = l.launch(msg);
        assertEquals("Azione disponibile solo a partita iniziata!", output.getDescriptions().get(0));
    }

    @Test
    public void testCantAttack() {
        match.getPossibleActions().add("move");
        msg = new Message(currentUser.getMyId(), "attack");
        output = l.launch(msg);
        assertEquals("Non è il momento di attaccare!", output.getDescriptions().get(0));
    }

    @Test
    public void testHumanAlienAttack() {
        for (User u : match.getMapUserPlayer().keySet()) {
            match.setCurrentPlayer(match.getMapUserPlayer().get(u));
            match.getPossibleActions().clear();
            match.getPossibleActions().add("attack");
            msg = new Message(u.getMyId(), "attack");
            output = l.launch(msg);
            if (match.getCurrentPlayer() instanceof Alien)
                assertEquals("Hai attaccato con successo!", output.getDescriptions().get(0));
            else
                assertEquals("Solo gli alieni possono attaccare senza usare oggetti!", output.getDescriptions().get(0));
        }
    }
}
