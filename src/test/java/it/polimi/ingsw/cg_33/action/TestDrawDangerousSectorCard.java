package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.EnterRoom;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Signup;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import it.polimi.ingsw.cg_33.model.card.item.Item;
import it.polimi.ingsw.cg_33.model.card.sectorcard.DangerousSectorCard;
import it.polimi.ingsw.cg_33.model.card.sectorcard.NoiseCard;
import it.polimi.ingsw.cg_33.model.card.sectorcard.NoiseInAnySectorCard;
import it.polimi.ingsw.cg_33.model.card.sectorcard.SilenceCard;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class TestDrawDangerousSectorCard {

    private Match match;
    private MatchController matchController;
    private User currentUser;
    private User notCurrentUser;
    private Server server = Server.getInstance();
    private Action action;
    private Output output;
    private Launcher l = new Launcher();
    private Message msg;

    @Before
    public void setUp() throws Exception {
        // setUp match by entering 8 users in room
        String userKnwown = "";
        List<String> args;
        for (int i = 0; i < 8; i++) {
            args = new ArrayList<>();
            args.add(UUID.randomUUID().toString());
            args.add("passwordaaaa");
            userKnwown = UUID.randomUUID().toString();
            action = new Signup(userKnwown, args);
            action.execute();
            action = new EnterRoom(userKnwown, "Galilei");
            action.execute();
        }
        matchController = server.getUserIdMatchControllerMap().get(userKnwown);
        match = matchController.getMatch();
        for (User user : match.getMapUserPlayer().keySet()) {
            if (match.getMapUserPlayer().get(user).equals(match.getCurrentPlayer()))
                currentUser = user;
            else
                notCurrentUser = user;
        }
    }

    @Test
    public void testNotCurrentUser() {
        msg = new Message(notCurrentUser.getMyId(), "draw");
        output = l.launch(msg);
        assertEquals("Questa azione la puoi fare solo quando è il tuo turno", output.getDescriptions().get(0));
    }

    @Test
    public void testFakeUser() {
        msg = new Message("nonEsisto", "draw");
        output = l.launch(msg);
        assertEquals("Azione disponibile solo a partita iniziata!", output.getDescriptions().get(0));
    }

    @Test
    public void testCantDraw() {
        match.getPossibleActions().clear();
        match.getPossibleActions().add("move");
        msg = new Message(currentUser.getMyId(), "draw");
        output = l.launch(msg);
        assertEquals("Non puoi pescare ora!", output.getDescriptions().get(0));
    }

    @Test
    public void testDrawSuccess() {
        for (int i = 0; i < 100; i++) {
            match.getPossibleActions().clear();
            match.getPossibleActions().add("draw");
            msg = new Message(currentUser.getMyId(), "draw");
            output = l.launch(msg);
            DangerousSectorCard c = (DangerousSectorCard) match.getDiscardedCards().get(i);
            if (c instanceof NoiseCard)
                assertTrue(output.getDescriptions().contains("Solo io e te sappiamo che hai fatto rumore nel tuo settore, ora non ti resta che prendere tempo" + "e dirmi quando vuoi che gli altri sappiano la tua posizione, ricorda il tempismo è fondamentale"));
            else if (c instanceof NoiseInAnySectorCard)
                assertTrue(output.getDescriptions().contains("Ehi, hai trovato un bullone, puoi lanciarlo e fare rumore dove vuoi," + "mi raccomando dimmi le coordinate del settore in cui vuoi far rumore prima" + "dello scadere del tempo, altrimenti potrei dire a tutti dove ti trovi"));
            else if (c instanceof SilenceCard)
                assertTrue(output.getDescriptions().contains("Bravo non hai fatto rumore, gli altri non ti hanno notato"));
            if (c.getHasItem()) {
                Item item = match.getMapUserPlayer().get(currentUser).getItems().get(0);
                match.getMapUserPlayer().get(currentUser).getItems().clear();
                match.getDiscardedItems().add(item);
            }
        }
    }
}
