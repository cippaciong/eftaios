package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.EnterRoom;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Signup;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import it.polimi.ingsw.cg_33.model.card.item.Attack;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class TestDiscardItem {

    private Match match;
    private MatchController matchController;
    private Server server = Server.getInstance();
    private Action action;
    private Output output;
    private Launcher l = new Launcher();
    private Message msg;
    private User currentUser;
    private User notCurrentUser;

    @Before
    public void setUp() throws Exception {
        // setUp match by entering 8 users in room
        String userKnwown = "";
        List<String> args;
        for (int i = 0; i < 8; i++) {
            args = new ArrayList<>();
            args.add(UUID.randomUUID().toString());
            args.add("passwordaaaa");
            userKnwown = UUID.randomUUID().toString();
            action = new Signup(userKnwown, args);
            action.execute();
            action = new EnterRoom(userKnwown, "Galilei");
            action.execute();
        }
        matchController = server.getUserIdMatchControllerMap().get(userKnwown);
        match = matchController.getMatch();
        // save in a var who is the current user and one of the users that is
        // not current user.
        for (User user : match.getMapUserPlayer().keySet()) {
            if (match.getMapUserPlayer().get(user).equals(match.getCurrentPlayer()))
                currentUser = user;
            else
                notCurrentUser = user;
        }
    }

    @Test
    public void testFakeUser() {
        msg = new Message("nonEsisto", "discarditem", Collections.singletonList("attack"));
        output = l.launch(msg);
        assertEquals("Azione disponibile solo a partita iniziata!", output.getDescriptions().get(0));
    }

    @Test
    public void testNotCurrentUser() {
        msg = new Message(notCurrentUser.getMyId(), "discarditem", Collections.singletonList("attack"));
        output = l.launch(msg);
        assertEquals("Questa azione la puoi fare solo quando è il tuo turno", output.getDescriptions().get(0));
    }
    
    @Test
    public void testCantDiscard() {
        match.getMapUserPlayer().get(currentUser).getItems().clear();
        match.getMapUserPlayer().get(currentUser).getItems().add(new Attack());
        match.getPossibleActions().clear();
        match.getPossibleActions().add("move");
        msg = new Message(currentUser.getMyId(), "discarditem", Collections.singletonList("attack"));
        output = l.launch(msg);
        assertEquals("Non è questo il momento di scartare un oggetto!", output.getDescriptions().get(0));
    }
    
    @Test
    public void testFakeItem() {
        match.getPossibleActions().add("discarditem");
        msg = new Message(currentUser.getMyId(), "discarditem", Collections.singletonList("asdfsf"));
        output = l.launch(msg);
        assertEquals("Non possiedi questo oggetto!", output.getDescriptions().get(0));
    }
    
    @Test
    public void testDontHaveItem() {
        match.getPossibleActions().add("discarditem");
        msg = new Message(currentUser.getMyId(), "discarditem", Collections.singletonList("teleport"));
        output = l.launch(msg);
        assertEquals("Non possiedi questo oggetto!", output.getDescriptions().get(0));
    }
    
    @Test
    public void testDiscardSuccess() {
        match.getMapUserPlayer().get(currentUser).getItems().clear();
        match.getMapUserPlayer().get(currentUser).getItems().add(new Attack());
        match.getPossibleActions().add("discarditem");
        msg = new Message(currentUser.getMyId(), "discarditem", Collections.singletonList("attack"));
        output = l.launch(msg);
        assertEquals("Oggetto scartato con successo!", output.getDescriptions().get(0));
        assertTrue(match.getMapUserPlayer().get(currentUser).getItems().isEmpty());
    }

}
