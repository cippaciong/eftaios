package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.gameengine.Launcher;

import org.junit.Test;

public class LauncherTest {

    @Test
    public void test() {
        Launcher l = new Launcher();
        Message msg = new Message(null, "nonEsiste");
        assertTrue(l.launch(msg).getDescriptions().contains("Undefined Action"));
    }
}
