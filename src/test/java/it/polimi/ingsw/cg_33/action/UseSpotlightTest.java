package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.EnterRoom;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Signup;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import it.polimi.ingsw.cg_33.model.card.item.Spotlight;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.player.Alien;
import it.polimi.ingsw.cg_33.model.player.Human;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class UseSpotlightTest {

    private Match match;
    private MatchController matchController;
    private User currentUser;
    private User notCurrentUser;
    private Server server = Server.getInstance();
    private Action action;
    private Output output;
    Launcher l = new Launcher();
    Message msg;
    List<String> args = Collections.singletonList("0-1");

    @Before
    public void setUp() throws Exception {
        // setUp match by entering 8 users in room
        String userKnwown = "";
        List<String> args;
        for (int i = 0; i < 8; i++) {
            args = new ArrayList<>();
            args.add(UUID.randomUUID().toString());
            args.add("passwordaaaa");
            userKnwown = UUID.randomUUID().toString();
            action = new Signup(userKnwown, args);
            action.execute();
            action = new EnterRoom(userKnwown, "Galilei");
            action.execute();
        }
        matchController = server.getUserIdMatchControllerMap().get(userKnwown);
        match = matchController.getMatch();
        // save in a var who is the current user and one of the users that is
        // not current user.
        for (User user : match.getMapUserPlayer().keySet()) {
            if (match.getMapUserPlayer().get(user).equals(match.getCurrentPlayer()))
                currentUser = user;
            else
                notCurrentUser = user;
        }
    }

    @Test
    public void testUseSpotlightSuccess() {
        for (User u : match.getMapUserPlayer().keySet()) {
            currentUser = u;
            match.setCurrentPlayer(match.getMapUserPlayer().get(currentUser));
            match.getCurrentPlayer().getItems().clear();
            match.getCurrentPlayer().addItem(new Spotlight());
            msg = new Message(currentUser.getMyId(), "usespotlight", args);
            output = l.launch(msg);
            if (match.getCurrentPlayer() instanceof Alien)
                assertTrue(output.getDescriptions().contains("Gli alieni non possono usare oggetti!"));
            else
                assertTrue(output.getDescriptions().contains("Ok la torcia è in azione, a breve scopriremo chi hai avvistato!"));
        }
    }

    @Test
    public void testNotCurrentUser() {
        msg = new Message(notCurrentUser.getMyId(), "usespotlight", args);
        output = l.launch(msg);
        assertEquals("Questa azione la puoi fare solo quando è il tuo turno", output.getDescriptions().get(0));
    }

    @Test
    public void testFakeUser() {
        msg = new Message("nonEsisto", "usespotlight", args);
        output = l.launch(msg);
        assertEquals("Azione disponibile solo a partita iniziata!", output.getDescriptions().get(0));
    }

    @Test
    public void testDontHaveSpotlight() {
        match.getCurrentPlayer().getItems().clear();
        msg = new Message(currentUser.getMyId(), "usespotlight", args);
        output = l.launch(msg);
        assertTrue(output.getDescriptions().contains("Non possiedi questo oggetto!"));
    }

    @Test
    public void testErrorFormatNumber() {
        for (User u : match.getMapUserPlayer().keySet()) {
            currentUser = u;
            match.setCurrentPlayer(match.getMapUserPlayer().get(currentUser));
            match.getCurrentPlayer().getItems().clear();
            match.getCurrentPlayer().addItem(new Spotlight());
            msg = new Message(currentUser.getMyId(), "move", Collections.singletonList("aaa"));
            output = l.launch(msg);
            if (match.getCurrentPlayer() instanceof Human)
                assertEquals("Devi inserire il targeti nel formato x-y dove x e y sono due interi", output.getDescriptions().get(0));
        }
    }

}