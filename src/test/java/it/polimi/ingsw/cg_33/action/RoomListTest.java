package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.gameengine.Launcher;

import org.junit.Test;

public class RoomListTest {

    @Test
    public void test() {
        Launcher l = new Launcher();
        Message msg = new Message("useriiiid", "roomlist");
        Output output = l.launch(msg);
        assertTrue(!output.getDescriptions().isEmpty());
    }
}
