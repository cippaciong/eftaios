package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.controller.match.RoomController;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import it.polimi.ingsw.cg_33.model.player.PlayerState;
import it.polimi.ingsw.cg_33.model.server.Server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class LoginTest {

    private String userId;
    List<String> args = new ArrayList<>();
    Output output;
    Launcher l = new Launcher();
    Message msg;

    @Before
    public void setUp() {
        userId = UUID.randomUUID().toString();
        args.add(userId);
        args.add("password");
        msg = new Message(userId, "signup", args);
        output = l.launch(msg);
        Server.getInstance().getClientidUsernameMap().remove(userId);
    }

    @Test
    public void testInvalidUsername() {
        msg = new Message(UUID.randomUUID().toString(), "login", null);
        output = l.launch(msg);
        assertTrue(!output.isLoginSuccess());
        assertTrue(output.getLoginDescription().equals("Invalid username! Sign up and then try again"));
    }

    @Test
    public void testWrongPassword() {
        ArrayList<String> wrong = new ArrayList<>();
        wrong.add(userId);
        wrong.add("passSbagliata");
        msg = new Message(userId, "login", wrong);
        output = l.launch(msg);
        assertTrue(!output.isLoginSuccess());
        assertTrue(output.getLoginDescription().equals("Wrong Password"));
    }

    @Test
    public void testLoginComplete() {
        msg = new Message(userId, "login", args);
        output = l.launch(msg);
        assertTrue(output.isLoginSuccess());
        assertTrue(output.getLoginDescription().equals("Login Complete!"));
    }

    @Test
    public void testLoginAgain() {
        msg = new Message(userId, "login", args);
        output = l.launch(msg);
        msg = new Message(userId, "login", args);
        output = l.launch(msg);
        assertTrue(output.getLoginDescription().equals("You're logged in again now, nothing has changed!"));
    }

    @Test
    public void testReEnterRoom() {
        msg = new Message(userId, "login", args);
        output = l.launch(msg);
        msg = new Message(userId, "enterroom", Collections.singletonList("Fermi"));
        output = l.launch(msg);
        msg = new Message(userId, "login", args);
        output = l.launch(msg);
        assertTrue(output.getLoginDescription().equals("You're now in your old room"));
    }

    @Test
    public void testLoginTwiceMatch() {
        msg = new Message(userId, "login", args);
        output = l.launch(msg);
        msg = new Message(userId, "enterroom", Collections.singletonList("Fermi"));
        output = l.launch(msg);
        RoomController.getInstance().startNewMatch("Fermi");
        RoomController.getInstance().cleanRoom("Fermi");
        msg = new Message(userId, "login", args);
        output = l.launch(msg);
        assertTrue(output.getLoginDescription().equals("You can't login twince, keep playing instead!"));
    }

    @Test
    public void testResumeMatch() {
        msg = new Message(userId, "login", args);
        output = l.launch(msg);
        msg = new Message(userId, "enterroom", Collections.singletonList("Galvani"));
        output = l.launch(msg);
        RoomController.getInstance().startNewMatch("Galvani");
        RoomController.getInstance().cleanRoom("Galvani");
        MatchController m = Server.getInstance().getUserIdMatchControllerMap().get(userId);
        m.getMatch().getCurrentPlayer().setPlayerState(PlayerState.INACTIVE);
        msg = new Message(userId, "login", args);
        output = l.launch(msg);
        assertTrue(output.getLoginDescription().equals("You're going to resume your match!"));
    }
}
