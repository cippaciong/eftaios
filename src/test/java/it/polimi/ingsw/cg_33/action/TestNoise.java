package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.EnterRoom;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Signup;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class TestNoise {

    private Match match;
    private MatchController matchController;
    private User currentUser;
    private User notCurrentUser;
    private Server server = Server.getInstance();
    private Action action;
    private Output output;
    Launcher l = new Launcher();
    Message msg;

    @Before
    public void setUp() throws Exception {
        // setUp match by entering 8 users in room
        String userKnwown = "";
        List<String> args;
        for (int i = 0; i < 8; i++) {
            args = new ArrayList<>();
            args.add(UUID.randomUUID().toString());
            args.add("passwordaaaa");
            userKnwown = UUID.randomUUID().toString();
            action = new Signup(userKnwown, args);
            action.execute();
            action = new EnterRoom(userKnwown, "Galilei");
            action.execute();
        }
        matchController = server.getUserIdMatchControllerMap().get(userKnwown);
        match = matchController.getMatch();
        for (User user : match.getMapUserPlayer().keySet()) {
            if (match.getMapUserPlayer().get(user).equals(match.getCurrentPlayer()))
                currentUser = user;
            else
                notCurrentUser = user;
        }
    }

    @Test
    public void testNotCurrentUser() {
        msg = new Message(notCurrentUser.getMyId(), "noise");
        output = l.launch(msg);
        assertEquals("Questa azione la puoi fare solo quando è il tuo turno", output.getDescriptions().get(0));
    }

    @Test
    public void testFakeUser() {
        msg = new Message("nonEsisto", "noise");
        output = l.launch(msg);
        assertEquals("Azione disponibile solo a partita iniziata!", output.getDescriptions().get(0));
    }

    @Test
    public void testCantNoise() {
        match.getPossibleActions().clear();
        msg = new Message(currentUser.getMyId(), "noise");
        output = l.launch(msg);
        assertEquals("Non puoi fare rumore ora!", output.getDescriptions().get(0));
    }

    @Test
    public void testNoiseSuccess() {
        match.getPossibleActions().clear();
        match.getPossibleActions().add("noise");
        msg = new Message(currentUser.getMyId(), "noise");
        output = l.launch(msg);
        assertEquals("Speriamo che nessuno ci creda!", output.getDescriptions().get(0));
    }

}
