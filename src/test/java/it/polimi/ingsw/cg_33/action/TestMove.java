package it.polimi.ingsw.cg_33.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.EnterRoom;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Signup;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import it.polimi.ingsw.cg_33.model.map.GameMap;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.player.Alien;
import it.polimi.ingsw.cg_33.model.player.Player;
import it.polimi.ingsw.cg_33.model.player.PlayerState;
import it.polimi.ingsw.cg_33.model.sector.AlienStartingSector;
import it.polimi.ingsw.cg_33.model.sector.EscapeHatchSector;
import it.polimi.ingsw.cg_33.model.sector.HumanStartingSector;
import it.polimi.ingsw.cg_33.model.sector.Sector;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class TestMove {

    private Match match;
    private MatchController matchController;
    private User currentUser;
    private User notCurrentUser;
    private Server server = Server.getInstance();
    private Action action;
    List<String> moveArgs = Collections.singletonList("0-1");
    Output output;
    Launcher l = new Launcher();
    Message msg;

    @Before
    public void setUp() throws Exception {
        // setUp match by entering 8 users in room
        String userKnwown = "";
        List<String> args;
        for (int i = 0; i < 8; i++) {
            args = new ArrayList<>();
            args.add(UUID.randomUUID().toString());
            args.add("passwordaaaa");
            userKnwown = UUID.randomUUID().toString();
            action = new Signup(userKnwown, args);
            action.execute();
            action = new EnterRoom(userKnwown, "Galilei");
            action.execute();
        }
        matchController = server.getUserIdMatchControllerMap().get(userKnwown);
        match = matchController.getMatch();
        for (User user : match.getMapUserPlayer().keySet()) {
            if (match.getMapUserPlayer().get(user).equals(match.getCurrentPlayer()))
                currentUser = user;
            else
                notCurrentUser = user;
        }
    }

    @Test
    public void testMove() {
        for (User u : match.getMapUserPlayer().keySet()) {
            Player p = match.getMapUserPlayer().get(u);
            match.setCurrentPlayer(p);
            match.getPossibleActions().clear();
            match.getPossibleActions().add("move");
            Sector near = null;
            for (Sector s : match.getMap().sectorNeighbors(p.getPosition()))
                near = s;
            msg = new Message(u.getMyId(), "move", Collections.singletonList(near.toString()));
            output = l.launch(msg);
            assertEquals(p.getPosition().toString(), output.getCurrentPosition());
            if (p instanceof Alien)
                assertTrue(output.getPossibleActions().contains("attack"));
        }
    }

    @Test
    public void testNotCurrentUser() {
        msg = new Message(notCurrentUser.getMyId(), "move", moveArgs);
        output = l.launch(msg);
        assertEquals("Questa azione la puoi fare solo quando è il tuo turno", output.getDescriptions().get(0));
    }

    @Test
    public void testFakeUser() {
        msg = new Message("nonEsisto", "move", moveArgs);
        output = l.launch(msg);
        assertEquals("Azione disponibile solo a partita iniziata!", output.getDescriptions().get(0));
    }

    @Test
    public void testAlreadyMoved() {
        match.getPossibleActions().clear();
        msg = new Message(currentUser.getMyId(), "move", moveArgs);
        output = l.launch(msg);
        assertEquals("You can't move now! Remember, only one move for each turn", output.getDescriptions().get(0));
    }

    @Test
    public void testErrorFormatNumber() {
        match.getPossibleActions().clear();
        match.getPossibleActions().add("move");
        msg = new Message(currentUser.getMyId(), "move", Collections.singletonList("aaa"));
        output = l.launch(msg);
        assertEquals("Devi inserire il targeti nel formato x-y dove x e y sono due interi", output.getDescriptions().get(0));
    }

    @Test
    public void testCantReach() {
        match.getPossibleActions().clear();
        match.getPossibleActions().add("move");
        msg = new Message(currentUser.getMyId(), "move", moveArgs);
        output = l.launch(msg);
        assertEquals("Can't reach destination!", output.getDescriptions().get(0));
    }

    @Test
    public void testUnknownSector() {
        match.getPossibleActions().clear();
        match.getPossibleActions().add("move");
        msg = new Message(currentUser.getMyId(), "move", Collections.singletonList("1000-10000"));
        output = l.launch(msg);
        assertEquals("This map haven't this sector", output.getDescriptions().get(0));
    }

    @Test
    public void testSamePosition() {
        match.getPossibleActions().clear();
        match.getPossibleActions().add("move");
        Player p = match.getMapUserPlayer().get(currentUser);
        msg = new Message(currentUser.getMyId(), "move", Collections.singletonList(p.getPosition().toString()));
        output = l.launch(msg);
        assertEquals("You're already there, choose another destination", output.getDescriptions().get(0));
    }

    @Test
    public void testMoveOnStartingSector() {
        match.getPossibleActions().clear();
        match.getPossibleActions().add("move");
        Player p = match.getMapUserPlayer().get(currentUser);
        GameMap map = match.getMap();
        Sector near = null;
        for (Sector s : map.getMap().values())
            if (s instanceof AlienStartingSector || s instanceof HumanStartingSector) {
                for (Sector sector : map.sectorNeighbors(s))
                    near = sector;
                p.setPosition(near);
                msg = new Message(currentUser.getMyId(), "move", Collections.singletonList(s.toString()));
                output = l.launch(msg);
                assertEquals("You can't move on a starting sector", output.getDescriptions().get(0));
            }
    }

    @Test
    public void testMoveOnHatch() {
        for (User u : match.getMapUserPlayer().keySet()) {
            Player p = match.getMapUserPlayer().get(u);
            match.setCurrentPlayer(p);
            GameMap map = match.getMap();
            Sector near = null;
            for (Sector s : map.getMap().values())
                if (s instanceof EscapeHatchSector) {
                    p.setPlayerState(PlayerState.ACTIVE);
                    match.getPossibleActions().clear();
                    match.getPossibleActions().add("move");
                    for (Sector sector : map.sectorNeighbors(s))
                        near = sector;
                    p.setPosition(near);
                    EscapeHatchSector e = (EscapeHatchSector) s;
                    boolean active = e.isActive();
                    msg = new Message(u.getMyId(), "move", Collections.singletonList(s.toString()));
                    output = l.launch(msg);
                    if (p instanceof Alien) {
                        assertEquals("Aliens can't use escape hatches!", output.getDescriptions().get(0));
                    } else if (active) {
                        assertTrue(output.getDescriptions().contains("Sei su una scialuppa!"));
                        if (p.getPlayerState().equals(PlayerState.ESCAPED))
                            assertTrue(output.getDescriptions().contains("You're lucky, this hatch is working! Go home and hug your family"));
                        else
                            assertTrue(output.getDescriptions().contains("Damn! This hatch doesn't work!"));

                    } else if (!active)
                        assertTrue(output.getDescriptions().contains("Non sei il primo ad arrivare alla scialuppa, mi spiace effettua una mossa diversa!"));
                }
        }
    }

}
