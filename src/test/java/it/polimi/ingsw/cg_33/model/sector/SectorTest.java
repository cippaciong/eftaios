package it.polimi.ingsw.cg_33.model.sector;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.model.map.Cube;

import org.junit.Before;
import org.junit.Test;

public class SectorTest {

    private Sector oddSector;
    private Sector evenSector;

    @Before
    public void setUp() throws Exception {
        oddSector = new SecureSector(3, 2);
        evenSector = new SecureSector(2, 4);
    }

    @Test
    public void testOffsetToCubeConversion() {
        Cube oddCube = new Cube(3, -4, 1);
        Cube evenCube = new Cube(2, -5, 3);
        assertTrue("oddSector isn't equal do oddCube",oddCube.equals(oddSector.toCube()));
        assertTrue("evenSector isn't equal to evenCube",evenCube.equals(evenSector.toCube()));
    }
}
