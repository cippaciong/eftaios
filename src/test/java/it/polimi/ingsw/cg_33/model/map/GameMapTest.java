package it.polimi.ingsw.cg_33.model.map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.model.sector.Sector;

import java.awt.Point;

import org.junit.BeforeClass;
import org.junit.Test;

public class GameMapTest {

    static GameMap map;
    
    @BeforeClass
    public static void setUpClass() {
        map = new GameMap("galilei");
    }

    @Test
    public void pickASectorTest() {
        Point key = new Point(3, 4);
        Sector sector = map.getSector(key);
        assertTrue(sector instanceof Sector);
    }
    
    @Test
    public void distanceOne() {
        assertFalse(map.isDistanceReachable(1, map.getSector(new Point(2, 3)),
                map.getSector(new Point(4, 4))));
       assertTrue(map.isDistanceReachable(1, map.getSector(new Point(0, 1)),
                map.getSector(new Point(1, 1))));
    }

    @Test
    public void distanceTwo() {
        assertFalse(map.isDistanceReachable(2, map.getSector(new Point(2, 3)),
                map.getSector(new Point(4, 4))));
        assertFalse(map.isDistanceReachable(2, map.getSector(new Point(0, 0)),
                map.getSector(new Point(4, 4))));
       assertTrue(map.isDistanceReachable(2, map.getSector(new Point(0, 0)),
                map.getSector(new Point(1, 1))));
       assertTrue(map.isDistanceReachable(2, map.getSector(new Point(1, 1)),
                map.getSector(new Point(3, 1))));
    }
    
    @Test
    public void distanceThree() {
        assertFalse(map.isDistanceReachable(3, map.getSector(new Point(6, 4)),
                map.getSector(new Point(9, 5))));
        assertFalse(map.isDistanceReachable(3, map.getSector(new Point(0, 0)),
                map.getSector(new Point(4, 4))));
       assertTrue(map.isDistanceReachable(3, map.getSector(new Point(0, 0)),
                map.getSector(new Point(0, 3))));
       assertTrue(map.isDistanceReachable(3, map.getSector(new Point(1, 1)),
                map.getSector(new Point(3, 2))));
    }

}
