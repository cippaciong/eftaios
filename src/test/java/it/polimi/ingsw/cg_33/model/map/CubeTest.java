package it.polimi.ingsw.cg_33.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Point;

import org.junit.Before;
import org.junit.Test;

public class CubeTest {

    Cube cubeA, cubeB, cubeC;
    @Before
    public void setUp() throws Exception {
        cubeA = new Cube(2, 2, -4);
        cubeB = new Cube(3, -4, 1);
        cubeC = new Cube(2, -5, 3);
    }

    @Test
    public void testDistance() {
        assertEquals(cubeA.distance(cubeB),6);
        assertEquals(cubeA.distance(cubeC),7);
    }
    
    @Test
    public void testSum() {
        assertTrue(cubeA.add(cubeB).equals(new Cube(5, -2, -3)));
    }
    
    @Test
    public void testCubeToOffsetConversion() {
        assertTrue(cubeA.toOffset().equals(new Point(2, -3)));
    }
    
    @Test
    public void testEquals() {
        Cube cube1, cube2, cube3, cube4, cube5, nullCube;
        Point point = new Point(1,1);
        cube1 = new Cube (1, 3, -4);
        cube2 = cube1;
        cube3 = new Cube (1, 3, -4);
        cube4 = new Cube (2, -2, 0);
        cube5 = new Cube(1, -3, 2);
        nullCube = null;
        assertTrue(cube1.equals(cube2));
        assertTrue(cube1.equals(cube3));
        assertTrue(cube2.equals(cube2));
        assertFalse(cube1.equals(cube4));
        assertFalse(cube1.equals(cube5));
        assertFalse(cube1.equals(nullCube));
        assertFalse(cube1.equals(point));
    }

}
