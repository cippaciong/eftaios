package it.polimi.ingsw.cg_33;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_33.model.map.GameMap;
import it.polimi.ingsw.cg_33.model.sector.AlienStartingSector;
import it.polimi.ingsw.cg_33.model.sector.DangerousSector;
import it.polimi.ingsw.cg_33.model.sector.EmptySector;
import it.polimi.ingsw.cg_33.model.sector.EscapeHatchSector;
import it.polimi.ingsw.cg_33.model.sector.HumanStartingSector;
import it.polimi.ingsw.cg_33.model.sector.SecureSector;

import java.awt.Point;

import org.junit.BeforeClass;
import org.junit.Test;

public class GetPixelColorTest {
    
    private static GameMap map;

    @BeforeClass
    public static void setUpClass() {
    map = new GameMap("Galilei");
    }

    @Test
    public void test() {
        assertTrue(map.getSector(new Point(1, 1)) instanceof EscapeHatchSector);
        assertTrue(map.getSector(new Point(7, 2)) instanceof SecureSector);
        assertTrue(map.getSector(new Point(6, 4)) instanceof DangerousSector);
        assertTrue(map.getSector(new Point(11, 5)) instanceof AlienStartingSector);
        assertTrue(map.getSector(new Point(11, 7)) instanceof HumanStartingSector);
        assertTrue(map.getSector(new Point(3, 3)) instanceof EmptySector);
    }

}
