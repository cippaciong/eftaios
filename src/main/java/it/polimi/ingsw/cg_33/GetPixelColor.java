package it.polimi.ingsw.cg_33;

import it.polimi.ingsw.cg_33.model.sector.AlienStartingSector;
import it.polimi.ingsw.cg_33.model.sector.DangerousSector;
import it.polimi.ingsw.cg_33.model.sector.EmptySector;
import it.polimi.ingsw.cg_33.model.sector.EscapeHatchSector;
import it.polimi.ingsw.cg_33.model.sector.HumanStartingSector;
import it.polimi.ingsw.cg_33.model.sector.Sector;
import it.polimi.ingsw.cg_33.model.sector.SecureSector;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * GetPixelColor is an helper class with the sole purpose of converting png maps
 * into game maps with the {@link #generateMapFromImage(String) generateMapFromImage} method.
 * Map images shold be lowly compressed PNGs with a specific color pattern:
 * <ul>
 * <li> Grey for dangerous sectors
 * <li> White for secure sectors
 * <li> Red for empty sector
 * <li> blue for human start sectors
 * <li> green for alien start sectors
 * <li> black for escape hatch sectors
 * </ul>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class GetPixelColor {

    private static final Logger LOGGER = Logger.getLogger(GetPixelColor.class
            .getName());
    private static final String FS = File.separator;
    
    private GetPixelColor() {

    }

    /**
     * Takes a map name as input, loads its map image and generates a HashMap
     * based on pixel colors
     *
     * @param  mapName the name of the map to convert
     * @return the game map in HashMap format
     */
    public static Map<Point, Sector> generateMapFromImage(String mapName)
            throws IOException {

        File file1 = null;

        switch (mapName.toLowerCase()) {
        case "galilei":
            file1 = new File("src"+FS+"main"+FS+"resources"+FS+"galilei.png");
            break;
        case "galvani":
            file1 = new File("src"+FS+"main"+FS+"resources"+FS+"galvani.png");
            break;
        case "fermi":
            file1 = new File("src"+FS+"main"+FS+"resources"+FS+"fermi.png");
            break;
        default:
            LOGGER.warning("Unknown map name, please make sure you selected an exixting map");

        }

        Map<Point, Sector> mapFromImage = new HashMap<Point, Sector>();
        try {
            BufferedImage image1 = ImageIO.read(file1);

            for (int x = 65; x < image1.getWidth(); x += 93) {
                for (int y = 82; y < image1.getHeight(); y += 109) {

                    int col = (x - 65) / 93;
                    int row = (y - 82) / 109;
                    int pixelColor = image1.getRGB(x, y);
                    Color color = new Color(pixelColor);

                    mapFromImage.put(new Point(col, row),
                            GetPixelColor.sectorFromColor(color, col, row));

                }
            }
        } catch (IOException e) {
            LOGGER.log(
                    Level.SEVERE,
                    "Error reading the provided image, could not generate the map.",
                    e);
        }

        return mapFromImage;
    }

    private static Sector sectorFromColor(Color color, int col, int row) {
        if (isGrey(color))
            return new DangerousSector(col, row);
        else if (isWhite(color))
            return new SecureSector(col, row);
        else if (isRed(color))
            return new EmptySector(col, row);
        else if (isGreen(color))
            return new AlienStartingSector(col, row);
        else if (isBlue(color))
            return new HumanStartingSector(col, row);
        else
            return new EscapeHatchSector(col, row);
    }

    private static boolean isRed(Color color) {
        return color.getRed() == 255 && color.getGreen() == 0
                && color.getBlue() == 0;
    }

    private static boolean isWhite(Color color) {
        return color.getRed() == 255 && color.getGreen() == 255
                && color.getBlue() == 255;
    }

    private static boolean isGrey(Color color) {
        return color.getRed() == 209 && color.getGreen() == 210
                && color.getBlue() == 212;
    }

    private static boolean isBlue(Color color) {
        return color.getRed() == 0 && color.getGreen() == 0
                && color.getBlue() == 255;
    }

    private static boolean isGreen(Color color) {
        return color.getRed() == 0 && color.getGreen() == 255
                && color.getBlue() == 0;
    }
}