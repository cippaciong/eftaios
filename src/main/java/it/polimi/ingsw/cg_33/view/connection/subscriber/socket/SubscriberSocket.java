package it.polimi.ingsw.cg_33.view.connection.subscriber.socket;

import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.view.connection.subscriber.Subscribe;
import it.polimi.ingsw.cg_33.view.input.Input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

public class SubscriberSocket extends Thread implements Subscribe {
    private Socket subSocket;
    private BufferedReader in;
    private static final String ADDRESS = "localhost";
    private static final int PORT = 7778;
    private static final Logger LOGGER = Logger
            .getLogger(SubscriberSocket.class.getName());
    private Input input;

    /**
     * Non appena il thread viene instanziato, ci si sottoscrive al broker. NB.
     * Non é stato implementato il concetto di topic; questo viene lasciato come
     * compito agli studenti.
     * 
     * @param id
     *            L'id assegnato manualmente al thread.
     * @param input
     */
    public SubscriberSocket(Input input) {
        this.input = input;
    }

    /**
     * Dopo aver effettuato la sottoscrizione, questo metodo rimane in ascolto
     * di messaggi da parte del publisher.
     */
    @Override
    public void run() {
        while (true) {
            receive();
            try {
                // aspetta 5ms per ridurre i cicli di clock
                // soprattutto nel caso in cui il publisher vada in crash
                Thread.sleep(5);
            } catch (InterruptedException e) {
                LOGGER.log(
                        Level.WARNING,
                        "thread has been interrupted either before or during the activity",
                        e);
            }

        }
    }

    /**
     * Metodo che riceve eventuali messaggi di testo dal publisher
     * 
     * @return
     */
    private String receive() {
        String msg = null;
        try {
            msg = in.readLine();
            if (msg != null) {
                Gson gson = new Gson();
                ToAllOutput toAllOutput = gson.fromJson(msg, ToAllOutput.class);
                input.dispatchToAllOutput(toAllOutput);
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING,
                    "Error receiving message on socket subscriber", e);
        }
        return msg;
    }

    /**
     * Effettua la sottoscrizione al solo ed unico topic, i.e., crea la socket
     * verso il subscriber e apre uno stream in ingresso per ricevere i messaggi
     * del publisher. NB. Non é necessario creare uno stream in uscita, in
     * ottemperanza al pattern.
     * 
     * @throws UnknownHostException
     * @throws IOException
     */
    @Override
    public void subscribe(String topic) {
        try {
            subSocket = new Socket(ADDRESS, PORT);
            in = new BufferedReader(new InputStreamReader(
                    subSocket.getInputStream()));
            PrintWriter out = new PrintWriter(subSocket.getOutputStream(), true);
            out.println(topic);
            subSocket.shutdownOutput();
            this.start();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Can't subscribe!", e);
        }
    }
}
