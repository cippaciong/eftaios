package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.view.gui.helpers.LoaderSVG;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class ActionPopup extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private List<String> actionNames;
    private Set<String> possibleActions;
    private Map<String, ActionPopupButton> actionButtons;
    private Image image;
    private float imageSize = GameBoard.getWindowHeight() * 0.06f;
    private static final String FS = File.separator;
    private static final String BASEPATH = "src" + FS + "main" + FS + "resources" + FS
            + "swing" + FS + "actions" + FS;

    public ActionPopup() {
        loadComponentSettings();
        loadActionButtons();
    }

    private void loadActionButtons() {
        actionNames = new LinkedList<String>();
        actionNames.add("mark");
        actionNames.add("move");
        actionNames.add("attack");
        actionNames.add("draw");
        actionNames.add("noiseinanysector");
        actionNames.add("noise");
        actionNames.add("discarditem");
        actionNames.add("endturn");
        actionButtons = new LinkedHashMap<String, ActionPopupButton>();
        for (String action : actionNames) {
            image = LoaderSVG.loadSVGFromFile(BASEPATH + action + "_no.svg",
                    imageSize, imageSize);
            Icon buttonIcon = new ImageIcon(image);
            actionButtons
                    .put(action, new ActionPopupButton(action, buttonIcon));
        }

        for (ActionPopupButton button : actionButtons.values()) {
            add(button);
        }

    }

    public void reloadActionIcon() {
        for (Entry<String, ActionPopupButton> buttonEntry : actionButtons
                .entrySet()) {
            if (possibleActions.contains(buttonEntry.getKey())) {
                image = LoaderSVG.loadSVGFromFile(
                        BASEPATH + buttonEntry.getKey() + ".svg", imageSize,
                        imageSize);
            } else {
                image = LoaderSVG.loadSVGFromFile(
                        BASEPATH + buttonEntry.getKey() + "_no.svg", imageSize,
                        imageSize);
            }
            Icon buttonIcon = new ImageIcon(image);
            buttonEntry.getValue().setIcon(buttonIcon);
        }
    }

    private void loadComponentSettings() {
        setLayout(new GridLayout(2, 0));
        setForeground(Color.WHITE);
        setBackground(new Color(0, 0, 0, 200));
        setOpaque(true);
        setBounds(GameBoard.getWindowWidth(), GameBoard.getWindowHeight(),
                (int) imageSize * 7, (int) imageSize * 5);
        setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0),
                new LineBorder(new Color(68, 170, 228), 3)));
    }

    /**
     * @return the actionButtons
     */
    public Map<String, ActionPopupButton> getActionButtons() {
        return actionButtons;
    }

    /**
     * @return the possibleActions
     */
    public Set<String> getPossibleActions() {
        return possibleActions;
    }

    /**
     * @param possibleActions
     *            the possibleActions to set
     */
    public void setPossibleActions(Set<String> possibleActions) {
        this.possibleActions = possibleActions;
    }
}
