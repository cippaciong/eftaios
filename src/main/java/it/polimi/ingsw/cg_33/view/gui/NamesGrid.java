package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.view.gui.helpers.LoaderSVG;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class NamesGrid extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    // private List<String> playerNames;
    private Map<String, JLabel> playerLabels;
    private Image image;
    private float imageSize = GameBoard.getWindowHeight() * 0.04f;
    private static final String FS = File.separator;

    public NamesGrid(List<String> playerNames) {
        super(new GridLayout(2, 2, 0, 5));

        setOpaque(false);
        setBorder(new EmptyBorder(0, 0, 5, 0));
        loadPlayerLabels(playerNames);

    }

    private void loadPlayerLabels(List<String> playerNames) {
        playerLabels = new LinkedHashMap<String, JLabel>();
        int i = 0;
        for (String player : playerNames) {
            image = LoaderSVG.loadSVGFromFile("src"+FS+"main"+FS+"resources"+FS+"swing"+FS+"names"+FS+""
                    + Integer.toString(i) + ".svg", imageSize, imageSize);
            Icon labelIcon = new ImageIcon(image);
            JLabel playerLabel = new JLabel(player, labelIcon, JLabel.LEFT);
            playerLabel.setHorizontalTextPosition(JLabel.RIGHT);
            playerLabel.setFont(new Font("Orbitron", Font.PLAIN, 11));
            playerLabel.setForeground(Color.WHITE);
            playerLabels.put(player, playerLabel);
            add(playerLabel);
            i++;
        }
    }
}
