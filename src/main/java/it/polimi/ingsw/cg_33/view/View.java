package it.polimi.ingsw.cg_33.view;

import it.polimi.ingsw.cg_33.view.cli.CliIn;
import it.polimi.ingsw.cg_33.view.cli.CliOut;
import it.polimi.ingsw.cg_33.view.connection.client.ConnectionManagerOutAbstract;
import it.polimi.ingsw.cg_33.view.connection.client.RmiManagerInOut;
import it.polimi.ingsw.cg_33.view.connection.client.SocketManagerOut;
import it.polimi.ingsw.cg_33.view.connection.subscriber.Subscribe;
import it.polimi.ingsw.cg_33.view.connection.subscriber.rmi.SubscriberRmi;
import it.polimi.ingsw.cg_33.view.connection.subscriber.socket.SubscriberSocket;
import it.polimi.ingsw.cg_33.view.gui.Gui;
import it.polimi.ingsw.cg_33.view.gui.GuiIn;
import it.polimi.ingsw.cg_33.view.input.Input;

import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is the Main class for the view component.
 * It's purpose it to ask the user for the connection protocol and user interface
 * he wants to use and start the correct components accordingly
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class View {

    private static final Logger LOGGER = Logger.getLogger(View.class.getName());
    private static final Scanner stdin = new Scanner(System.in);

    private View() {
    }

    public static void main(String[] args) throws IOException {

        String id = UUID.randomUUID().toString();
        boolean validnet = false;
        String network;
        do {
            System.out.println("Please choose a network protcol:");
            System.out.println("    - rmi");
            System.out.println("    - socket");
            network = stdin.nextLine().toLowerCase();
            validnet = checkNetworkSettings(network);
        } while (!validnet);
        switch (network) {
        case "rmi":
            startRMI(id);
            break;
        case "socket":
            startSocket(id);
            break;
        default:
            Logger.getAnonymousLogger().warning(
                    "Invalid protocol, please choose either 'rmi' or 'socket'");
            break;
        }
    }

    /**
     * Starts client and subscriber using the socket protocol
     * passing the view id
     *
     * @param  id the view randomly generated id
     */
    private static void startSocket(String id) {
        LOGGER.info("Starting a new client using the socket protocol");
        try {
            Input input = new Input();
            ConnectionManagerOutAbstract socketManagerOut = new SocketManagerOut(
                    input);
            new Thread(socketManagerOut).start();
            Subscribe subscriber = new SubscriberSocket(input);
            input.addObserver(startGraphics(id, socketManagerOut, subscriber));
        } catch (IOException e) {
            LOGGER.log(
                    Level.SEVERE,
                    "An error occurred while starting the client for the socket protocol",
                    e);
        }
    }

    /**
     * Starts client and subscriber using the RMI protocol
     * passing the view id
     *
     * @param  id the view randomly generated id
     */
    public static void startRMI(String id) {
        LOGGER.info("Starting a new client using the rmi protocol");
        RmiManagerInOut rmiManager = new RmiManagerInOut();
        Subscribe subscriber = new SubscriberRmi(id, rmiManager.getInput());
        new Thread(rmiManager).start();
        try {
            rmiManager.getInput().addObserver(
                    startGraphics(id, rmiManager, subscriber));
        } catch (IOException e) {
            LOGGER.log(
                    Level.SEVERE,
                    "An error occurred while starting the client for the RMI protocol",
                    e);
        }
    }

    /**
     * Starts selected user interface (CLI or GUI)
     *
     * @param  id the view randomly generated id
     * @param  connectionManager the client component (socket or RMI)
     * @param  subscriber the subscriber component (socket or RMI)
     */
    public static GraphicInterfaceIn startGraphics(String id,
            ConnectionManagerOutAbstract connectionManager, Subscribe subscriber)
            throws IOException {
        GraphicInterfaceIn graphicChoice = null;
        boolean valid = false;
        String graphics;
        do {
            System.out.println("Choose graphics settings:");
            System.out.println("    - cli");
            System.out.println("    - gui");
            graphics = stdin.nextLine().toLowerCase();
            valid = checkGraphicSettings(graphics);
        } while (!valid);
        switch (graphics) {
        case "cli":
            CliOut cliOut = new CliOut(id, connectionManager);
            cliOut.start();
            CliIn cliIn = new CliIn(cliOut, subscriber);
            graphicChoice = cliIn;
            break;
        case "gui":
            Gui gui = new Gui(id, connectionManager);
            gui.startSwing();
            GuiIn guiIn = new GuiIn(gui, subscriber);
            graphicChoice = guiIn;
            break;
        default:
            break;
        }
        return graphicChoice;
    }

    private static boolean checkNetworkSettings(String network) {
        if (!"rmi".equals(network) && !"socket".equals(network)) {
            Logger.getAnonymousLogger().warning(
                    "Invalid protocol, please choose either 'rmi' or 'socket'");
            return false;
        }
        return true;
    }

    private static boolean checkGraphicSettings(String graphics) {
        if (!"gui".equals(graphics) && !"cli".equals(graphics)) {
            Logger.getAnonymousLogger().warning(
                    "Undefined choice, please choose either 'cli' or 'gui'");
            return false;
        }
        return true;
    }

}
