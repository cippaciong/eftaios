package it.polimi.ingsw.cg_33.view.connection.subscriber.rmi;

import it.polimi.ingsw.cg_33.interfaces.BrokerInterface;
import it.polimi.ingsw.cg_33.interfaces.SubscriberInterface;
import it.polimi.ingsw.cg_33.view.connection.subscriber.Subscribe;
import it.polimi.ingsw.cg_33.view.input.Input;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SubscriberRmi implements Subscribe {

    private static final Logger LOGGER = Logger.getLogger(SubscriberRmi.class
            .getName());
    private String id;
    private Input input;

    public SubscriberRmi(String id, Input input) {
        this.id = id;
        this.input = input;
    }

    @Override
    public void subscribe(String topic) {
        DispatcherRmi sub = new DispatcherRmi(id, input);
        try {
            // LocateRegistry class provides static methods for synthesizing
            // a remote reference to a registry at a particular network
            // address (host and port). In this case 'localhost' is assumed.
            // The no-argument overload of getRegisry uses port 1099
            Registry registry = LocateRegistry.getRegistry(7779);
            // lookup method searches for the remote interface binded to name
            // "Broker"
            // in the server host's registry
            BrokerInterface broker = (BrokerInterface) registry
                    .lookup("Broker");

            // subscriber exports its own remote interface SubscriberInterface
            // so that it can
            // receive invocations from remote brokers.
            // Then it invokes subscribe remote method of the BrokerInterface
            // and passes it's
            // own remote interface as a parameter.
            broker.subscribe(topic, (SubscriberInterface) UnicastRemoteObject
                    .exportObject(sub, 0));

        } catch (NotBoundException | RemoteException e) {
            LOGGER.log(
                    Level.SEVERE,
                    "Could not subscribe to the broker using the socket protocol",
                    e);
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
    }
}
