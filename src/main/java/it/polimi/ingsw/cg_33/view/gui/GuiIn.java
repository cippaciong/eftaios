package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.view.GraphicInterfaceIn;
import it.polimi.ingsw.cg_33.view.connection.subscriber.Subscribe;

import java.awt.Point;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractButton;
import javax.swing.JTextArea;

public class GuiIn implements GraphicInterfaceIn {

    private Subscribe subscriber;
    private String player = "";
    private Gui gui;
    private static final Logger LOGGER = Logger
            .getLogger(GuiIn.class.getName());

    public GuiIn(Gui gui, Subscribe subscriber) {
        this.subscriber = subscriber;
        this.gui = gui;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Output) {
            Output output = (Output) arg;
            outputParseAndPrint(output);
        } else if (arg instanceof ToAllOutput) {
            ToAllOutput toAllOutput = (ToAllOutput) arg;
            toAllOutputParseAndPrint(toAllOutput);
        }
    }

    private void toAllOutputParseAndPrint(ToAllOutput toAllOutput) {
        JTextArea logTextArea = gui.getGameBoard().getSidePanel().getLogArea();
        JTextArea chatTextArea = gui.getGameBoard().getSidePanel()
                .getTextArea();

        chatMessage(toAllOutput, chatTextArea);
        logDescription(toAllOutput, logTextArea);
        updatePlayerStatus(toAllOutput);
        beginMatch(toAllOutput, logTextArea);
        checkNoise(toAllOutput);
        checkSpotlight(toAllOutput);
    }

    private void outputParseAndPrint(Output output) {
        appendDescription(output);
        loadGameMap(output);
        loadPieceIcon(output);
        loadPlayersName(output);
        reloadActions(output);
        reloadCurrentPosition(output);
        if (output.isLoginSuccess() || output.isReconnectSuccess()
                || output.isSignupSuccess()) {
            gui.getGameBoard().closeLogin();
            if (output.isReconnectSuccess())
                gui.getGameBoard().getInitialInformations();
        }
        readLoginDescription(output);
        reloadItemsList(output);
        subscribe(output);
    }

    private void chatMessage(ToAllOutput toAllOutput, JTextArea chatTextArea) {

        if (toAllOutput.getChatMsg() != null) {
            chatTextArea.append(toAllOutput.getChatMsg() + "\n");
            chatTextArea.setCaretPosition(chatTextArea.getDocument()
                    .getLength());
        }
    }

    private void logDescription(ToAllOutput toAllOutput, JTextArea logTextArea) {
        if (toAllOutput.getDescription() != null
                && (!this.player.equals(toAllOutput.getCurrentPlayer())
                        || toAllOutput.getNumTurn() != 0 || !toAllOutput
                        .getChatPlayer().equals(player))) {
            logTextArea.append(toAllOutput.getDescription() + "\n");
            logTextArea.setCaretPosition(logTextArea.getDocument().getLength());
        }
    }

    private void updatePlayerStatus(ToAllOutput toAllOutput) {
        if (!toAllOutput.getMapPlayerStatus().isEmpty()) {
            Map<String, String> mapPlayerStatus = toAllOutput
                    .getMapPlayerStatus();
            for (Entry<String, String> entry : mapPlayerStatus.entrySet()) {
                gui.getGameBoard()
                        .getSidePanel()
                        .getPlayers()
                        .setImageFromString(entry.getKey(),
                                entry.getValue().toLowerCase());
            }

        }
    }

    private void beginMatch(ToAllOutput toAllOutput, JTextArea logTextArea) {
        if (toAllOutput.isMatchBegin()) {
            gui.getGameBoard().getInitialInformations();
            logTextArea.append(player + "\n");
            logTextArea.setCaretPosition(logTextArea.getDocument().getLength());
        }
    }

    private void checkNoise(ToAllOutput toAllOutput) {
        if (toAllOutput.getNoiseSector() != null) {
            Point pointCoordinates = pointCoordinateFromString(toAllOutput
                    .getNoiseSector());
            if (!(new Point(-1, -1).equals(pointCoordinates)))
                gui.getGameBoard().getMapPanel()
                        .moveNoiseLabel(pointCoordinates);
            else
                LOGGER.warning("Could not reload the current position from a wrong coordinate");
        }
    }

    private void checkSpotlight(ToAllOutput toAllOutput) {
        if (!toAllOutput.getSpottedPlayers().isEmpty()) {
            Map<String, String> spottedPlayers = toAllOutput
                    .getSpottedPlayers();
            for (Entry<String, String> entry : spottedPlayers.entrySet()) {
                Point sectorCoord = pointCoordinateFromString(entry.getValue());
                int player = gui.getGameBoard().getUsernames()
                        .indexOf(entry.getKey());
                gui.getGameBoard().getMapPanel()
                        .movePlayerIcon(player, sectorCoord);
            }
        }

    }

    private void appendDescription(Output output) {
        JTextArea logTextArea = gui.getGameBoard().getSidePanel().getLogArea();
        if (!output.getDescriptions().isEmpty()) {
            for (String description : output.getDescriptions()) {
                logTextArea.append(description + "\n");
                logTextArea.setCaretPosition(logTextArea.getDocument()
                        .getLength());
            }
        }
    }

    private void loadPieceIcon(Output output) {
        if (output.getTitle() != null) {
            gui.getGameBoard().getMapPanel().initPieceLabel(output.getTitle());
        }
    }

    private void loadPlayersName(Output output) {
        if (!output.getPlayersName().isEmpty()) {
            gui.getGameBoard().getSidePanel()
                    .loadNamesGrid(output.getPlayersName());
            if (gui.getGameBoard().getUsernames().isEmpty())
                gui.getGameBoard().setUsernames(output.getPlayersName());
        }
    }

    private void reloadActions(Output output) {
        if (!output.getPossibleActions().isEmpty()) {
            gui.getGameBoard().getMapPanel().getActionPopup()
                    .setPossibleActions(output.getPossibleActions());
            gui.getGameBoard().getMapPanel().getActionPopup()
                    .reloadActionIcon();
        }
    }

    private void reloadCurrentPosition(Output output) {
        if (output.getCurrentPosition() != null) {
            Point pointCoordinates = pointCoordinateFromString(output
                    .getCurrentPosition());
            if (!(new Point(-1, -1).equals(pointCoordinates)))
                gui.getGameBoard().getMapPanel()
                        .movePieceLabel(pointCoordinates);
            else
                LOGGER.warning("Could not reload the current position from a wrong coordinate");
        }
    }

    private void readLoginDescription(Output output) {
        if (output.getLoginDescription() != null)
            System.err.println(output.getLoginDescription());
    }

    private void reloadItemsList(Output output) {
        System.out.println(output.getHaveItems());
        gui.getGameBoard().setItemsList(output.getHaveItems());
        ItemsGrid itemsGrid = gui.getGameBoard().getSidePanel().getItemsGrid();
        Map<String, AbstractButton> itemButtons = itemsGrid.getItemButtons();
        for (String item : itemButtons.keySet()) {
            if (output.getHaveItems().contains(item))
                itemsGrid.setItemButtonIcon(item, true);
            else
                itemsGrid.setItemButtonIcon(item, false);
        }
    }

    private void loadGameMap(Output output) {
        if (output.getGameMap() != null) {
            MapPanel mapPanel = gui.getGameBoard().getMapPanel();
            mapPanel.populateWithHexagons(output.getGameMap(),
                    mapPanel.getWidth(), mapPanel.getHeight());
            gui.getGameBoard().listenTo();

            reloadActions(output);
        }
    }

    private void subscribe(Output output) {
        if (output.getTopic() != null)
            subscriber.subscribe(output.getTopic());
    }

    private Point pointCoordinateFromString(String stringCoordinate) {
        int x = -1;
        int y = -1;
        try {
            x = Integer.parseInt(stringCoordinate.split("-")[0]);
            y = Integer.parseInt(stringCoordinate.split("-")[1]);
        } catch (NumberFormatException e) {
            LOGGER.log(Level.WARNING,
                    "Could not convert current position from String to int", e);
        }
        return new Point(x, y);
    }

}
