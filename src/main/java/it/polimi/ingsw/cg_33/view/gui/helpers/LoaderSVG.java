package it.polimi.ingsw.cg_33.view.gui.helpers;

import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.batik.anim.dom.SVGDOMImplementation;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscodingHints;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.util.SVGConstants;
import org.w3c.dom.DOMImplementation;

public class LoaderSVG {

    private static final Logger LOGGER = Logger
            .getLogger(LoaderSVG.class.getName());
    
    private LoaderSVG() {
        
    }
    
    public static BufferedImage loadSVGFromFile(String path, Float width,
            Float height) {
        MyTranscoder transcoder = new MyTranscoder();
        DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();
        TranscodingHints hints = new TranscodingHints();
        hints.put(ImageTranscoder.KEY_WIDTH, width);
        hints.put(ImageTranscoder.KEY_HEIGHT, height);
        hints.put(ImageTranscoder.KEY_DOM_IMPLEMENTATION, impl);
        hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT_NAMESPACE_URI,
                SVGConstants.SVG_NAMESPACE_URI);
        hints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT,
                SVGConstants.SVG_SVG_TAG);
        transcoder.setTranscodingHints(hints);
        try {
            transcoder.transcode(new TranscoderInput(path), null);
        } catch (TranscoderException e1) {
            LOGGER.log(Level.WARNING, "An error occured while trying to transcode your svg image", e1);
        }
        return transcoder.getImage();
    }

}
