package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.view.connection.client.ConnectionManagerOutAbstract;
import it.polimi.ingsw.cg_33.view.gui.graphicelements.HexagonalButton;
import it.polimi.ingsw.cg_33.view.gui.graphicelements.TransparentTextField;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;


/**
 * This is the main GUI class containing all informations about gui components.
 * From this class you can reach almost any component, both in the Map Panel or in the Side Panel.
 * It also implements ActionListener in order to react to various events other components generate
 * and send the right message to the server.
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class GameBoard extends JFrame implements ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static GraphicsDevice gd = GraphicsEnvironment
            .getLocalGraphicsEnvironment().getDefaultScreenDevice();
    private static final int WINDOW_WIDTH = gd.getDisplayMode().getWidth();
    private static final int WINDOW_HEIGHT = gd.getDisplayMode().getHeight();
    private static final Logger LOGGER = Logger.getLogger(GameBoard.class
            .getName());

    private Image backgroundImage;
    private JLabel backgroundLabel;
    private JLabel chooseMapLabel;
    private JLayeredPane layeredPane;
    private SidePanel sidePanel;
    private MapPanel mapPanel;
    private LoginPanel loginPanel;
    private String clientId = null;
    private ConnectionManagerOutAbstract connectionOut;
    private List<String> itemsList;
    private List<String> usernames = new ArrayList<String>();
    private static final String FS = File.separator;

    private static final int LAYER_BACKGROUND = 1;
    private static final int LAYER_HEXAGONS = 10;
    private static final int LAYER_SETUP = 20;

    public GameBoard(String clientId, ConnectionManagerOutAbstract connectionOut) {

        this.clientId = clientId;
        this.connectionOut = connectionOut;

        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);

        // we don't want to let the user to resize the windows
        setResizable(false);
        setUndecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        loadFont();
        loadBackground();
        initComponents();

    }

    private void loadBackground() {

        try {
            backgroundImage = ImageIO.read(
                    new File("src"+FS+"main"+FS+"resources"+FS+"swing"+FS+"outerspaceblur.jpg"))
                    .getScaledInstance((int) (WINDOW_WIDTH),
                            (int) (WINDOW_HEIGHT), Image.SCALE_SMOOTH);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING,
                    "Could not load GameBoard background image", e);
        }

    }

    private void initComponents() {

        layeredPane = new JLayeredPane();
        setContentPane(layeredPane);
        initBackground();
        initMapPanel();
        initSidePanel();
    }

    private void initBackground() {
        backgroundLabel = new JLabel(new ImageIcon(backgroundImage));
        backgroundLabel.setBounds(0, 0, (int) (WINDOW_WIDTH), WINDOW_HEIGHT);
        add(backgroundLabel);
        layeredPane.setLayer(backgroundLabel, LAYER_BACKGROUND);

    }

    private void initMapPanel() {
        mapPanel = new MapPanel();
        mapPanel.setBounds(0, 0, (int) (WINDOW_WIDTH * 0.7), WINDOW_HEIGHT);
        add(mapPanel);
        layeredPane.setLayer(mapPanel, LAYER_HEXAGONS);

    }

    private void initSidePanel() {
        sidePanel = new SidePanel();
        sidePanel.setBounds((int) (WINDOW_WIDTH * 0.7), 0,
                (int) (WINDOW_WIDTH * 0.3), WINDOW_HEIGHT);
        sidePanel.setOpaque(false);
        add(sidePanel);
        layeredPane.setLayer(sidePanel, LAYER_HEXAGONS);

    }

    /**
     * Starts the login panel and hides all the panels below
     */
    public void login() {
        loginPanel = new LoginPanel(clientId, connectionOut);
        loginPanel.setBounds(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        add(loginPanel);
        layeredPane.setLayer(loginPanel, LAYER_SETUP);
    }

    /**
     * Closes the login panel and initializes the panel with the three map buttons
     */
    public void closeLogin() {
        chooseMap();
        remove(loginPanel);
    }

    /**
     * Starts the chooseMap panel to perform the enterRoom
     */
    public void chooseMap() {
        chooseMapLabel = new JLabel();
        chooseMapLabel.setOpaque(true);
        chooseMapLabel.setBackground(Color.BLACK);
        chooseMapLabel.setBounds(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        try {
            chooseMapLabel.setIcon(new ImageIcon(ImageIO.read(
                    new File("src"+FS+"main"+FS+"resources"+FS+"swing"+FS+"artwork"+FS+"title.png"))
                    .getScaledInstance(WINDOW_WIDTH, WINDOW_HEIGHT,
                            Image.SCALE_SMOOTH)));
        } catch (IOException e) {
            LOGGER.log(Level.WARNING,
                    "Could not load ChooseMap label background image", e);
        }

        loadMapButtons(chooseMapLabel);
        add(chooseMapLabel);
        layeredPane.setLayer(chooseMapLabel, LAYER_SETUP);

    }

    private void loadMapButtons(JLabel chooseMapLabel) {
        int buttonHeight = (int) (WINDOW_HEIGHT * 0.111);
        JButton galileibtn = new HexagonalButton("Galilei", buttonHeight);
        JButton fermibtn = new HexagonalButton("Fermi", buttonHeight);
        JButton galvanibtn = new HexagonalButton("Galvani", buttonHeight);
        galileibtn.setActionCommand("galilei");
        galvanibtn.setActionCommand("galvani");
        fermibtn.setActionCommand("fermi");
        Set<JButton> buttons = new HashSet<JButton>();
        buttons.add(galileibtn);
        buttons.add(fermibtn);
        buttons.add(galvanibtn);

        for (JButton button : buttons) {
            button.setFont(new Font("Orbitron", Font.PLAIN, buttonHeight / 5));
            button.setForeground(Color.BLACK);
            chooseMapLabel.add(button);
            button.addActionListener(this);

        }
        galileibtn.setLocation((int) (WINDOW_WIDTH * 0.8),
                (int) (WINDOW_HEIGHT * 0.34));
        fermibtn.setLocation((int) (WINDOW_WIDTH * 0.8),
                (int) (WINDOW_HEIGHT * 0.34 + buttonHeight));
        galvanibtn.setLocation((int) (WINDOW_WIDTH * 0.8),
                (int) (WINDOW_HEIGHT * 0.34 + buttonHeight * 2));
    }

    private void closeChooseMap() {
        remove(chooseMapLabel);
        repaint();
    }

    /**
     * Loads a custom font
     **/
    private void loadFont() {
        GraphicsEnvironment ge = null;
        try {
            ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File(
                    "src"+FS+"main"+FS+"resources"+FS+"swing"+FS+"fonts"+FS+"orbitron_medium.ttf")));
        } catch (IOException | FontFormatException e) {
            LOGGER.log(Level.WARNING, "Could not load the desired font", e);
        }
    }

    /**
     * Adds the gamebord as action listener of all the components
     **/
    public void listenTo() {
        Map<String, ActionPopupButton> actionButtons = mapPanel
                .getActionPopup().getActionButtons();
        for (ActionPopupButton button : actionButtons.values())
            button.addActionListener(this);

        Map<String, AbstractButton> itemButtons = sidePanel.getItemsGrid()
                .getItemButtons();
        for (AbstractButton button : itemButtons.values())
            button.addActionListener(this);

        sidePanel.getTextField().addActionListener(this);
    }

    /**
     * Gets initial informations from the server
     **/
    public void getInitialInformations() {
        Message msg = new Message(clientId, "getinitialinformations");
        connectionOut.dispatchMessage(msg);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<String> args = null;
        Message msg;
        if (e.getSource() instanceof TransparentTextField) {
            args = new ArrayList<String>();
            args.add(e.getActionCommand());
            msg = new Message(clientId, "sendChatMessage", args);
            connectionOut.dispatchMessage(msg);
        } else {
            switch (e.getActionCommand()) {
            case "galilei":
                createAndSendMessage("enterRoom", "Galilei");
                closeChooseMap();
                break;
            case "galvani":
                createAndSendMessage("enterRoom", "Galvani");
                closeChooseMap();
                break;
            case "fermi":
                createAndSendMessage("enterRoom", "Fermi");
                closeChooseMap();
                break;
            // /////////////////// ACTIONS /////////////////////
            case "mark":
                Object[] users = new String[usernames.size()];
                users = usernames.toArray(users);
                String markedUser = (String) JOptionPane.showInputDialog(
                        new JFrame(), "Choose the item you want to discard:\n",
                        "Discard Item", JOptionPane.PLAIN_MESSAGE,
                        new ImageIcon(), users, users[0]);
                int userListPosition = usernames.indexOf(markedUser);
                mapPanel.movePlayerIcon(userListPosition, mapPanel
                        .getSelectionController().getHexagonCoords());
                System.err.println(mapPanel.getSelectionController().getHexagonCoords());
                break;
            case "move":
                createAndSendMessage("move", mapPanel.getSelectionController()
                        .getSelectedHexagonCoord());
                break;
            case "attack":
                createAndSendMessage("attack");
                break;
            case "draw":
                createAndSendMessage("draw");
                break;
            case "discarditem":
                Object[] possibilities = new String[itemsList.size()];
                possibilities = itemsList.toArray(possibilities);
                String discardedItem = (String) JOptionPane.showInputDialog(
                        new JFrame(), "Choose the item you want to discard:\n",
                        "Discard Item", JOptionPane.PLAIN_MESSAGE,
                        new ImageIcon(), possibilities, possibilities[0]);
                createAndSendMessage("discardItem", discardedItem);
                break;
            case "noise":
                createAndSendMessage("noise");
                break;
            case "noiseinanysector":
                createAndSendMessage("noiseInAnySector", mapPanel
                        .getSelectionController().getSelectedHexagonCoord());
                break;
            case "endturn":
                createAndSendMessage("endTurn");
                break;
            // /////////////////// ITEMS /////////////////////
            case "Adrenaline":
                createAndSendMessage("useAdrenaline");
                break;
            case "Attackitem":
                createAndSendMessage("useAttack");
                break;
            case "Defence":
                break;
            case "Sedatives":
                createAndSendMessage("useSedatives");
                break;
            case "Spotlight":
                if (!sidePanel.getItemsGrid().getSpotlightButton().isSelected()) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Choose the sector where you want to use the spotlight\n"
                                    + "Then click on the spotlight icon again");
                    mapPanel.getSelectionController().setSpotlight(true);
                } else {
                    createAndSendMessage("useSpotlight", mapPanel
                            .getSelectionController().getSelectedHexagonCoord());
                    mapPanel.getSelectionController().setSpotlight(false);
                }
                break;
            case "Teleport":
                createAndSendMessage("useTeleport");
                break;
            default:
                LOGGER.info("Catched unhandled action");
                break;
            }
        }
    }

    private void createAndSendMessage(String action, String arg) {
        List<String> args = null;
        Message msg;
        args = new ArrayList<String>();
        args.add(arg);
        msg = new Message(clientId, action, args);
        connectionOut.dispatchMessage(msg);
    }

    private void createAndSendMessage(String action) {
        Message msg;
        msg = new Message(clientId, action);
        connectionOut.dispatchMessage(msg);
    }

    /**
     * @return the mapPanel
     */
    public MapPanel getMapPanel() {
        return mapPanel;
    }

    /**
     * @return the sidePanel
     */
    public SidePanel getSidePanel() {
        return sidePanel;
    }

    /**
     * @return the itemsList
     */
    public List<String> getItemsList() {
        return itemsList;
    }

    /**
     * @param itemsList
     *            the itemsList to set
     */
    public void setItemsList(List<String> itemsList) {
        this.itemsList = itemsList;
    }

    /**
     * @return the usernames
     */
    public List<String> getUsernames() {
        return usernames;
    }

    /**
     * @param usernames
     *            the usernames to set
     */
    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

    /**
     * @return the layeredPane
     */
    @Override
    public JLayeredPane getLayeredPane() {
        return layeredPane;
    }

    /**
     * @return the windowWidth
     */
    public static int getWindowWidth() {
        return WINDOW_WIDTH;
    }

    /**
     * @return the windowHeight
     */
    public static int getWindowHeight() {
        return WINDOW_HEIGHT;
    }

}
