package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.view.gui.drawablesectors.DrawableHexagon;
import it.polimi.ingsw.cg_33.view.gui.helpers.PixToHex;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class SelectionController implements MouseListener {

    private Map<Point, DrawableHexagon> hexagons;
    private int hexagonHeight;
    private int borderX;
    private int borderY;
    private JLayeredPane layeredPane;
    private JPanel label;
    private String selectedHexagonCoord;
    private Point hexagonCoords;
    private boolean isSpotlight;
    private static final Logger LOGGER = Logger
            .getLogger(SelectionController.class.getName());

    public SelectionController(Map<Point, DrawableHexagon> hexagons,
            int borderX, int borderY, JPanel label, JLayeredPane layeredPane) {
        this.hexagons = hexagons;
        this.borderX = borderX;
        this.borderY = borderY;
        this.label = label;
        this.layeredPane = layeredPane;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            selectAction(e);
        }
        if (SwingUtilities.isRightMouseButton(e))
            rightAction();
    }

    private void selectAction(MouseEvent e) {
        hexagonHeight = hexagons.get(new Point(0, 0)).getHeight();
        hexagonCoords = PixToHex.pixToHex(e.getX(), e.getY(),
                hexagonHeight, borderX, borderY);

        if (!isSpotlight) {
            if (e.getX() < layeredPane.getWidth() / 2
                    && e.getY() > layeredPane.getHeight() / 2) {
                label.setLocation(new Point(e.getX(), e.getY()
                        - label.getHeight()));
            } else if (e.getX() > layeredPane.getWidth() / 2
                    && e.getY() > layeredPane.getHeight() / 2) {
                label.setLocation(new Point(e.getX() - label.getWidth(), e
                        .getY() - label.getHeight()));
            } else if (e.getX() > layeredPane.getWidth() / 2
                    && e.getY() < layeredPane.getHeight() / 2) {
                label.setLocation(new Point(e.getX() - label.getWidth(), e
                        .getY()));
            } else {
                label.setLocation(new Point(e.getX(), e.getY()));
            }

            layeredPane.setLayer(label, MapPanel.POPUP_LAYER);
            layeredPane.add(label);
        }

        try {
            int x = (int) hexagonCoords.getX();
            int y = (int) hexagonCoords.getY();
            selectedHexagonCoord = Integer.toString(x) + "-"
                    + Integer.toString(y);
        } catch (NullPointerException npe) {
            LOGGER.log(Level.INFO, "The hexagon you selected doesn't exist", npe);
        }
    }

    private void rightAction() {
        layeredPane.remove(label);
        layeredPane.repaint();
    }

    /**
     * @return the hexagonCoords
     */
    public Point getHexagonCoords() {
        return hexagonCoords;
    }

    /**
     * @return the selectedHexagonCoord
     */
    public String getSelectedHexagonCoord() {
        return selectedHexagonCoord;
    }

    /**
     * @param selectedHexagonCoord
     *            the selectedHexagonCoord to set
     */
    public void setSelectedHexagonCoord(String selectedHexagonCoord) {
        this.selectedHexagonCoord = selectedHexagonCoord;
    }

    /**
     * @return the isSpotlight
     */
    public boolean isSpotlight() {
        return isSpotlight;
    }

    /**
     * @param isSpotlight
     *            the isSpotlight to set
     */
    public void setSpotlight(boolean isSpotlight) {
        this.isSpotlight = isSpotlight;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

}
