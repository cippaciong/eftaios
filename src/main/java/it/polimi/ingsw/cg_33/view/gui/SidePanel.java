package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.view.gui.graphicelements.InvisibleScrollBarUI;
import it.polimi.ingsw.cg_33.view.gui.graphicelements.TransparentTextArea;
import it.polimi.ingsw.cg_33.view.gui.graphicelements.TransparentTextField;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class SidePanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JTextField textField;
    private JTextArea textArea, logArea;
    private NamesGrid namesGrid;
    private ItemsGrid itemsGrid;
    private PlayersGrid players;
    private String fontName = "Orbitron";

    public SidePanel() {
        super(new GridBagLayout());

        textArea = new TransparentTextArea();
        textArea.setEditable(false);
        textArea.setFont(new Font(fontName, Font.PLAIN, 12));
        textArea.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.getViewport().setOpaque(false);
        scrollPane.setBorder(null);
        scrollPane.setOpaque(false);
        scrollPane.getVerticalScrollBar().setUI(new InvisibleScrollBarUI());
        scrollPane.getVerticalScrollBar().setBackground(
                new Color(255, 255, 255, 0));

        textField = new TransparentTextField();
//        textField.addActionListener(this);
        textField.setCaretColor(Color.WHITE);
        textField.setFont(new Font(fontName, Font.PLAIN, 12));
        
//        namesGrid = new NamesGrid();
        
        
        logArea = new TransparentTextArea();
        logArea.setEditable(false);
        logArea.setFont(new Font(fontName, Font.PLAIN, 12));
        logArea.setLineWrap(true);
        JScrollPane logScrollPane = new JScrollPane(logArea);
        logScrollPane.getViewport().setOpaque(false);
        logScrollPane.setBorder(null);
        logScrollPane.setOpaque(false);
        logScrollPane.getVerticalScrollBar().setUI(new InvisibleScrollBarUI());
        logScrollPane.getVerticalScrollBar().setBackground(
                new Color(255, 255, 255, 0));

        // Add Components to this panel.
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;

        c.fill = GridBagConstraints.BOTH;
        c.gridy = 1;
        c.weightx = 0.5;
        c.weighty = 1.0;
        add(scrollPane, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridy = 2;
        c.weighty = 0.1;
        add(textField, c);
        
//        c.fill = GridBagConstraints.BOTH;
//        c.gridy = 3;
//        c.weighty = 0.1;
//        add(namesGrid, c);

        c.fill = GridBagConstraints.BOTH;
        c.gridy = 4;
        c.weighty = 0.2;
        add(logScrollPane, c);

        players = new PlayersGrid();
        c.fill = GridBagConstraints.BOTH;
        c.gridy = 5;
        c.weighty = 0.2;
        c.insets = new Insets(0, 10, 0, 10);
//        c.insets = new Insets(0, 10, 30, 10);
        add(players, c);

        itemsGrid = new ItemsGrid();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridy = 6;
        c.weighty = 0.0;
        c.anchor = GridBagConstraints.PAGE_END; // bottom of space
        c.insets = new Insets(0, 10, 10, 10);
        add(itemsGrid, c);
    }

//    public void actionPerformed(ActionEvent evt) {
//        String text = textField.getText();
//        textArea.append(text + newline);
//        textField.selectAll();
//
//        // Make sure the new text is visible, even if there
//        // was a selection in the text area.
//        textArea.setCaretPosition(textArea.getDocument().getLength());
//    }
    
    public void loadNamesGrid(List<String> names) {
        System.err.println(SwingUtilities.isEventDispatchThread());
        NamesGrid namesGrid = new NamesGrid(names);
        GridBagConstraints constr = new GridBagConstraints();
        constr.gridy = 3;
        constr.fill = GridBagConstraints.BOTH;
        constr.weighty = 0.0;
        add(namesGrid, constr);
        validate();
//        repaint();
//        revalidate();
    }

    /**
     * @return the itemButtons
     */
    public ItemsGrid getItemsGrid() {
        return itemsGrid;
    }

    /**
     * @return the players
     */
    public PlayersGrid getPlayers() {
        return players;
    }

    /**
     * @return the textArea
     */
    public JTextArea getTextArea() {
        return textArea;
    }

    /**
     * @return the namesGrid
     */
    public NamesGrid getNamesGrid() {
        return namesGrid;
    }

    /**
     * @return the logArea
     */
    public JTextArea getLogArea() {
        return logArea;
    }

    /**
     * @return the textField
     */
    public JTextField getTextField() {
        return textField;
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(new Color(0, 0, 0, 128));
        Insets insets = getInsets();
        int x = insets.left;
        int y = insets.top;
        int width = getWidth() - (insets.left + insets.right);
        int height = getHeight() - (insets.top + insets.bottom);
        g.fillRect(x, y, width, height);
        super.paintComponent(g);
    }
}
