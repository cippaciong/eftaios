package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.view.gui.helpers.LoaderSVG;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

public class ItemsGrid extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private List<String> itemNames;
    private Map<String, AbstractButton> itemButtons;
    private Image image;
    private float imageSize = GameBoard.getWindowHeight() * 0.06f;
    private static final String FS = File.separator;
    private static final String BASEPATH = "src" + FS + "main" + FS
            + "resources" + FS + "swing" + FS + "items" + FS;

    public ItemsGrid() {
        super(new GridLayout());

        setOpaque(false);
        setLayout(new GridLayout(1, 0));
        setBorder(new EmptyBorder(0, 0, 25, 0));

        loadItemButtons();
    }

    private void loadItemButtons() {
        itemNames = new LinkedList<String>();
        itemNames.add("Adrenaline");
        itemNames.add("Attack");
        itemNames.add("Defence");
        itemNames.add("Sedatives");
        itemNames.add("Spotlight");
        itemNames.add("Teleport");
        itemButtons = new LinkedHashMap<String, AbstractButton>();
        for (String item : itemNames) {
            image = LoaderSVG.loadSVGFromFile(BASEPATH + item.toLowerCase()
                    + "_no.svg", imageSize, imageSize);
            Icon buttonIcon = new ImageIcon(image);
            if ("Spotlight".equals(item)) {
                JToggleButton spotlightButton = new JToggleButton(item,
                        buttonIcon, true);
                loadSpotlightSettings(spotlightButton);
                itemButtons.put(item, spotlightButton);
            } else {
                itemButtons.put(item, new ItemButton(item, buttonIcon));
            }
        }

        for (AbstractButton button : itemButtons.values()) {
            add(button);
        }

    }

    private void loadSpotlightSettings(JToggleButton spotlightButton) {
        spotlightButton.setBorder(null);
        spotlightButton.setContentAreaFilled(false);
        spotlightButton.setBorderPainted(false);
        spotlightButton.setOpaque(false);
        spotlightButton.setBackground(new Color(255, 255, 255, 0));
        spotlightButton.setForeground(Color.WHITE);
        spotlightButton.setFont(new Font("Orbitron", Font.PLAIN, 12));
        spotlightButton.setHorizontalTextPosition(JLabel.CENTER);
        spotlightButton.setVerticalTextPosition(JLabel.BOTTOM);
        spotlightButton.setActionCommand("Spotlight");
        add(spotlightButton);
    }

    public void setItemButtonIcon(String itemName, boolean hasItem) {
        Icon itemButtonIcon;
        if (hasItem) {
            image = LoaderSVG.loadSVGFromFile(BASEPATH
                    + itemName.toLowerCase() + ".svg", imageSize, imageSize);
            itemButtonIcon = new ImageIcon(image);
            itemButtons.get(itemName).setIcon(itemButtonIcon);
        } else {
            image = LoaderSVG.loadSVGFromFile(BASEPATH
                    + itemName.toLowerCase() + "_no.svg", imageSize, imageSize);
            itemButtonIcon = new ImageIcon(image);
            itemButtons.get(itemName).setIcon(itemButtonIcon);

        }
    }

    /**
     * @return the itemButtons
     */
    public Map<String, AbstractButton> getItemButtons() {
        return itemButtons;
    }

    /**
     * @return the spotlightButton
     */
    public AbstractButton getSpotlightButton() {
        return itemButtons.get("Spotlight");
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(new Color(255, 255, 255, 0));
        Insets insets = getInsets();
        int x = insets.left;
        int y = insets.top;
        int width = getWidth() - (insets.left + insets.right);
        int height = getHeight() - (insets.top + insets.bottom);
        g.fillRect(x, y, width, height);
        super.paintComponent(g);
    }

}
