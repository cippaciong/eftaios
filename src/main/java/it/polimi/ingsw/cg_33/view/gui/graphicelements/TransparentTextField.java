package it.polimi.ingsw.cg_33.view.gui.graphicelements;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class TransparentTextField extends JTextField {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public TransparentTextField() {
        setOpaque(false);
        setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10),
                new LineBorder(Color.LIGHT_GRAY)));
        setBackground(new Color(0, 0, 0, 255));
        setForeground(new Color(255, 255, 255, 255));
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g.create();
        g2d.setComposite(AlphaComposite.SrcOver.derive(0.5f));
        super.paint(g2d);
        g2d.dispose();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g.create();
        g2d.setColor(getBackground());
        g2d.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g2d);
        g2d.dispose();
    }

}
