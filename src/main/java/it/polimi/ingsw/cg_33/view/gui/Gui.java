package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.view.connection.client.ConnectionManagerOutAbstract;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Gui {

    private String clientId;
    private ConnectionManagerOutAbstract connectionManager;
    private GameBoard gameBoard;
    private static final Logger LOGGER = Logger.getLogger(Gui.class.getName());

    public Gui(String clientId, ConnectionManagerOutAbstract connectionManager) {
        this.clientId = clientId;
        this.connectionManager = connectionManager;
    }

    public void startSwing() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager
                            .getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException
                        | IllegalAccessException
                        | UnsupportedLookAndFeelException ex) {
                    LOGGER.log(
                            Level.INFO,
                            "Swing wasn't able to set the desired look and feel",
                            ex);
                }

                gameBoard = new GameBoard(clientId, connectionManager);
                gameBoard.login();
                gameBoard.setVisible(true);
            }
        });
    }

    /**
     * @return the gameBoard
     */
    public GameBoard getGameBoard() {
        return gameBoard;
    }

}
