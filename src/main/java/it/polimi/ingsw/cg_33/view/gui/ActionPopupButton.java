package it.polimi.ingsw.cg_33.view.gui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.Icon;
import javax.swing.JButton;

public class ActionPopupButton extends JButton {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String actionName;

    public ActionPopupButton(String actionName, Icon buttonIcon) {
        super(actionName, buttonIcon);
        this.actionName = actionName;

        loadComponentSettings(actionName);
    }

    private void loadComponentSettings(String actionName) {
        setBorder(null);
        setContentAreaFilled(false);
        setForeground(Color.WHITE);
        setFont(new Font("Orbitron", Font.PLAIN, 12));
        setHorizontalTextPosition(JButton.CENTER);
        setVerticalTextPosition(JButton.BOTTOM);
        setActionCommand(actionName);
    }

    /**
     * @return the actionName
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * @param actionName the actionName to set
     */
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

}
