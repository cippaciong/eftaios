package it.polimi.ingsw.cg_33.view.gui.helpers;

import java.awt.Point;

public class PixToHex {

    // private static int BORDERS=0; //default number of pixels for the border.

    public static Point pixToHex(int mx, int my, int altezza, int borderX,
            int borderY) {
        Point p = new Point(-1, -1);

        final double diametroCircInscritta = 1.73205;
        int raggio = altezza / 2;
        int lato = (int) (altezza / diametroCircInscritta);
        int mezzolato = lato / 2;

        // correction for BORDERS and XYVertex
        mx -= borderX;
        my -= borderY;

        int x = (mx / (lato + mezzolato)); // this gives a quick value for x. It
                                           // works only on odd cols and doesn't
                                           // handle the triangle sections. It
                                           // assumes that the hexagon is a
                                           // rectangle with width s+t (=1.5*s).
        int y = ((my - (x % 2) * raggio) / altezza); // this gives the row
                                                     // easily. It needs to be
                                                     // offset by h/2 (=r)if it
                                                     // is in an even column

        /****** FIX for clicking in the triangle spaces (on the left side only) *******/
        // dx,dy are the number of pixels from the hex boundary. (ie. relative
        // to the hex clicked in)
        int dx = mx - x * (lato + mezzolato);
        int dy = my - y * altezza;

        if (my - (x % 2) * raggio < 0)
            return p; // prevent clicking in the open halfhexes at the top of
                      // the screen

        // System.out.println("dx=" + dx + " dy=" + dy + "  > " + dx*r/t +
        // " <");

        // even columns
        if (x % 2 == 0) {
            if (dy > raggio) { // bottom half of hexes
                if (dx * raggio / mezzolato < dy - raggio) {
                    x--;
                }
            }
            if (dy < raggio) { // top half of hexes
                if ((mezzolato - dx) * raggio / mezzolato > dy) {
                    x--;
                    y--;
                }
            }
        } else { // odd columns
            if (dy > altezza) { // bottom half of hexes
                if (dx * raggio / mezzolato < dy - altezza) {
                    x--;
                    y++;
                }
            }
            if (dy < altezza) { // top half of hexes
                // System.out.println("" + (t- dx)*r/t + " " + (dy - r));
                if ((mezzolato - dx) * raggio / mezzolato > dy - raggio) {
                    x--;
                }
            }
        }
        p.x = x;
        p.y = y;
        return p;
    }

}
