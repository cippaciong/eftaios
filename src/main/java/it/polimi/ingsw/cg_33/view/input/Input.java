package it.polimi.ingsw.cg_33.view.input;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.communication.ToAllOutput;

import java.util.Observable;

public class Input extends Observable {
    public void dispatchOutput(Output output) {
        setChanged();
        notifyObservers(output);
    }
    
    public void dispatchToAllOutput(ToAllOutput toAllOutput){
        setChanged();
        notifyObservers(toAllOutput);
    }
}
