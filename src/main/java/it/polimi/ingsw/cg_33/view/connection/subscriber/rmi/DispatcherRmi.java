package it.polimi.ingsw.cg_33.view.connection.subscriber.rmi;

import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.interfaces.SubscriberInterface;
import it.polimi.ingsw.cg_33.view.input.Input;

public class DispatcherRmi implements SubscriberInterface {

    private String name;
    Input input;
    /**
     * 
     * @param name
     *            The name of the subscriber
     */
    public DispatcherRmi(String name, Input input) {
        this.name = name;
        this.input = input;
    }

    /**
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param toAllOutput
     *            is the message sent by the broker by invoking subscriber's
     *            remote interface the method simply prints the message received
     *            by the broker
     */
    @Override
    public void dispatchMessage(ToAllOutput toAllOutput) {
        input.dispatchToAllOutput(toAllOutput);
    }

}
