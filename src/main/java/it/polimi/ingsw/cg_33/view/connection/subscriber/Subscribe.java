package it.polimi.ingsw.cg_33.view.connection.subscriber;

public interface Subscribe {
    public void subscribe(String string);
}
