package it.polimi.ingsw.cg_33.view.gui.drawablesectors;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class EmptyDrawableHexagon extends DrawableHexagon {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private ImageIcon hexagonImage;
    private static final Logger LOGGER = Logger
            .getLogger(EmptyDrawableHexagon.class.getName());
    private static final String FS = File.separator;

    public EmptyDrawableHexagon(int hexagonWidth, int hexagonHeight) {
        // TODO Auto-generated constructor stub
        try {

            hexagonImage = new ImageIcon(ImageIO.read(
                    new File("src"+FS+"main"+FS+"resources"+FS+"swing"+FS+"sectors"+FS+"empty.png"))
                    .getScaledInstance(hexagonWidth,
                            hexagonHeight, Image.SCALE_DEFAULT));

        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Could not load EmptyDrawableHexagon image", e);
        }

        // Set the size of the JLabel
        setBounds(0, 0, hexagonWidth, hexagonHeight);
        setIcon(hexagonImage);
    }
}
