package it.polimi.ingsw.cg_33.view.cli;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.view.GraphicInterfaceIn;
import it.polimi.ingsw.cg_33.view.connection.subscriber.Subscribe;

import java.util.Observable;

public class CliIn implements GraphicInterfaceIn {

    private final CliOut cliOut;
    private Subscribe subscriber;
    private String player = "";
    public CliIn(CliOut cliOut, Subscribe subscriber) {
        this.cliOut = cliOut;
        this.subscriber = subscriber;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Output) {
            Output output = (Output) arg;
            outputParseAndPrint(output);
        } else if (arg instanceof ToAllOutput) {
            ToAllOutput toAllOutput = (ToAllOutput) arg;
            toAllOutputParseAndPrint(toAllOutput);
        }
    }

    private void toAllOutputParseAndPrint(ToAllOutput toAllOutput) {
        if (toAllOutput.getChatMsg() != null)
            cliOut.addMsgChat(toAllOutput.getChatMsg());
        if (toAllOutput.getDescription() != null && (!this.player.equals(toAllOutput.getCurrentPlayer()) || toAllOutput.getNumTurn() != 0 || !this.player.equals(toAllOutput.getChatPlayer())))
            System.out.println(toAllOutput.getDescription());
        if (toAllOutput.isMatchBegin()) {
            cliOut.requireInitialInformations();
        }
        if (toAllOutput.getCliMap() != null)
            System.out.println(toAllOutput.getCliMap());
    }

    private void outputParseAndPrint(Output output) {
        if (!output.getDescriptions().isEmpty())
            for (String description : output.getDescriptions())
                System.out.println(description);
        if(output.getLoginDescription()!=null)
            System.out.println(output.getLoginDescription());
        if (!output.getActionDescription().isEmpty())
            cliOut.setActionDescription(output.getActionDescription());
        if (output.getName() != null)
            player = output.getName();
        if (!output.getPossibleActions().isEmpty()){
            cliOut.setPossibleActions(output.getPossibleActions());
            System.out.println("COMANDI CONSIGLIATI: " + output.getPossibleActions());
        }
        if (output.getTopic() != null)
            subscriber.subscribe(output.getTopic());
        if (!output.getPlayersLog().isEmpty())
            System.out.println(output.getPlayersLog());
        if (output.isReconnectSuccess())
            cliOut.requireInitialInformations();
    }
}
