package it.polimi.ingsw.cg_33.view.connection.client;

import it.polimi.ingsw.cg_33.communication.Message;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class ConnectionManagerOutAbstract implements Runnable {

    private final Queue<Message> message = new ConcurrentLinkedQueue<Message>();

    protected Queue<Message> getMessage() {
        return message;
    }

    protected abstract void send(Message msg);

    /**
     * Questo metodo inserisce un messaggio nella coda e notifica ai thread in
     * attesa (in questo caso a se stesso) la presenza di un messaggio.
     * 
     * @param msg
     *            Il messaggio da inserire.
     */
    public void dispatchMessage(Message msg) {
        getMessage().add(msg);
        // é necessario sincronizzarsi sull'oggetto monitorato
        synchronized (getMessage()) {
            getMessage().notify();
        }
    }

}
