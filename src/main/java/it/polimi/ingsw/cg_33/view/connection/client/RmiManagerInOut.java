package it.polimi.ingsw.cg_33.view.connection.client;

import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.interfaces.LauncherInterface;
import it.polimi.ingsw.cg_33.view.input.Input;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RmiManagerInOut extends ConnectionManagerOutAbstract {

    private Registry registry;
    private Input input;

    private static final Logger LOGGER = Logger.getLogger(RmiManagerInOut.class
            .getName());
    private LauncherInterface launcher = null;

    public RmiManagerInOut() {
        try {
            input = new Input();
            registry = LocateRegistry.getRegistry(7779);
            launcher = (LauncherInterface) registry.lookup("Launcher");
        } catch (RemoteException | NotBoundException e) {
            LOGGER.log(Level.WARNING, "Rmi Exception", e);
        }
    }

    @Override
    public void run() {
        Message msg;
        while (true) {
            // Si prova ad estrarre un messaggio dalla coda...
            msg = getMessage().poll();
            // ... se c'é lo si invia
            if (msg != null)
                send(msg);
            else {
                // ... altrimenti, per evitare cicli inutili di CPU
                // che possono portare ad utilizzarla inutilmente...
                try {
                    // ... si aspetta fin quando la coda non conterrá qualcosa
                    // é necessario sincronizzarsi sull'oggetto monitorato, in
                    // modo tale
                    // che il thread corrente possieda il monitor sull'oggetto.
                    synchronized (getMessage()) {
                        getMessage().wait();
                    }
                } catch (InterruptedException e) {
                    LOGGER.log(
                            Level.WARNING,
                            "thread has been interrupted either before or during the activity",
                            e);
                }
            }
        }
    }

    @Override
    protected void send(Message msg) {
        try {
            input.dispatchOutput(launcher.launch(msg));
        } catch (RemoteException e) {
            LOGGER.log(Level.WARNING, "Rmi can't send messaege", e);
        }
    }

    /**
     * @return the input
     */
    public Input getInput() {
        return input;
    }
}
