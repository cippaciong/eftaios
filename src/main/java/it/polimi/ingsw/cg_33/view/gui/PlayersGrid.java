package it.polimi.ingsw.cg_33.view.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.File;

import javax.swing.JPanel;

import org.apache.batik.swing.JSVGCanvas;

public class PlayersGrid extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JSVGCanvas captain, pilot, psychologist, soldier, alien1, alien2,
            alien3, alien4;
    private static final String FS = File.separator;
    private static final String PLAYERBASEPATH = "src" + FS + "main" + FS
            + "resources" + FS + "swing" + FS + "players" + FS;

    public PlayersGrid() {
        super(new GridLayout(1, 0, 5, 0));

        setOpaque(false);

        loadButtons();
    }

    public void loadButtons() {

        captain = new JSVGCanvas();
        captain.setURI(PLAYERBASEPATH+"captain_inactive.svg");
        captain.setBackground(new Color(255, 255, 255, 0));

        pilot = new JSVGCanvas();
        pilot.setURI(PLAYERBASEPATH+"pilot_inactive.svg");
        pilot.setBackground(new Color(255, 255, 255, 0));

        psychologist = new JSVGCanvas();
        psychologist
                .setURI(PLAYERBASEPATH+"psychologist_inactive.svg");
        psychologist.setBackground(new Color(255, 255, 255, 0));

        soldier = new JSVGCanvas();
        soldier.setURI(PLAYERBASEPATH+"soldier_inactive.svg");
        soldier.setBackground(new Color(255, 255, 255, 0));

        alien1 = new JSVGCanvas();
        alien1.setURI(PLAYERBASEPATH+"alien1_inactive.svg");
        alien1.setBackground(new Color(255, 255, 255, 0));

        alien2 = new JSVGCanvas();
        alien2.setURI(PLAYERBASEPATH+"alien2_inactive.svg");
        alien2.setBackground(new Color(255, 255, 255, 0));

        alien3 = new JSVGCanvas();
        alien3.setURI(PLAYERBASEPATH+"alien3_inactive.svg");
        alien3.setBackground(new Color(255, 255, 255, 0));

        alien4 = new JSVGCanvas();
        alien4.setURI(PLAYERBASEPATH+"alien4_inactive.svg");
        alien4.setBackground(new Color(255, 255, 255, 0));

        add(captain);
        add(pilot);
        add(psychologist);
        add(soldier);
        add(alien1);
        add(alien2);
        add(alien3);
        add(alien4);
    }

    public void setImageFromString(String name, String status) {
        switch (name) {
        case "captain":
            captain.setURI(name + "_" + status + ".svg");
            break;
        case "pilot":
            pilot.setURI(name + "_" + status + ".svg");
            break;
        case "psychologist":
            psychologist.setURI(name + "_" + status + ".svg");
            break;
        case "soldier":
            soldier.setURI(name + "_" + status + ".svg");
            break;
        case "alien1":
            alien1.setURI(name + "_" + status + ".svg");
            break;
        case "alien2":
            alien2.setURI(name + "_" + status + ".svg");
            break;
        case "alien3":
            alien3.setURI(name + "_" + status + ".svg");
            break;
        case "alien4":
            alien4.setURI(name + "_" + status + ".svg");
            break;
        default:
            break;
        }

    }

    /**
     * @return the captain
     */
    public JSVGCanvas getCaptain() {
        return captain;
    }

    /**
     * @return the pilot
     */
    public JSVGCanvas getPilot() {
        return pilot;
    }

    /**
     * @return the psychologist
     */
    public JSVGCanvas getPsychologist() {
        return psychologist;
    }

    /**
     * @return the soldier
     */
    public JSVGCanvas getSoldier() {
        return soldier;
    }

    /**
     * @return the alien1
     */
    public JSVGCanvas getAlien1() {
        return alien1;
    }

    /**
     * @return the alien2
     */
    public JSVGCanvas getAlien2() {
        return alien2;
    }

    /**
     * @return the alien3
     */
    public JSVGCanvas getAlien3() {
        return alien3;
    }

    /**
     * @return the alien4
     */
    public JSVGCanvas getAlien4() {
        return alien4;
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(new Color(255, 255, 255, 0));
        Insets insets = getInsets();
        int x = insets.left;
        int y = insets.top;
        int width = getWidth() - (insets.left + insets.right);
        int height = getHeight() - (insets.top + insets.bottom);
        g.fillRect(x, y, width, height);
        super.paintComponent(g);
    }

}
