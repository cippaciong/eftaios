package it.polimi.ingsw.cg_33.view.gui.graphicelements;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;

import javax.swing.JButton;

public class HexagonalButton extends JButton {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Polygon p;
    private final double INSCRIBED_CIRCLE_DIAMETER;
    private int radius;
    private int side;
    private int halfside;

    public HexagonalButton(String label, int height) {
        super(label);

        INSCRIBED_CIRCLE_DIAMETER = 1.73205;
        radius = height / 2;
        side = (int) (height / INSCRIBED_CIRCLE_DIAMETER);
        halfside = side / 2;

        int x = 0;
        int y = 0;

        // x,y is the top left corner of the rectangle outside the hex
        int[] xPoly = { x + halfside, x + side + halfside,
                x + side + halfside + halfside, x + side + halfside,
                x + halfside, x };
        int[] yPoly = { y, y, y + radius, y + radius + radius,
                y + radius + radius, y + radius };

        p = new Polygon(xPoly, yPoly, 6);

        setBounds(100, 100, side * 2, height);
        setBorder(null);
        setContentAreaFilled(false);
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2.drawPolygon(p);
        if (getModel().isArmed()) {
            g.setColor(new Color(68, 170, 228));
            g2.fillPolygon(p);
        }
        if (getModel().isPressed()) {
            g.setColor(Color.orange);
            g2.fillPolygon(p);
        }
        // g.setColor(new Color(68, 170, 228));
        // g.setColor(new Color(231,32,105));
        g.setColor(Color.WHITE);
        g2.fillPolygon(p);
        g2.drawPolygon(p);
        super.paintComponent(g);
    }
}