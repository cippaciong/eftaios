package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.view.connection.client.ConnectionManagerOutAbstract;
import it.polimi.ingsw.cg_33.view.gui.graphicelements.HexagonalButton;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginPanel extends JPanel implements ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String clientId;
    private ConnectionManagerOutAbstract connectionManager;
    private JPanel inputPanel;
    private JTextField username;
    private JPasswordField password;
    private JButton loginButton, signupButton;
    private List<String> args;
    private String fontName = "Orbitron";

    public LoginPanel(String clientId,
            ConnectionManagerOutAbstract connectionManager) {
        super();
        setLayout(null);
        this.clientId = clientId;
        this.connectionManager = connectionManager;
        setOpaque(true);
        setBackground(Color.BLACK);
        loadInputAreas();
        add(inputPanel);

    }

    private void loadInputAreas() {

        int panelWidth = (int) (GameBoard.getWindowWidth() * 0.2);
        int panelHeight = (int) (GameBoard.getWindowHeight() * 0.4);

        inputPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        inputPanel.setOpaque(false);
        inputPanel.setBackground(new Color(255, 255, 255, 0));
        inputPanel.setBounds(GameBoard.getWindowWidth() / 2 - panelWidth / 2,
                GameBoard.getWindowHeight() / 2 - panelHeight / 2, panelWidth,
                panelHeight);

        username = new JTextField("username");
        username.setFont(new Font(fontName, Font.PLAIN, 16));
        c.gridheight = 1;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0.1;
        c.weightx = 0.1;
        c.gridx = 0;
        c.gridy = 0;
        inputPanel.add(username, c);

        password = new JPasswordField("password");
        password.setFont(new Font(fontName, Font.PLAIN, 16));
        c.gridx = 0;
        c.gridy = 1;
        inputPanel.add(password, c);

        loginButton = new HexagonalButton("Log In", panelHeight/4);
        loginButton.setFont(new Font(fontName, Font.PLAIN, 16));
        loginButton.setForeground(Color.white);
        loginButton.setActionCommand("login");
        loginButton.addActionListener(this);
        loginButton.setHorizontalTextPosition(JButton.CENTER);
        loginButton.setVerticalTextPosition(JButton.CENTER);
        c.weighty = 1;
        c.ipady = 80;
        c.ipadx = 80;
        c.gridx = 0;
        c.gridy = 2;
        inputPanel.add(loginButton, c);

        signupButton = new HexagonalButton("Sign Up", panelHeight/4);
        signupButton.setActionCommand("signup");
        signupButton.addActionListener(this);
        signupButton.setFont(new Font(fontName, Font.PLAIN, 16));
        signupButton.setForeground(Color.white);
        signupButton.setHorizontalTextPosition(JButton.CENTER);
        signupButton.setVerticalTextPosition(JButton.CENTER);
        c.gridx = 0;
        c.gridy = 3;
        inputPanel.add(signupButton, c);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        args = new ArrayList<String>();
        String user = username.getText();
        String pwd = String.valueOf(password.getPassword());
        args.add(user);
        args.add(pwd);
        Message msg = new Message(clientId, e.getActionCommand(), args);
        connectionManager.dispatchMessage(msg);
    }

}
