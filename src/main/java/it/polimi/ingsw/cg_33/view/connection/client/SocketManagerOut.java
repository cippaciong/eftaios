package it.polimi.ingsw.cg_33.view.connection.client;

import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.view.input.Input;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

public class SocketManagerOut extends ConnectionManagerOutAbstract {

    private static final Logger LOGGER = Logger.getLogger(SocketManagerOut.class.getName());

    private PrintWriter socketOut;
    private Input input;
    private Socket socket;
    private static final String IP = "127.0.0.1";
    private static final int PORT = 7777;

    public SocketManagerOut(Input input) throws IOException {
        this.input = input;
    }

    @Override
    public void run() {
        Message msg;
        while (true) {
            // Si prova ad estrarre un messaggio dalla coda...
            msg = getMessage().poll();
            // ... se c'é lo si invia
            if (msg != null) {

                try {
                    socket = new Socket(IP, PORT);
                    socketOut = (new PrintWriter(socket.getOutputStream()));
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "An error occurred creating the socket", e);
                }
                new SocketManagerIn(socket, input).start();
                send(msg);
            } else {
                // ... altrimenti, per evitare cicli inutili di CPU
                // che possono portare ad utilizzarla inutilmente...
                try {
                    // ... si aspetta fin quando la coda non conterrá qualcosa
                    // é necessario sincronizzarsi sull'oggetto monitorato, in
                    // modo tale
                    // che il thread corrente possieda il monitor sull'oggetto.
                    synchronized (getMessage()) {
                        getMessage().wait();
                    }
                } catch (InterruptedException e) {
                    LOGGER.log(Level.WARNING, "thread has been interrupted either before or during the activity", e);
                }
            }
        }
    }

    @Override
    protected void send(Message msg) {
        Gson gson = new Gson();
        String out = gson.toJson(msg);
        socketOut.println(out);
        socketOut.flush();
    }

}
