package it.polimi.ingsw.cg_33.view.gui.drawablesectors;

import it.polimi.ingsw.cg_33.view.gui.helpers.LoaderSVG;

import java.awt.Image;
import java.io.File;

import javax.swing.ImageIcon;

public class AlienStartDrawableHexagon extends DrawableHexagon {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Image hexagonImage;
    private static final String FS = File.separator;

    public AlienStartDrawableHexagon(int hexagonWidth, int hexagonHeight) {

        hexagonImage = LoaderSVG.loadSVGFromFile(
                "src"+FS+"main"+FS+"resources"+FS+"swing"+FS+"sectors"+FS+"alienstart.svg",
                (float) hexagonWidth, (float) hexagonHeight);

        // Set the size of the JLabel
        setBounds(0, 0, hexagonWidth, hexagonHeight);
        setIcon(new ImageIcon(hexagonImage));
    }

}
