package it.polimi.ingsw.cg_33.view.gui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;

public class ItemButton extends JButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String itemName;

    public ItemButton(String itemName, Icon buttonIcon) {
        super(itemName, buttonIcon);
        this.itemName = itemName;

        loadComponentSettings(itemName);
    }

    private void loadComponentSettings(String itemName) {
        setBorder(null);
        setContentAreaFilled(false);
        setBorderPainted(false);
        setOpaque(false);
        setBackground(new Color(255, 255, 255, 0));
        setForeground(Color.WHITE);
        setFont(new Font("Orbitron", Font.PLAIN, 12));
        setHorizontalTextPosition(JLabel.CENTER);
        setVerticalTextPosition(JLabel.BOTTOM);
        if ("Attack".equals(itemName)) {
            setActionCommand("Attackitem");
        } else {
            setActionCommand(itemName);
        }
    }

    /**
     * @return the actionName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName
     *            the actionName to set
     */
    public void setActionName(String itemName) {
        this.itemName = itemName;
    }

}
