package it.polimi.ingsw.cg_33.view.gui;

import it.polimi.ingsw.cg_33.model.sector.ExportSector;
import it.polimi.ingsw.cg_33.view.gui.drawablesectors.AlienStartDrawableHexagon;
import it.polimi.ingsw.cg_33.view.gui.drawablesectors.DangerousDrawableHexagon;
import it.polimi.ingsw.cg_33.view.gui.drawablesectors.DrawableHexagon;
import it.polimi.ingsw.cg_33.view.gui.drawablesectors.EmptyDrawableHexagon;
import it.polimi.ingsw.cg_33.view.gui.drawablesectors.EscapeHatchDrawableHexagon;
import it.polimi.ingsw.cg_33.view.gui.drawablesectors.HumanStartDrawableHexagon;
import it.polimi.ingsw.cg_33.view.gui.drawablesectors.SecureDrawableHexagon;
import it.polimi.ingsw.cg_33.view.gui.helpers.LoaderSVG;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MapPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Map<Point, DrawableHexagon> hexagons;
    private SelectionController selectionController;
    private int hexagonWidth, hexagonHeight, hexagonSide, hexagonRadius,
            hexagonHalfside;
    private int borderX, borderY;
    private JLayeredPane layeredPane;
    private ActionPopup actionPopup;
    private JLabel glassLabel;
    private JLabel pieceLabel;
    private JLabel noiseLabel;
    private List<JLabel> playersIcon = new ArrayList<JLabel>();
    public static final int HIDDEN_LAYER = 5;
    public static final int HEXAGON_LAYER = 10;
    public static final int PLAYER_LAYER = 15;
    public static final int POPUP_LAYER = 20;
    private static final String FS = File.separator;
    private static final String PLAYERBASEPATH = "src" + FS + "main" + FS
            + "resources" + FS + "swing" + FS + "players" + FS;
    private static final String NOISEBASEPATH = "src" + FS + "main" + FS
            + "resources" + FS + "swing" + FS + "misc" + FS;
    private static final String NAMEBASEPATH = "src" + FS + "main" + FS
            + "resources" + FS + "swing" + FS + "names" + FS;

    public MapPanel() {
        super();
        setLayout(null);
        setOpaque(false);
        setBackground(new Color(255, 255, 255, 0));

    }

    public void populateWithHexagons(String jsonMap, int panelWidth,
            int panelHeight) {
        layeredPane = new JLayeredPane();
        layeredPane.setSize(new Dimension(this.getWidth(), this.getHeight()));
        add(layeredPane);

        computeHexagonDimensionsAndBorders(panelWidth, panelHeight);

        Map<Point, DrawableHexagon> hexagonMap = new HashMap<Point, DrawableHexagon>();

        hexagonMap = generateMapFromJson(jsonMap);
        for (Entry<Point, DrawableHexagon> pointHex : hexagonMap.entrySet()) {
            DrawableHexagon d = pointHex.getValue();
            Point coord = pointHex.getKey();
            layeredPane.add(d);
            layeredPane.setLayer(d, HEXAGON_LAYER);

            int x = (int) coord.getX();
            int y = (int) coord.getY();
            int hx = x * (hexagonSide + hexagonHalfside);
            int hy = y * hexagonHeight + (x % 2) * (hexagonHeight / 2);
            d.setBounds(hx + borderX, hy + borderY, hexagonWidth, hexagonHeight);
            hexagons.put(new Point(x, y), d);
        }

        initActionPopup();
        initGlassLayer();
        initNoiseLabel();
        initPlayersIcon();

        selectionController = new SelectionController(hexagons, borderX,
                borderY, actionPopup, layeredPane);
        addMouseListener(selectionController);
    }

    private void initActionPopup() {
        actionPopup = new ActionPopup();
        layeredPane.add(actionPopup);
        layeredPane.setLayer(actionPopup, 0);

    }

    private void initGlassLayer() {
        glassLabel = new JLabel();
        glassLabel.setOpaque(true);
        glassLabel.setBackground(new Color(255, 255, 255, 0));
        glassLabel.addMouseListener(selectionController);
        glassLabel.setBounds(0, 0, layeredPane.getWidth(),
                layeredPane.getHeight());
        layeredPane.add(glassLabel);
        layeredPane.setLayer(glassLabel, HIDDEN_LAYER);
    }

    private void initPlayersIcon() {
        for (int i = 0; i < 8; i++) {
            Image image = LoaderSVG.loadSVGFromFile(
                    NAMEBASEPATH + Integer.toString(i) + ".svg",
                    (float) hexagonWidth * 0.6f, (float) hexagonHeight * 0.6f);
            playersIcon.add(new JLabel(new ImageIcon(image), JLabel.CENTER));
        }
        for (JLabel label : playersIcon) {
            label.setBounds(0, 0, hexagonWidth, hexagonHeight);
            layeredPane.add(label);
            layeredPane.setLayer(label, HIDDEN_LAYER);
        }
    }
    
    public void movePlayerIcon(int player, Point sectorCoord) {
        JLabel playerLabel = playersIcon.get(player);
        if(JLayeredPane.getLayer(playerLabel) == HIDDEN_LAYER)
            layeredPane.setLayer(playerLabel, PLAYER_LAYER);
        DrawableHexagon hexagon = hexagons.get(sectorCoord);
        playerLabel.setLocation(hexagon.getLocation());
    }

    public void initPieceLabel(String characterName) {
        Image pieceImage = LoaderSVG.loadSVGFromFile(PLAYERBASEPATH
                + characterName + "_active.svg", (float) hexagonWidth * 0.6f,
                (float) hexagonHeight * 0.6f);
        pieceLabel = new JLabel(new ImageIcon(pieceImage), JLabel.CENTER);
        pieceLabel.setBounds(0, 0, hexagonWidth, hexagonHeight);
        layeredPane.add(pieceLabel);
        layeredPane.setLayer(pieceLabel, PLAYER_LAYER);
    }

    public void movePieceLabel(Point coords) {
        System.err.println(noiseLabel.equals(pieceLabel));
        DrawableHexagon hexagon = hexagons.get(coords);
        pieceLabel.setLocation(hexagon.getLocation());
    }

    public void initNoiseLabel() {
        Image noiseImage = LoaderSVG.loadSVGFromFile(NOISEBASEPATH
                + "noise.svg", (float) hexagonWidth * 0.6f,
                (float) hexagonHeight * 0.6f);
        noiseLabel = new JLabel(new ImageIcon(noiseImage), JLabel.CENTER);
        noiseLabel.setBounds(0, 0, hexagonWidth, hexagonHeight);
        layeredPane.add(noiseLabel);
        layeredPane.setLayer(noiseLabel, PLAYER_LAYER);
    }

    public void moveNoiseLabel(Point coords) {
        DrawableHexagon hexagon = hexagons.get(coords);
        noiseLabel.setLocation(hexagon.getLocation());
    }

    public void markPosition(String username, Point coords) {

    }

    private void computeHexagonDimensionsAndBorders(int panelWidth,
            int panelHeight) {
        hexagons = new HashMap<Point, DrawableHexagon>();
        hexagonWidth = (int) (panelWidth / (23f - 23f / 4f));
        hexagonHeight = (int) (Math.sqrt(3) / 2 * hexagonWidth);
        hexagonSide = (int) (hexagonHeight / 1.73205);
        hexagonRadius = hexagonHeight / 2;
        hexagonHalfside = (int) (hexagonRadius / 1.73205);
        borderX = (int) (panelWidth - hexagonWidth * (23f - 23f / 4f)) / 2;
        borderY = (panelHeight - (hexagonHeight * 14 + hexagonHeight / 2)) / 2;
    }

    private Map<Point, DrawableHexagon> generateMapFromJson(String jsonMap) {
        Gson gson = new Gson();
        Type collectionType = new TypeToken<Set<ExportSector>>() {
        }.getType();
        Set<ExportSector> exportedSectorSet = gson.fromJson(jsonMap,
                collectionType);
        Map<Point, DrawableHexagon> mapFromJson = new HashMap<Point, DrawableHexagon>();
        for (ExportSector es : exportedSectorSet) {
            int col = es.getCol();
            int row = es.getRow();
            String className = es.getSectorType();
            switch (className) {
            case "AlienStartingSector":
                mapFromJson.put(new Point(col, row),
                        new AlienStartDrawableHexagon(hexagonWidth,
                                hexagonHeight));
                break;
            case "DangerousSector":
                mapFromJson.put(new Point(col, row),
                        new DangerousDrawableHexagon(hexagonWidth,
                                hexagonHeight));
                break;
            case "EscapeHatchSector":
                mapFromJson.put(new Point(col, row),
                        new EscapeHatchDrawableHexagon(hexagonWidth,
                                hexagonHeight));
                break;
            case "HumanStartingSector":
                mapFromJson.put(new Point(col, row),
                        new HumanStartDrawableHexagon(hexagonWidth,
                                hexagonHeight));
                break;
            case "SecureSector":
                mapFromJson.put(new Point(col, row), new SecureDrawableHexagon(
                        hexagonWidth, hexagonHeight));
                break;
            default:
                mapFromJson.put(new Point(col, row), new EmptyDrawableHexagon(
                        hexagonWidth, hexagonHeight));
                break;
            }
        }
        return mapFromJson;
    }

    /**
     * @return the hexagonHeight
     */
    public int getHexagonHeight() {
        return hexagonHeight;
    }

    /**
     * @return the actionPopup
     */
    public ActionPopup getActionPopup() {
        return actionPopup;
    }

    /**
     * @return the selectionController
     */
    public SelectionController getSelectionController() {
        return selectionController;
    }
}
