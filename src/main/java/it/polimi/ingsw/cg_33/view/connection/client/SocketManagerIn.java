package it.polimi.ingsw.cg_33.view.connection.client;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.view.input.Input;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

/**
 * manages the incoming connection of the client, that is the messages sent from
 * the server to the client
 */
public class SocketManagerIn extends Thread {

    /**
     * contains the Scanner which is used to read messages from the server
     */
    private Socket socket;
    private Scanner socketIn;
    private Input input;
    private static final Logger LOGGER = Logger.getLogger(SocketManagerIn.class.getName());

    /**
     * creates a new Handler for the incoming connections
     * 
     * @param socketIn
     *            is the scanner which is used to read messages that are sent
     *            from the server
     */
    public SocketManagerIn(Socket socket, Input input) {
        this.socket = socket;
        this.input = input;
        try {
            this.socketIn = new Scanner(socket.getInputStream());
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Can't create socketin", e);
        }
    }

    /**
     * executes the client handler
     */
    @Override
    public void run() {

        boolean running = true;

        while (running) {
            // reads a new Line from the Scanner
            if (socketIn.hasNextLine()) {
                String line = socketIn.nextLine();
                Gson gson = new Gson();
                input.dispatchOutput(gson.fromJson(line, Output.class));
                running = false;
                try {
                    socket.close();
                } catch (IOException e) {
                    LOGGER.log(Level.WARNING, "Can't close the socket", e);
                }
            }
        }
    }

    /**
     * @return the input
     */
    public Input getInput() {
        return input;
    }
}