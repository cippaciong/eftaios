package it.polimi.ingsw.cg_33.view.cli;

import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.view.connection.client.ConnectionManagerOutAbstract;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class CliOut extends Thread {

    boolean running = true;
    private String clientId = null;
    private String action = null;
    private List<String> args = new ArrayList<>();
    ConnectionManagerOutAbstract connectionOut;
    private Map<String, List<String>> actionDescription;
    private List<String> chat = new ArrayList<>();
    private Set<String> possibleActions = new HashSet<>();

    public CliOut(String clientId, ConnectionManagerOutAbstract connectionOut)
            throws IOException {
        this.clientId = clientId;
        this.connectionOut = connectionOut;
        Message message = new Message(clientId, "actionDescription");
        connectionOut.dispatchMessage(message);
    }

    /**
     * executes the client handler
     */
    @Override
    public void run() {
        // creates a new scanner to read input from standard input
        Scanner stdin = new Scanner(System.in);
        while (running) {
            // read the string from the standard input
            // action
            askValidCommand(stdin);
            if (!actionDescription.get(action).isEmpty()) {
                args.clear();
                for (String description : actionDescription.get(action)) {
                    System.out.println(description);
                    args.add(stdin.nextLine());
                }
            }
            Message message = new Message(clientId, action, args);
            connectionOut.dispatchMessage(message);
        }
        stdin.close();
    }

    private void askValidCommand(Scanner stdin) {
        String line;
        do {
            System.out.println();
            System.out.println("Dimmi cosa vuoi fare");
            line = stdin.nextLine().replaceAll("[^a-zA-Z]", "");
            if ("quit".equalsIgnoreCase(line))
                running = false;
            else if ("openChat".equals(line))
                System.out.println(chat);
            else if (!actionDescription.keySet().contains(line)) {
                System.out.println("Comando non valido, seleziona uno dei comandi seguenti: ");
                System.out.println(actionDescription.keySet().toString());
                if (!possibleActions.isEmpty())
                    System.out.println("COMANDI CONSIGLIATI: " + possibleActions);
            }
        } while (!(actionDescription.keySet().contains(line) || "quit".equals(line)));
        action = line;
    }

    public void addMsgChat(String msg) {
        this.chat.add(msg);
    }

    public void setActionDescription(Map<String, List<String>> actionDescription) {
        this.actionDescription = actionDescription;
    }

    public void requireInitialInformations() {
        Message message = new Message(clientId, "getinitialinformations");
        connectionOut.dispatchMessage(message);
    }

    public Set<String> getPossibleActions() {
        return possibleActions;
    }

    public void setPossibleActions(Set<String> possibleActions) {
        this.possibleActions = possibleActions;
    }

}
