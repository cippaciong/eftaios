package it.polimi.ingsw.cg_33.model.sector;

public class HumanStartingSector extends Sector {

    public HumanStartingSector(int col, int row) {
        super(col, row);
    }

    @Override
    public String toOneDescription() {
        return "Sei sul settore di partenza degli umani";
    }

    @Override
    public String getLinkedPossibleAction() {
        return "move";
    }

}
