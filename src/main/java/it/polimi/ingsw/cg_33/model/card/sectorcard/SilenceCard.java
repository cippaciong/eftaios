package it.polimi.ingsw.cg_33.model.card.sectorcard;

/**
 * <p>
 * A card that allow you to make silence
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class SilenceCard extends DangerousSectorCard {

    public SilenceCard() {
        super(false);
        description = "Bravo non hai fatto rumore, gli altri non ti hanno notato";
    }

    @Override
    public String getLinkedPossibleAction() {
        return "endturn";
    }
}
