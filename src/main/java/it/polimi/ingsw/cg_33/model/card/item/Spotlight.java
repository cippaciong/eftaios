package it.polimi.ingsw.cg_33.model.card.item;

/**
 * <p>
 * A simple Item that will be put in a deck 
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Spotlight extends Item {

    public Spotlight() {
        super("choose a sector and the spotlight show you everyone there and in near secotrs");
    }

    @Override
    public String toOneDescription() {
        return "Ok la torcia è in azione, a breve scopriremo chi hai avvistato!";
    }

    @Override
    public String toAllDescription() {
        return " ha acceso la torcia!";
    }
}
