package it.polimi.ingsw.cg_33.model.sector;

public class EscapeHatchSector extends Sector {

    private boolean isActive = true;

    public EscapeHatchSector(int col, int row) {
        super(col, row);
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toOneDescription() {
        if (isActive)
            return "Sei su una scialuppa!";
        else
            return "Non sei il primo ad arrivare alla scialuppa, mi spiace effettua una mossa diversa!";
    }

    @Override
    public String getLinkedPossibleAction() {
        return "endTurn";
    }
}
