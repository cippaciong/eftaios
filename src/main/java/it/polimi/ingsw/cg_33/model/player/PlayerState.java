package it.polimi.ingsw.cg_33.model.player;

public enum PlayerState {

    /**
     * Is Player's turn to play
     */
    ACTIVE,
    /**
     * Isn't Player's turn to play
     */
    INACTIVE,
    /**
     * Human that reach the hatch and fly away so is one of the winner
     * Characters
     */
    ESCAPED,
    /**
     * Characters that has been killed so has lost the game
     */
    DEAD

}
