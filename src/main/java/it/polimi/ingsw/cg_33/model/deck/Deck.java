package it.polimi.ingsw.cg_33.model.deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 * A simple Deck superClass of all Decks
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Deck {
    private List<Object> cards = new ArrayList<Object>();

    public Object draw() {
        Object card = cards.get(0);
        cards.remove(0);
        return card;
    }

    public List<Object> getCards() {
        return cards;
    }

    public void clone(List<Object> discardedCards) {
        Collections.shuffle(discardedCards);
        this.getCards().addAll(discardedCards);
    }

}
