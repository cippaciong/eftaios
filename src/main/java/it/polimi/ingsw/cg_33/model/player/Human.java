package it.polimi.ingsw.cg_33.model.player;

import it.polimi.ingsw.cg_33.model.card.item.Defence;
import it.polimi.ingsw.cg_33.model.map.GameMap;
import it.polimi.ingsw.cg_33.model.sector.Sector;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 * The Human Player
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Human extends Player {

    private Human(Sector start, String title) {
        this.setSpeed(1);
        this.setPosition(start);
        this.setPlayerTitle(title);
    }

    protected static List<Player> createHumansList(int num, Sector start) {
        List<Player> humans = new ArrayList<>();
        humans.add(new Human(start, "captain"));
        humans.add(new Human(start, "pilot"));
        humans.add(new Human(start, "psychologist"));
        humans.add(new Human(start, "soldier"));
        return humans.subList(0, num);
    }

    @Override
    public boolean hit() {
        if (items.contains(new Defence())) {
            removeItem(new Defence());
            return false;
        } else
            return true;
    }

    @Override
    public boolean canMoveTo(GameMap map, Sector destination) {
        if (map.isDistanceReachable(getSpeed(), getPosition(), destination)) {
            setSpeed(1);
            return true;
        }
        return false;
    }

}
