package it.polimi.ingsw.cg_33.model.player;

import it.polimi.ingsw.cg_33.model.map.GameMap;
import it.polimi.ingsw.cg_33.model.sector.Sector;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * The Alien Player
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Alien extends Player {

    private Alien(Sector start, String title) {
        this.setSpeed(2);
        this.setPosition(start);
        this.setPlayerTitle(title);
    }

    protected static List<Player> createAliensList(int num, Sector start) {
        List<Player> aliens = new ArrayList<>();
        aliens.add(new Alien(start, "alien1"));
        aliens.add(new Alien(start, "alien2"));
        aliens.add(new Alien(start, "alien3"));
        aliens.add(new Alien(start, "alien4"));
        return aliens.subList(0, num);
    }

    @Override
    public boolean canMoveTo(GameMap map, Sector destination) {
        return map.isDistanceReachable(getSpeed(), getPosition(), destination);
    }

}
