package it.polimi.ingsw.cg_33.model.sector;

import it.polimi.ingsw.cg_33.model.HumanCommunicationInterface;
import it.polimi.ingsw.cg_33.model.map.Cube;

public abstract class Sector implements HumanCommunicationInterface {

    private int col;
    private int row;

    Sector(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public Cube toCube() {
        Cube cube;
        int x = this.col;
        int z = this.row - (col - (col % 2)) / 2;
        int y = -x - z;
        cube = new Cube(x, y, z);
        return cube;
    }

    public abstract String getLinkedPossibleAction();

    @Override
    public String toAllDescription() {
        return " si è mosso";
    }
    
    @Override
    public String toString(){
        return this.col + "-" + this.row;
    }
}