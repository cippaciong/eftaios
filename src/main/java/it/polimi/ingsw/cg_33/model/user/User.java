package it.polimi.ingsw.cg_33.model.user;

/**
 * <p>
 * The User
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class User {

    private String myId; 
    private final String name;

    public User(String clientId, String name) {
        this.myId = clientId;
        this.name = name;
    }

    public String getMyId() {
        return myId;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

}
