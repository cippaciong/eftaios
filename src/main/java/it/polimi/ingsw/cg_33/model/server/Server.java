package it.polimi.ingsw.cg_33.model.server;

import it.polimi.ingsw.cg_33.controller.match.MatchController;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Server {

    private static final Server INSTANCE = new Server();
    private final ConcurrentMap<String, MatchController> userIdMatchControllerMap = new ConcurrentHashMap<String, MatchController>();
    private final ConcurrentMap<String, String> usernamePasswordMap = new ConcurrentHashMap<String, String>();
    private final ConcurrentMap<String, String> clientidUsernameMap = new ConcurrentHashMap<String, String>();

    private Server() {
        // disable the constructor to implement the singleton pattern
    }

    /**
     * @return the instance
     */
    public static Server getInstance() {
        return INSTANCE;
    }

    /**
     * @return the matchUsersMap
     */
    public ConcurrentMap<String, MatchController> getUserIdMatchControllerMap() {
        return userIdMatchControllerMap;
    }

    public ConcurrentMap<String, String> getUsernamePasswordMap() {
        return usernamePasswordMap;
    }

    public ConcurrentMap<String, String> getClientidUsernameMap() {
        return clientidUsernameMap;
    }
}
