package it.polimi.ingsw.cg_33.model.card.sectorcard;

import it.polimi.ingsw.cg_33.model.HumanCommunicationInterface;

/**
 * <p>
 * An abstract card that is superClass of all sectirCards
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public abstract class DangerousSectorCard implements
        HumanCommunicationInterface {

    protected String description;

    protected boolean hasItem;

    public DangerousSectorCard(boolean hasItem) {
        this.hasItem = hasItem;
    }

    public boolean getHasItem() {
        return hasItem;
    }

    public abstract String getLinkedPossibleAction();

    @Override
    public String toAllDescription() {
        return null;
    }

    @Override
    public String toOneDescription() {
        return description;
    }

}
