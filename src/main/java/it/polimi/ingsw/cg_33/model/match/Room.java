package it.polimi.ingsw.cg_33.model.match;

import it.polimi.ingsw.cg_33.model.map.GameMap;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * A simple Waiting Room to put users before match starting
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Room {

    private final List<User> users = new ArrayList<User>();
    private String topic;
    private GameMap map;
    
    public Room(String topic) {
        this.topic = topic;
    }

    /**
     * @return the topic
     */
    public synchronized String getTopic() {
        return topic;
    }

    /**
     * @return the users
     */
    public synchronized List<User> getUsers() {
        return users;
    }

    public synchronized int getNumberOfUsers() {
        return users.size();
    }

    public synchronized void addUser(User user) {
        this.users.add(user);
    }

    public synchronized void removeUser(User user) {
        this.users.remove(user);
    }

    public GameMap getMap() {
        return map;
    }

    public void setMap(GameMap map) {
        this.map = map;
    }

}
