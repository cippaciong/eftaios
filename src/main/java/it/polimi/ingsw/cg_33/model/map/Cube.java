package it.polimi.ingsw.cg_33.model.map;

import java.awt.Point;

public class Cube {

    private int x;
    private int y;
    private int z;

    public Cube(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int distance(Cube a) {
        return Math.max(
                Math.abs(this.x - a.getX()),
                Math.max(Math.abs(this.y - a.getY()),
                        Math.abs(this.z - a.getZ())));
    }

    public Cube add(Cube a) {
        this.x = this.x + a.getX();
        this.y = this.y + a.getY();
        this.z = this.z + a.getZ();

        return this;
    }

    public Point toOffset() {
        int col = this.x;
        int row = this.z + (x - (x % 2)) / 2;
        return new Point(col, row);
    }

    public Point toAxial() {
        int xAxial = this.x;
        int zAxial = this.z;
        return new Point(xAxial, zAxial);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cube other = (Cube) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        if (z != other.z)
            return false;
        return true;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @return the z
     */
    public int getZ() {
        return z;
    }

}
