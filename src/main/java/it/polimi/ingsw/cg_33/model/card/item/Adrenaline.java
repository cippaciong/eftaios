package it.polimi.ingsw.cg_33.model.card.item;

/**
 * <p>
 * A simple Item that will be put in a deck 
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Adrenaline extends Item {
    
    public Adrenaline() {
        super("This card make you faster!");
    }

    @Override
    public String toOneDescription() {
        return "Ora puoi muovere di 2";
    }

    @Override
    public String toAllDescription() {
        return " ha usato l'adrenalina dunque può muovere di due per una mossa";
    }

}
