package it.polimi.ingsw.cg_33.model.match;

/**
 * <p>
 * A enum that represent game state
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public enum GameState {
    RUNNING, END

}
