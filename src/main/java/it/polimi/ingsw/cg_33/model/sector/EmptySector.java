package it.polimi.ingsw.cg_33.model.sector;


public class EmptySector extends Sector {

    public EmptySector(int col, int row) {
        super(col, row);
    }

    @Override
    public String toOneDescription() {
        return " non puoi muovere su un settore vuoto";
    }

    @Override
    public String getLinkedPossibleAction() {
        return null;
    }
}
