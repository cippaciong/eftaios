package it.polimi.ingsw.cg_33.model;

public interface HumanCommunicationInterface {

    public String toOneDescription();

    public String toAllDescription();
}
