package it.polimi.ingsw.cg_33.model.sector;

public class DangerousSector extends Sector {

    public DangerousSector(int col, int row) {
        super(col, row);
    }

    @Override
    public String toOneDescription() {
        return "Sei su un settore pericoloso puoi scegliere se pescare la carta o attaccare";
    }

    @Override
    public String getLinkedPossibleAction() {
        return "draw";
    }
}
