package it.polimi.ingsw.cg_33.model.card.escapehatchcard;

import it.polimi.ingsw.cg_33.model.HumanCommunicationInterface;

/**
 * <p>
 * The super abstract class of green and red card.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public abstract class EscapeHatchCard implements HumanCommunicationInterface {

    private boolean win;
   
    public boolean isWin() {
        return win;
    }

    protected void setWin(boolean win) {
        this.win = win;
    }
}
