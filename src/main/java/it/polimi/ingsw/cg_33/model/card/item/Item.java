package it.polimi.ingsw.cg_33.model.card.item;

import it.polimi.ingsw.cg_33.model.HumanCommunicationInterface;

/**
 * <p>
 * A simple Item that will be put in a deck 
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public abstract class Item implements HumanCommunicationInterface {

    private String description;

    public Item(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Item other = (Item) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        return true;
    }

}
