package it.polimi.ingsw.cg_33.model.deck;

import it.polimi.ingsw.cg_33.model.card.item.Adrenaline;
import it.polimi.ingsw.cg_33.model.card.item.Attack;
import it.polimi.ingsw.cg_33.model.card.item.Defence;
import it.polimi.ingsw.cg_33.model.card.item.Sedatives;
import it.polimi.ingsw.cg_33.model.card.item.Spotlight;
import it.polimi.ingsw.cg_33.model.card.item.Teleport;

import java.util.Collections;

/**
 * <p>
 * A simple Deck of items
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class DeckOfItems extends Deck {

    public DeckOfItems() {
        for (int i = 0; i < 2; i++)
            super.getCards().add(new Attack());
        for (int i = 0; i < 2; i++)
            super.getCards().add(new Spotlight());
        for (int i = 0; i < 1; i++)
            super.getCards().add(new Defence());
        for (int i = 0; i < 3; i++)
            super.getCards().add(new Sedatives());
        for (int i = 0; i < 2; i++)
            super.getCards().add(new Adrenaline());
        for (int i = 0; i < 2; i++)
            super.getCards().add(new Teleport());
        Collections.shuffle(getCards());
    }
}
