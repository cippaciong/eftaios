package it.polimi.ingsw.cg_33.model.map;

import it.polimi.ingsw.cg_33.GetPixelColor;
import it.polimi.ingsw.cg_33.model.sector.AlienStartingSector;
import it.polimi.ingsw.cg_33.model.sector.EmptySector;
import it.polimi.ingsw.cg_33.model.sector.ExportSector;
import it.polimi.ingsw.cg_33.model.sector.HumanStartingSector;
import it.polimi.ingsw.cg_33.model.sector.Sector;

import java.awt.Point;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dk.ilios.asciihexgrid.AsciiBoard;
import dk.ilios.asciihexgrid.printers.SmallFlatAsciiHexPrinter;

/**
 * GameMap is the class containing all the map sectors and some helper methods
 * to work with these sectors such as find near sectors {@link #sectorNeighbors(Sector) sectorNeighbors}
 * or calculate distances {@link #isDistanceReachable(int, Sector, Sector) isDistanceReachable}.
 * Other methods allow to export the map in JSON format {@link #jsonizeMap() jsonizeMap}
 * or create and ASCII version of the map {@link #printMapToCli() printMapToCli}.
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class GameMap {

    private String name;
    private Map<Point, Sector> map = new HashMap<Point, Sector>();
    private Set<Cube> neighbors = new HashSet<Cube>();
    private static final Logger LOGGER = Logger.getLogger(GameMap.class
            .getName());
    private Sector humanStartSector;
    private Sector alienStartSector;

    public GameMap(String name) {
        try {
            this.map = GetPixelColor.generateMapFromImage(name);
        } catch (IOException e) {
            LOGGER.log(
                    Level.SEVERE,
                    "An error occurred while trying to generate the map from the image",
                    e);
        }
        for (Sector s : map.values()) {
            if (s instanceof AlienStartingSector)
                this.alienStartSector = s;
            else if (s instanceof HumanStartingSector)
                this.humanStartSector = s;
        }
        this.name = name;
        neighbors = populateNeighbors();
    }

    private Set<Cube> populateNeighbors() {
        neighbors.add(new Cube(+1, -1, 0));
        neighbors.add(new Cube(+1, 0, -1));
        neighbors.add(new Cube(0, +1, -1));
        neighbors.add(new Cube(-1, +1, 0));
        neighbors.add(new Cube(-1, 0, +1));
        neighbors.add(new Cube(0, -1, +1));

        return neighbors;
    }

    /**
     * Returns true if, from a starting sector, you can reach a destination sector
     * having a maximum number of sectors you can cross
     *
     * @param  playerMaxMovement the number of sector the player can cross
     * @param  start the starting sector
     * @param  destination the destination sector
     * @return      true if the player can reach the destination sector
     */
    public boolean isDistanceReachable(int playerMaxMovement, Sector start,
            Sector destination) {
        if (playerMaxMovement < plainDistance(start, destination))
            return false;
        else
            switch (playerMaxMovement) {
            case 1:
                return isDistanceOneSector(start, destination);

            case 2:
                return isDistanceOneSector(start, destination)
                        || isDistanceTwoSector(start, destination);

            case 3:
                return isDistanceOneSector(start, destination)
                        || isDistanceTwoSector(start, destination)
                        || isDistanceThreeSector(start, destination);

            default:
                return false;
            }

    }

    private int plainDistance(Sector start, Sector destination) {
        return start.toCube().distance(destination.toCube());
    }

    private boolean isDistanceOneSector(Sector start, Sector destination) {
        if (!(destination instanceof EmptySector)
                && plainDistance(start, destination) == 1)
            return true;
        return false;
    }

    private boolean isDistanceTwoSector(Sector start, Sector destination) {
        for (Sector sec : sectorNeighbors(start)) {
            if (isDistanceOneSector(sec, destination))
                return true;
        }
        return false;
    }

    private boolean isDistanceThreeSector(Sector start, Sector destination) {
        for (Sector sec : sectorNeighbors(start)) {
            if (isDistanceTwoSector(sec, destination))
                return true;
        }
        return false;
    }

    /**
     * Returns a list containing all the sectors adjacent to the
     * sector passed as argument, excluding empty sectors
     *
     * @param  start the sector whose neighbors you want to find
     * @return      true if the player can reach the destination sector
     */
    public Set<Sector> sectorNeighbors(Sector start) {
        Set<Sector> sectorNeighbors = new HashSet<Sector>();

        for (Cube neigh : neighbors) {
            Cube sum = start.toCube().add(neigh);
            Point key = sum.toOffset();
            if (!(this.getSector(key) instanceof EmptySector)
                    && this.getSector(key) != null)
                sectorNeighbors.add(this.getSector(key));
        }
        return sectorNeighbors;
    }

    /**
     * Returns the current map in ASCII code
     *
     * @return the current map in ASCII code
     */
    public String printMapToCli() {
        AsciiBoard board = new AsciiBoard(0, 23, 0, 14,
                new SmallFlatAsciiHexPrinter());
        fillBoardWithHexes(board);
        return board.prettPrint(true);
    }

    private void fillBoardWithHexes(AsciiBoard board) {
        for (Entry<Point, Sector> pointSector : map.entrySet()) {
            Cube coord = pointSector.getValue().toCube();
            Point axial = coord.toAxial();
            Point offset = pointSector.getKey();
            char marker;

            String sectorType = pointSector.getValue().getClass()
                    .getSimpleName();
            switch (sectorType) {
            case "DangerousSector":
                marker = '•';
                break;

            case "SecureSector":
                marker = ' ';
                break;

            case "StartingSector":
                marker = '#';
                break;

            case "EscapeHatchSector":
                marker = '+';
                break;

            default:
                marker = '!';
                break;

            }
            if (!"EmptySector".equals(sectorType))
                board.printHex(Integer.toString(offset.x),
                        Integer.toString(offset.y), marker, axial.x, axial.y);
        }
    }

    public Set<ExportSector> exportMap() {
        Set<ExportSector> exportedSectors = new HashSet<ExportSector>();
        for (Entry<Point, Sector> entry : map.entrySet())
            exportedSectors.add(new ExportSector(entry.getKey().x, entry
                    .getKey().y, entry.getValue().getClass().getSimpleName()));
        return exportedSectors;
    }


    /**
     * Convert the current map in an JSON array containing all sectors with
     * their coordinates and sector type
     *
     * @return the current map exported in JSON format
     */
    public String jsonizeMap() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(exportMap());
    }

    /**
     * @return the sector stored ad the key position in the map
     */
    public Sector getSector(Point key) {
        return map.get(key);
    }

    /**
     * @return the map
     */
    public Map<Point, Sector> getMap() {
        return map;
    }

    /**
     * @return the map name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the human starting sector
     */
    public Sector getHumanStartSector() {
        return humanStartSector;
    }

    /**
     * @return the alien starting sector
     */
    public Sector getAlienStartSector() {
        return alienStartSector;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GameMap other = (GameMap) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
