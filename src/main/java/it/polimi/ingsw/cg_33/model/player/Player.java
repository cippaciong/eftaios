package it.polimi.ingsw.cg_33.model.player;

import it.polimi.ingsw.cg_33.model.card.item.Item;
import it.polimi.ingsw.cg_33.model.map.GameMap;
import it.polimi.ingsw.cg_33.model.sector.Sector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 * The abstract player
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public abstract class Player {

    private String name;
    private String playerTitle;
    /**
     * A collection of items actually owned by a Player
     */
    protected List<Item> items = new ArrayList<Item>();

    /**
     * Current state of Player. Is set inactive by default at the beginning of
     * match.
     */
    private PlayerState playerState = PlayerState.ACTIVE;

    /**
     * Player's current position on the board
     */
    private Sector position;

    private int speed;

    protected Player() {
    }

    public static List<Player> createRandomPlayersList(int numPlayers,
            Sector alienStart, Sector humanStart) {
        List<Player> players = new ArrayList<Player>();
        players.addAll(Alien.createAliensList(numPlayers / 2 + numPlayers % 2, alienStart));
        players.addAll(Human.createHumansList(numPlayers / 2, humanStart));
        Collections.shuffle(players);
        return players;
    }

    public List<Item> getItems() {
        return items;
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }

    public String addItem(Item item) {
        String output = "Hai pescato l'oggetto: " + item.toString();
        this.items.add(item);
        return output;
    }

    public void removeItem(Item item) {
        this.items.remove(item);
    }

    public Sector getPosition() {
        return position;
    }

    public void setPosition(Sector position) {
        this.position = position;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public abstract boolean canMoveTo(GameMap map, Sector destination);

    public boolean hit() {
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getPlayerTitle() {
        return playerTitle;
    }

    public void setPlayerTitle(String playerTitle) {
        this.playerTitle = playerTitle;
    }
}
