package it.polimi.ingsw.cg_33.model.sector;

public class ExportSector {
    int col;
    int row;
    String sectorType;
    public ExportSector(int col, int row, String sectorType) {
        this.col = col;
        this.row = row;
        this.sectorType = sectorType;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ExportSector [col=" + col + ", row=" + row + ", sectorType="
                + sectorType + "]";
    }
    /**
     * @return the col
     */
    public int getCol() {
        return col;
    }
    /**
     * @return the row
     */
    public int getRow() {
        return row;
    }
    /**
     * @return the sectorType
     */
    public String getSectorType() {
        return sectorType;
    }

}
