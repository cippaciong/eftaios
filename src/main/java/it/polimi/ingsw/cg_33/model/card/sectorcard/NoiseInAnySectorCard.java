package it.polimi.ingsw.cg_33.model.card.sectorcard;

/**
 * <p>
 * A card that allow you to make noise where you want
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class NoiseInAnySectorCard extends DangerousSectorCard {

    public NoiseInAnySectorCard(boolean hasItem) {
        super(hasItem);
        description = "Ehi, hai trovato un bullone, puoi lanciarlo e fare rumore dove vuoi," + "mi raccomando dimmi le coordinate del settore in cui vuoi far rumore prima" + "dello scadere del tempo, altrimenti potrei dire a tutti dove ti trovi";
    }

    @Override
    public String getLinkedPossibleAction() {
        return "noiseinanysector";
    }

}
