package it.polimi.ingsw.cg_33.model.card.item;

/**
 * <p>
 * A simple Item that will be put in a deck 
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Sedatives extends Item {

    public Sedatives() {
        super("this card give you the chance to hide your position even if you make noise");
    }

    @Override
    public String toOneDescription() {
        return "sedativo usato con successo";
    }

    @Override
    public String toAllDescription() {
        return " ha usato un sedativo e dunque non rivelerà la sua posizione. ";
    }
}
