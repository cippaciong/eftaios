package it.polimi.ingsw.cg_33.model.deck;

import it.polimi.ingsw.cg_33.model.card.sectorcard.NoiseCard;
import it.polimi.ingsw.cg_33.model.card.sectorcard.NoiseInAnySectorCard;
import it.polimi.ingsw.cg_33.model.card.sectorcard.SilenceCard;

import java.util.Collections;

/**
 * <p>
 * A simple Deck of SectorCards
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class DeckOfDangerousSectorCards extends Deck {

    public DeckOfDangerousSectorCards() {
        for (int i = 0; i < 5; i++)
            getCards().add(new SilenceCard());
        for (int i = 0; i < 6; i++) {
            getCards().add(new NoiseCard(false));
            getCards().add(new NoiseInAnySectorCard(false));
        }
        for (int i = 0; i < 4; i++) {
            getCards().add(new NoiseCard(true));
            getCards().add(new NoiseInAnySectorCard(true));
        }
        Collections.shuffle(getCards());
    }
}
