package it.polimi.ingsw.cg_33.model.card.escapehatchcard;

/**
 * <p>
 * The Hatch card that allow Human to escape 
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */

public class EscapeHatchRedCard extends EscapeHatchCard {

    public EscapeHatchRedCard() {
        setWin(false);
    }

    @Override
    public String toOneDescription() {
        return "Damn! This hatch doesn't work!";
    }

    @Override
    public String toAllDescription() {
        return "have tryed to fly away with a hatch but he was unlucky because the battery was uncharge!";
    }

}
