package it.polimi.ingsw.cg_33.model.card.item;

/**
 * <p>
 * A simple Item that will be put in a deck 
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Teleport extends Item {

    public Teleport() {
        super("this teleport you to your starting sector");
    }

    @Override
    public String toOneDescription() {
        return "Ti sei teletrasportato con successo!";
    }

    @Override
    public String toAllDescription() {
        return " si è teletrasportato alla base. ";
    }
}
