package it.polimi.ingsw.cg_33.model.card.item;

/**
 * <p>
 * A simple Item that will be put in a deck 
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Defence extends Item {

    public Defence() {
        super("this card save you if someone attack you");
    }

    @Override
    public String toOneDescription() {
        return "Hai attaccato con successo!";
    }

    @Override
    public String toAllDescription() {
        return " si è salvato usando la carta difesa";
    }
}
