package it.polimi.ingsw.cg_33.model.match;

import it.polimi.ingsw.cg_33.model.deck.Deck;
import it.polimi.ingsw.cg_33.model.deck.DeckOfDangerousSectorCards;
import it.polimi.ingsw.cg_33.model.deck.DeckOfEscapeHatches;
import it.polimi.ingsw.cg_33.model.deck.DeckOfItems;
import it.polimi.ingsw.cg_33.model.map.GameMap;
import it.polimi.ingsw.cg_33.model.player.Player;
import it.polimi.ingsw.cg_33.model.player.PlayerState;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * <p>
 * The base element of model. Match contains all informations to players, items
 * and all things related to this game
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Match {

    private int currentTurn;
    private Player currentPlayer;
    private GameState gameState = GameState.RUNNING;
    private List<Object> discardedItems = new ArrayList<Object>();
    private List<Player> players;
    private int timeOut = 120000;
    private String id;
    private Set<String> possibleActionsBackUp = new HashSet<>();
    private Set<String> possibleActions = new HashSet<>();
    private List<Object> discardedCards = new ArrayList<Object>();
    private Deck itemsDeck = new DeckOfItems();
    private Deck cardsDeck = new DeckOfDangerousSectorCards();
    private Deck escapeHatchesDeck = new DeckOfEscapeHatches();
    private GameMap map;
    private final List<Player> sightedPlayers = new ArrayList<Player>();
    private final ConcurrentMap<User, Player> mapUserPlayer = new ConcurrentHashMap<User, Player>();
    private List<String> chat = new ArrayList<>();
    private final Map<String, List<String>> mapPlayerToAllDescription = new HashMap<>();

    public Match(String id, GameMap map, List<Player> players, List<User> users) {
        this.id = id;
        this.map = map;
        this.players = players;
        this.setMapUserPlayer(users, players);
        this.currentTurn = 0;
        this.currentPlayer = players.get(0);
        this.possibleActions.add("move");
        this.cardsDeck = new DeckOfDangerousSectorCards();
        this.escapeHatchesDeck = new DeckOfEscapeHatches();
        this.itemsDeck = new DeckOfItems();
        for (Player p : this.mapUserPlayer.values())
            mapPlayerToAllDescription.put(p.toString(), new ArrayList<String>());
    }

    public int getCurrentTurn() {
        return currentTurn;
    }

    public void setCurrentTurn(int currentTurn) {
        this.currentTurn = currentTurn;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getPlayerFromId(String id) {
        for (User u : mapUserPlayer.keySet())
            if (u.getMyId().equals(id))
                return mapUserPlayer.get(u);
        return null;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public List<Object> getDiscardedItems() {
        return discardedItems;
    }

    public List<Object> getDiscardedCards() {
        return discardedCards;
    }

    public GameMap getMap() {
        return map;
    }

    public List<Player> getSightedPlayers() {
        return sightedPlayers;
    }

    private synchronized void setMapUserPlayer(List<User> users,
            List<Player> players) {
        for (int i = 0; i < users.size(); i++) {
            this.mapUserPlayer.put(users.get(i), players.get(i));
            players.get(i).setName(users.get(i).getName());
        }
    }

    public synchronized ConcurrentMap<User, Player> getMapUserPlayer() {
        return mapUserPlayer;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public synchronized List<Player> getActivePlayers() {
        List<Player> activePlayers = new ArrayList<>();
        for (Player p : players)
            if (p.getPlayerState().equals(PlayerState.ACTIVE))
                activePlayers.add(p);
        return activePlayers;
    }

    /**
     * @return the timeOut
     */
    public int getTimeOut() {
        return timeOut;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    public Deck getItemsDeck() {
        return itemsDeck;
    }

    public Deck getCardsDeck() {
        return cardsDeck;
    }

    public List<String> getChat() {
        return chat;
    }

    public Deck getEscapeHatchesDeck() {
        return escapeHatchesDeck;
    }

    public Set<String> getPossibleActions() {
        return possibleActions;
    }

    public Set<String> getPossibleActionsBackUp() {
        return possibleActionsBackUp;
    }

    public Map<String, List<String>> getMapPlayerToAllDescription() {
        return mapPlayerToAllDescription;
    }

}
