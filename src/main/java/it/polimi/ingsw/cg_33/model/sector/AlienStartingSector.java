package it.polimi.ingsw.cg_33.model.sector;

public class AlienStartingSector extends Sector {

    public AlienStartingSector(int col, int row) {
        super(col, row);
    }

    @Override
    public String toOneDescription() {
        return "Sei sul settore di partenza degli alieni";
    }

    @Override
    public String getLinkedPossibleAction() {
        return "move";
    }
}
