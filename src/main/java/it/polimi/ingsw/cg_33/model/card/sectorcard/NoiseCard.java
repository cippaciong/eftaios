package it.polimi.ingsw.cg_33.model.card.sectorcard;


/**
 * <p>
 * A card that force you to make noise
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class NoiseCard extends DangerousSectorCard {

    public NoiseCard(boolean hasItem) {
        super(hasItem);
        description = "Solo io e te sappiamo che hai fatto rumore nel tuo settore, ora non ti resta che prendere tempo" + "e dirmi quando vuoi che gli altri sappiano la tua posizione, ricorda il tempismo è fondamentale";
    }

    @Override
    public String getLinkedPossibleAction() {
        return "noise";
    }
}
