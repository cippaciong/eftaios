package it.polimi.ingsw.cg_33.model.card.escapehatchcard;

/**
 * <p>
 * The Hatch card that allow Human to escape 
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class EscapeHatchGreenCard extends EscapeHatchCard {

    public EscapeHatchGreenCard() {
        setWin(true);
    }

    @Override
    public String toOneDescription() {
        return "You're lucky, this hatch is working! Go home and hug your family";
    }

    @Override
    public String toAllDescription() {
        return "is running far away from this hell!";
    }
}
