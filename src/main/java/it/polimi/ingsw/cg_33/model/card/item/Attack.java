package it.polimi.ingsw.cg_33.model.card.item;

/**
 * <p>
 * A simple Item that will be put in a deck 
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Attack extends Item {
   
    public Attack() {
        super("this card allow you to do an Attack even if you are Human");
    }

    @Override
    public String toOneDescription() {
        return "Item attack used, now let's see if someone will get hurt!";
    }

    @Override
    public String toAllDescription() {
        return " ha usato la carta attacco e può mietere vittime per questa volta";
    }
}
