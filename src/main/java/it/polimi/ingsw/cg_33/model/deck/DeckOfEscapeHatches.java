package it.polimi.ingsw.cg_33.model.deck;

import it.polimi.ingsw.cg_33.model.card.escapehatchcard.EscapeHatchGreenCard;
import it.polimi.ingsw.cg_33.model.card.escapehatchcard.EscapeHatchRedCard;

import java.util.Collections;

/**
 * <p>
 * A simple Deck of EscapeHatch
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class DeckOfEscapeHatches extends Deck {

    public DeckOfEscapeHatches() {
        getCards().add(new EscapeHatchGreenCard());
        getCards().add(new EscapeHatchGreenCard());
        getCards().add(new EscapeHatchGreenCard());
        getCards().add(new EscapeHatchRedCard());
        getCards().add(new EscapeHatchRedCard());
        getCards().add(new EscapeHatchRedCard());
        Collections.shuffle(getCards());
    }
}
