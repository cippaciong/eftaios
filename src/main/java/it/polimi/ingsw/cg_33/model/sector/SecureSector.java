package it.polimi.ingsw.cg_33.model.sector;

public class SecureSector extends Sector {

    public SecureSector(int col, int row) {
        super(col, row);
    }

    @Override
    public String toOneDescription() {
        return "Sei su un Settore sicuro!";
    }

    @Override
    public String getLinkedPossibleAction() {
        return "endturn";
    }

}
