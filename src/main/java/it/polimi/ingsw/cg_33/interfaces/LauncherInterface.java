package it.polimi.ingsw.cg_33.interfaces;

import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LauncherInterface extends Remote {
    Output launch(Message message) throws RemoteException;

}
