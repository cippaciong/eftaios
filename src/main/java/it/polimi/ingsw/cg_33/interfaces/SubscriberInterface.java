package it.polimi.ingsw.cg_33.interfaces;

import it.polimi.ingsw.cg_33.communication.ToAllOutput;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SubscriberInterface extends Remote {

    public void dispatchMessage(ToAllOutput toAllOutput) throws RemoteException;

    public String getName() throws RemoteException;

}