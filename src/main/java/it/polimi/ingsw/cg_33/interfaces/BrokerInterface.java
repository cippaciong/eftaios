package it.polimi.ingsw.cg_33.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface BrokerInterface extends Remote {

    public void subscribe(String topic, SubscriberInterface r)
            throws RemoteException;

}