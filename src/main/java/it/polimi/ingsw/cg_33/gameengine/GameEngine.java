package it.polimi.ingsw.cg_33.gameengine;

import it.polimi.ingsw.cg_33.gameengine.pub.rmi.BrokerRMI;
import it.polimi.ingsw.cg_33.gameengine.pub.socket.Broker;
import it.polimi.ingsw.cg_33.gameengine.server.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * This is the Main class for the Sever component.
 * The main method starts all the three server objects, that are:
 * <ul>
 * <li> Socket Server
 * <li> Socket Broker
 * <li> RMI Broker
 * </ul>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class GameEngine {

    private static final Logger LOGGER = Logger.getLogger(GameEngine.class.getName());

    private GameEngine() {

    }

    public static void main(String[] args) {

        try {
            FileInputStream fis = new FileInputStream("src" + File.separator + "main" + File.separator + "resources" + File.separator + "logging.prop");
            LogManager.getLogManager().readConfiguration(fis);
            fis.close();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Unable to load file logging.properties", e);
        }
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("src" + File.separator + "main" + File.separator + "resources" + File.separator + "userpassdb.prop"));
            ConcurrentMap<String, String> userpassMap = it.polimi.ingsw.cg_33.model.server.Server.getInstance().getUsernamePasswordMap();
            for (String key : properties.stringPropertyNames()) {
                userpassMap.put(key, properties.get(key).toString());
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Can't load db from disk!", e);
        }

        Server multiThreadedServer = new Server();
        Broker broker = Broker.getInstance();
        BrokerRMI brokerRMI = BrokerRMI.getInstance();
        broker.start();
        brokerRMI.start();
        LOGGER.info("Broker service started");

        try {
            multiThreadedServer.startServer();
            LOGGER.info("Server started");

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "A problem occurred trying to start the server", e);
        }

    }
}
