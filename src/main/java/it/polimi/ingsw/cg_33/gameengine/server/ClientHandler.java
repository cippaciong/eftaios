package it.polimi.ingsw.cg_33.gameengine.server;

import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

/**
 * This class is basically a thread which handles a single client connection
 * The communication between parts is done using JSON (Gson) objects, the ClienHandler
 * reads the incoming Message and sends a reply in the form of Output.
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class ClientHandler implements Runnable {

    private Socket socket;
    private Launcher launcher = new Launcher();
    private static final Logger LOGGER = Logger.getLogger(ClientHandler.class
            .getName());

    public ClientHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            Scanner socketIn = new Scanner(socket.getInputStream());
            PrintWriter socketOut = new PrintWriter(socket.getOutputStream());
            while (true) {
                String line = socketIn.nextLine();
                if ("quit".equals(line))
                    break;
                else
                    decodeMessageAndReply(line, socketOut);
            }
            socketIn.close();
            socketOut.close();
            socket.close();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Could not close the socket", e);
        }
    }

    private void decodeMessageAndReply(String line, PrintWriter socketOut) {

        socketOut.flush();
        Gson gson = new Gson();
        Message message = gson.fromJson(line, Message.class);
        Output output = launcher.launch(message);
        try {
            socketOut.println(new Gson().toJson(output, Output.class));
        } catch (SecurityException | IllegalArgumentException e) {
            LOGGER.log(
                    Level.WARNING,
                    "Could not decode message, execute the action or send the reply",
                    e);
        }
        socketOut.flush();

    }
}
