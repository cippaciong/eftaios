package it.polimi.ingsw.cg_33.gameengine.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is the Socket Server class, it opens a socket listening on port 7777
 * and start a ClientHandler thread for each incoming connection using a thread pool
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class Server {

    private static final int PORT = 7777;
    private static final Logger LOGGER = Logger.getLogger(Server.class
            .getName());

/**
 * Opens a socket listening on port 7777 and start a ClientHandler thread
 * for each incoming connection using a thread pool
 * 
 */
    public void startServer() throws IOException {

        ExecutorService threadPool = Executors.newCachedThreadPool();

        ServerSocket serverSocket = new ServerSocket(PORT);
        LOGGER.info("New ServerSocket created, listening on port: " + PORT);

        while (true) {
            try {

                Socket socket = serverSocket.accept();
                threadPool.submit(new ClientHandler(socket));

            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "Could not create a new socket", e);
                break;
            }
        }

        threadPool.shutdown();
        serverSocket.close();

    }

}
