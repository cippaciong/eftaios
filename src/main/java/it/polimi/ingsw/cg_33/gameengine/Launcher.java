package it.polimi.ingsw.cg_33.gameengine;

import it.polimi.ingsw.cg_33.communication.Message;
import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.ActionDescriptionMap;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.EnterRoom;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.GetInitialInformations;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Login;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.RoomList;
import it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction.Signup;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.Attack;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.DrawDangerousSectorCard;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.EndTurn;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.GetPlayersHistory;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.Move;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.Noise;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.NoiseInAnySector;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.SendChatMessage;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction.DiscardItem;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction.UseAdrenaline;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction.UseAttack;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction.UseSedatives;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction.UseSpotlight;
import it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction.UseTeleport;
import it.polimi.ingsw.cg_33.interfaces.LauncherInterface;

import java.util.List;

/**
 * This class is the connection between the Message received from the client
 * and the Output reply.
 * It parses the Message and invokes the correct action.
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class Launcher implements LauncherInterface {

    @Override
    public Output launch(Message message) {
        String clientId = message.getClientId();
        String action = message.getAction();
        List<String> args = message.getArgs();
        Action a;
        Output output = new Output();
        switch (action.toLowerCase().replaceAll("[^a-zA-Z]", "")) {
        case "move":
            a = new Move(clientId, args.get(0));
            break;
        case "draw":
            a = new DrawDangerousSectorCard(clientId);
            break;
        case "noise":
            a = new Noise(clientId);
            break;
        case "noiseinanysector":
            a = new NoiseInAnySector(clientId, args.get(0));
            break;
        case "useattack":
            a = new UseAttack(clientId);
            break;
        case "useteleport":
            a = new UseTeleport(clientId);
            break;
        case "usesedatives":
            a = new UseSedatives(clientId);
            break;
        case "useadrenaline":
            a = new UseAdrenaline(clientId);
            break;
        case "usespotlight":
            a = new UseSpotlight(clientId, args);
            break;
        case "attack":
            a = new Attack(clientId);
            break;
        case "discarditem":
            a = new DiscardItem(clientId, args.get(0));
            break;
        case "endturn":
            a = new EndTurn(clientId);
            break;
        case "enterroom":
            a = new EnterRoom(clientId, args.get(0));
            break;
        case "roomlist":
            a = new RoomList(clientId);
            break;
        case "actiondescription":
            a = new ActionDescriptionMap(clientId);
            break;
        case "sendchatmessage":
            a = new SendChatMessage(clientId, args.get(0));
            break;
        case "getlog":
            a = new GetPlayersHistory(clientId);
            break;
        case "login":
            a = new Login(clientId, args);
            break;
        case "signup":
            a = new Signup(clientId, args);
            break;
        case "getinitialinformations":
            a = new GetInitialInformations(clientId);
            break;
        default:
            output.getDescriptions().add("Undefined Action");
            return output;
        }
        output = a.execute();
        return output;
    }

}
