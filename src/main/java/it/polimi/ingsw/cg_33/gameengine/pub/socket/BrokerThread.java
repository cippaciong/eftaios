package it.polimi.ingsw.cg_33.gameengine.pub.socket;

import it.polimi.ingsw.cg_33.communication.ToAllOutput;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

public class BrokerThread extends Thread {

    private static final Logger LOGGER = Logger.getLogger(BrokerThread.class
            .getName());

    /*
     * La nuova socket, verso uno specifico subscriber, creata dalla
     * ServerSocket
     */
    private Socket socket;
    /*
     * Abbiamo soltanto bisogno di recapitare il messaggio. Il pattern non
     * prevede la ricezione, da parte del publisher, di alcun messaggio ( se non
     * la richiesta di sottoscrizione che tuttavia viene catturata dalla accept
     * nella ServerSocket)
     */
    private PrintWriter out;

    // Una coda che contiene i messaggi, specifici per ciascun subscriber
    private Queue<ToAllOutput> buffer;

    private String subscriberTopic;

    /**
     * Quando un client esterno si sottoscrive viene creato un nuovo thread che
     * rappresenterá la specifica connessione allo specifico client/subscriber.
     * 
     * @param socket
     *            La nuova socket, verso uno specifico subscriber, creata dalla
     *            ServerSocket
     */
    public BrokerThread(Socket socket) {
        this.socket = socket;
        buffer = new ConcurrentLinkedQueue<ToAllOutput>();

        try {
            this.out = new PrintWriter(socket.getOutputStream());
        } catch (IOException e) {
            LOGGER.log(Level.INFO, "Could not write on socket", e);
        }
    }

    /**
     * Questo metodo contiene, di fatto, la logica della comunicazione dal
     * publisher allo specifico subscriber.
     */
    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            subscriberTopic = in.readLine();
            LOGGER.info("Adding a new subscriber" + subscriberTopic);
            Broker.getInstance().addSubscriber(subscriberTopic, this);
            socket.shutdownInput();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Could not add new subscriber", e);
        }
        while (true) {
            // Si prova ad estrarre un messaggio dalla coda...
            ToAllOutput msg = buffer.poll();
            // ... se c'é lo si invia
            if (msg != null)
                send(msg);
            else {
                // ... altrimenti, per evitare cicli inutili di CPU
                // che possono portare ad utilizzarla inutilmente...
                try {
                    // ... si aspetta fin quando la coda non conterrá qualcosa
                    // é necessario sincronizzarsi sull'oggetto monitorato, in
                    // modo tale
                    // che il thread corrente possieda il monitor sull'oggetto.
                    synchronized (buffer) {
                        buffer.wait();
                    }
                } catch (InterruptedException e) {
                    LOGGER.log(
                            Level.WARNING,
                            "thread has been interrupted either before or during the activity",
                            e);
                }
            }
        }
    }

    /**
     * Questo metodo inserisce un messaggio nella coda e notifica ai thread in
     * attesa (in questo caso a se stesso) la presenza di un messaggio.
     * 
     * @param toAllOutput
     *            Il messaggio da inserire.
     */
    public void dispatchMessage(ToAllOutput toAllOutput) {
        buffer.add(toAllOutput);
        // é necessario sincronizzarsi sull'oggetto monitorato
        synchronized (buffer) {
            buffer.notify();
        }
    }

    /**
     * Questo metodo invia il messaggio al subscriber tramite la rete
     * 
     * @param toAllOutput
     */
    private void send(ToAllOutput toAllOutput) {
        Gson gson = new Gson();
        out.println(gson.toJson(toAllOutput));
        out.flush();
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Could not close the socket", e);
        } finally {
            out = null;
            socket = null;
        }
    }

    /**
     * @return the subscriberId
     */
    public String getSubscriberId() {
        return subscriberTopic;
    }
}
