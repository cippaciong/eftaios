package it.polimi.ingsw.cg_33.gameengine.pub.rmi;

import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.gameengine.Launcher;
import it.polimi.ingsw.cg_33.interfaces.BrokerInterface;
import it.polimi.ingsw.cg_33.interfaces.LauncherInterface;
import it.polimi.ingsw.cg_33.interfaces.SubscriberInterface;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BrokerRMI extends Thread implements BrokerInterface {

    private Map<String, List<SubscriberInterface>> subscribers = new HashMap<String, List<SubscriberInterface>>();
    private static final BrokerRMI INSTANCE = new BrokerRMI();

    private static final Logger LOGGER = Logger.getLogger(BrokerRMI.class
            .getName());

    private BrokerRMI() {
    }

    public static BrokerRMI getInstance() {
        return INSTANCE;
    }

    @Override
    public void run() {
        LOGGER.info("Starting the Broker Service");
        BrokerRMI broker = BrokerRMI.getInstance();
        Launcher launcher = new Launcher();

        try {

            // RMI provides a particular type of remote object, the RMI
            // registry,
            // for finding references to other remote objects. The RMI registry
            // is a simple naming service that enables clients
            // to obtain a reference to a remote object by name.
            // The registry is typically only used to locate the first remote
            // object that an RMI client needs to use. That first remote object
            // might then provide support for finding other objects.
            // First, we create an RMI registry on port 7777
            Registry registry = LocateRegistry.createRegistry(7779);

            // Export the supplied remote object so that it can receive
            // invocations of its remote methods from remote clients.
            // The second argument specifies which TCP port to use
            // to listen for incoming remote invocation requests for the object.
            // It is common to use the value zero, which specifies the use of
            // an anonymous port. The actual port will then be chosen at runtime
            // by RMI
            BrokerInterface brstub = (BrokerInterface) UnicastRemoteObject
                    .exportObject(broker, 0);
            LauncherInterface swstub = (LauncherInterface) UnicastRemoteObject
                    .exportObject(launcher, 0);

            // This rebind(name,interface) invocation makes a remote call to the
            // RMI registry
            // on the local host and binds the name "broker" to the remote
            // interface "BrokerInterface".
            registry.rebind("Broker", brstub);
            registry.rebind("Launcher", swstub);

            Scanner stdin = new Scanner(System.in);
            String inputLine = stdin.nextLine();

            // Broker publishes the messages until we write "quit"
            while (!isStopped(inputLine)) {
                inputLine = stdin.nextLine();
            }

            // when you are exposing objects through RMI it needs a thread to
            // accept
            // incoming requests for these objects.
            // This thread could be a daemon thread which wouldn't stop the JVM
            // from exiting.
            // To stop the threads use unexportObject for all remote objects and
            // unbind
            // them in the registry
            stdin.close();
            registry.unbind("Broker");
            registry.unbind("Launcher");
            UnicastRemoteObject.unexportObject(broker, true);
            UnicastRemoteObject.unexportObject(launcher, true);

        } catch (NotBoundException | RemoteException | NoSuchElementException e) {
            LOGGER.log(Level.WARNING, "Error encountered starting BrokerRMI", e);
        }
    }

    private static boolean isStopped(String inputLine) {
        return "quit".equals(inputLine);
    }

    /**
     * 
     * @param toAllOutput
     *            - message to be published to all the subscribers This is not a
     *            remote method, however it calls the remote method
     *            dispatchMessage for each Subscriber.
     */
    public void publish(String topic, ToAllOutput toAllOutput) {
        if (!subscribers.isEmpty()) {
            LOGGER.info("Publishing message on topic " + topic);
            for (SubscriberInterface sub : subscribers.get(topic)) {
                try {
                    sub.dispatchMessage(toAllOutput);
                } catch (RemoteException e) {
                    LOGGER.log(Level.WARNING, "Error dispatching message", e);
                }
            }
        } else {
            LOGGER.info("No subscribers!");
        }
    }

    /**
     * @param r
     *            is the Subcriber's remote interface that the broker can use to
     *            publish messages The method updates the list of subscriber
     *            interfaces that are subscribed to the broker
     */
    @Override
    public void subscribe(String topic, SubscriberInterface r) {
        List<SubscriberInterface> sublist;
        try {
            sublist = subscribers.get(topic);
            sublist.add(r);
        } catch (NullPointerException np) {
            LOGGER.log(Level.INFO,
                    "No existing sublist found, creating a new one", np);
            sublist = new ArrayList<SubscriberInterface>();
            sublist.add(r);
        }
        subscribers.put(topic, sublist);
    }
}
