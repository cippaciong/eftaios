package it.polimi.ingsw.cg_33.gameengine.pub.socket;

import it.polimi.ingsw.cg_33.communication.ToAllOutput;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Broker extends Thread {
    private static final int PORT = 7778;
    private boolean listening = true;
    private Map<String, BrokerThread> tmpSubscribers = new HashMap<String, BrokerThread>();
    private Map<String, List<BrokerThread>> perTopicSubscribers = new HashMap<String, List<BrokerThread>>();

    private static final Logger LOGGER = Logger.getLogger(Broker.class
            .getName());

    private static final Broker INSTANCE = new Broker();

    protected Broker() {
    }

    public static Broker getInstance() {
        return INSTANCE;
    }

    /**
     * We open a ServerSocket in a loop to accept multiple subscriptions.
     * When a new subscriber try to subscribe a new Socket is created in the
     * corresponding BrokerThread thread.
     */
    @Override
    public void run() {
        // runs the client handler in charge of reading the mosse from the
        // command line and sending it to the server

        try (ServerSocket brokerSocket = new ServerSocket(PORT)) {

            while (listening) {
                BrokerThread brokerThread = new BrokerThread(
                        brokerSocket.accept());
                brokerThread.start();
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Cannot listen on port: " + PORT, e);
        }
    }

    /**
     * This method sends a message to all the subscribers in the topicSubscribers list
     * It iterates over all the BrokerThread and invokes the dispatchMessage method
     * @param toAllOutput
     *            Il messaggio da mandare.
     */
    public void publish(String topic, ToAllOutput toAllOutput) {
        List<BrokerThread> topicSubscribers = getTopicSubscribers(topic);
        if (!topicSubscribers.isEmpty()) {
            LOGGER.info("Publishing message on socket");
            for (BrokerThread pub : topicSubscribers) {
                pub.dispatchMessage(toAllOutput);
            }
        } else {
            LOGGER.warning("No subscribers for this topic using the socket protocol");
        }
    }

    public void putTmpSubscribers(String subscriberId, BrokerThread brokerThread) {
        tmpSubscribers.put(subscriberId, brokerThread);
    }

    /**
     * @return the tmpSubscribers
     */
    public Map<String, BrokerThread> getTmpSubscribers() {
        return tmpSubscribers;
    }

    public void addSubscriber(String topic, BrokerThread brokerThread) {
        List<BrokerThread> brokerList = perTopicSubscribers.get(topic);
        try {
            brokerList.add(brokerThread);

        } catch (NullPointerException np) {
            LOGGER.log(Level.INFO,
                    "No existing brokerList found, creating a new one", np);
            brokerList = new ArrayList<BrokerThread>();
            brokerList.add(brokerThread);
        }
        perTopicSubscribers.put(topic, brokerList);
    }

    private List<BrokerThread> getTopicSubscribers(String topic) {
        return perTopicSubscribers.get(topic);
    }
}
