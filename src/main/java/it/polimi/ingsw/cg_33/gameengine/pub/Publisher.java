package it.polimi.ingsw.cg_33.gameengine.pub;

import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.gameengine.pub.rmi.BrokerRMI;
import it.polimi.ingsw.cg_33.gameengine.pub.socket.Broker;
import it.polimi.ingsw.cg_33.model.server.Server;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Publisher {

    // use the classname for the logger, this way you can refactor
    private static final Logger LOGGER = Logger.getLogger(Publisher.class.getName());

    private Publisher() {
    }

    public static void publish(String topic, ToAllOutput msg) {
        for (MatchController m : Server.getInstance().getUserIdMatchControllerMap().values())
            if (m.getMatch().getId().equals(topic) && msg.getCurrentPlayer() != null && msg.getDescription() != null) {
                m.getMatch().getMapPlayerToAllDescription().get(msg.getCurrentPlayer()).add(msg.getDescription());
            }
        try {
            Broker.getInstance().publish(topic, msg);
        } catch (NullPointerException e) {
            LOGGER.log(Level.INFO, topic + "There are no players connected using the socket protocol in this match", e);
        }
        try {
            BrokerRMI.getInstance().publish(topic, msg);
        } catch (NullPointerException e) {
            LOGGER.log(Level.INFO, topic + "There are no players connected using the rmi protocol in this match", e);
        }
    }

}
