package it.polimi.ingsw.cg_33.communication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * This Class is an element of the communication layer, in details it is
 * necessary to <b>Client/Server</b> functionality.
 * <p>
 * 
 * <p>
 * Message is a sort of <b>"box"</b> in which you put the information that
 * <b>server</b> had to known to execute an <code>action</code>. This class must
 * be <b>shared</b> between server and client.
 * </p>
 * 
 * <p>
 * A message is composed by three parameter:
 * <ul>
 * <li><code>clientId</code>: A string that is necessary to Server to identify
 * client;
 * <li><code>action</code>: A string that represent the action you want to do;
 * <li><code>args</code>: a List of strings that can be passed to server if the
 * choose action need parameters (<b>optional</b>);
 * <li>
 * 
 * <p>
 * Server send an Output when receive a Message
 * </p>
 * 
 * @see Output
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class Message implements Serializable {

    private static final long serialVersionUID = 1503863892646240557L;
    private String clientId;
    private String action;
    private List<String> args = new ArrayList<>();

    /**
     * The public constructor to create message for actions that need
     * <code>args</code>
     * 
     * @param clientId
     *            the UUID that identify the client
     * @param action
     *            the action you want to do
     * @param args
     *            list of parameters required by this action
     * 
     */
    public Message(String clientId, String action, List<String> args) {
        this.clientId = clientId;
        this.action = action;
        this.args = args;
    }

    /**
     * The public constructor to create message for actions without needed
     * parameters
     * 
     * @param clientId
     *            the UUID that identify the client
     * @param action
     *            the action you want to do
     * 
     */
    public Message(String clientId, String action) {
        this.clientId = clientId;
        this.action = action;
    }

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @return the action's parameters
     */
    public List<String> getArgs() {
        return args;
    }

}
