package it.polimi.ingsw.cg_33.communication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * This Class is an element of the communication layer, in details it is
 * necessary to <b>Client/Server</b> functionality.
 * <p>
 * 
 * <p>
 * Output is a sort of <b>"box"</b> in which <b>server</b> put the information
 * that <b>client</b> asked with a Message. This class must be <b>shared</b>
 * between server and client.
 * </p>
 * 
 * <p>
 * An Output is composed by several parameters, and maybe can increase/decrease
 * in an update, cause this parameters represents different informations you
 * want to know.
 * </p>
 * 
 * <p>
 * Server send an Output when it receive a Message from client. An Output object
 * contains private information and for no reason it should be send to other
 * client but the one asked for it.
 * </p>
 * 
 * @see Message
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class Output implements Serializable {

    private static final long serialVersionUID = 3215617171155795512L;

    /**
     * A list of description that represents what you have done by execute your
     * action. For example, when you do a Move, descriptions can be like:
     * ["you are now in a dangerous sector", "you can draw a card"].
     */
    private final List<String> descriptions = new ArrayList<>();
    private String name;
    private String title;
    private String currentPosition;
    private boolean loginSuccess;
    private boolean reconnectSuccess;
    private String gameMap;
    private boolean signupSuccess;
    private String loginDescription;
    private final List<String> playersName = new ArrayList<>();

    /**
     * The Publisher's topic related to this room/match. It is useful when you
     * want do a subscribe.
     */
    private String topic;

    /**
     * Your items.
     */
    private List<String> haveitems = new ArrayList<>();

    /**
     * Is true if you have 4 items, false otherwise.
     */
    private boolean shouldDiscard;

    /**
     * A set of string representing a set of action you can do when receive this
     * output.
     */
    private Set<String> possibleActions = new HashSet<>();

    /**
     * A key-value map where:
     * <ul>
     * <li>key: is an action, for example it can be "move"
     * <li>value: is a list of description to ask for input, for example
     * ["tell me where you want to move", "now tell me why"]
     * </ul>
     */
    private Map<String, List<String>> actionDescription = new HashMap<>();

    /**
     * A key-value map where:
     * <ul>
     * <li>key: is a player, for example it can be "Jack"
     * <li>value: is a list of action that this player did ["noise in 3-2",
     * "use attack"]
     * </ul>
     */
    private Map<String, List<String>> playersLog = new HashMap<>();

    /**
     * @return the room topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * @param topic
     *            the topic used by the publisher to send output to all
     *            subscriber
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }

    /**
     * @param possibleAction
     *            the action you want allow to be done by user
     */
    public void addPossibleActions(String possibleAction) {
        this.possibleActions.add(possibleAction);
    }

    /**
     * @return reference to this actionDescription
     */
    public Map<String, List<String>> getActionDescription() {
        return this.actionDescription;
    }

    /**
     * @param actionDescription
     *            an actionDescription like map you want to set.
     */
    public void setActionDescription(Map<String, List<String>> actionDescription) {
        this.actionDescription = actionDescription;
    }

    /**
     * 
     * @return the reference to yours item list.
     */
    public List<String> getHaveItems() {
        return haveitems;
    }

    /**
     * 
     * @return true if you have to discardItem
     */
    public boolean isShouldDiscard() {
        return shouldDiscard;
    }

    /**
     * 
     * @param shouldDiscard
     *            set true if user have too much items
     */
    public void setShouldDiscard(boolean shouldDiscard) {
        this.shouldDiscard = shouldDiscard;
    }

    /**
     * 
     * @return the list of descriptions
     */
    public List<String> getDescriptions() {
        return descriptions;
    }

    /**
     * 
     * @return the set of action you can do
     */
    public Set<String> getPossibleActions() {
        return possibleActions;
    }

    /**
     * 
     * @param possibleActions
     *            the set of action you want allow to be done by user
     */
    public void setPossibleActions(Set<String> possibleActions) {
        this.possibleActions = possibleActions;
    }

    public Map<String, List<String>> getPlayersLog() {
        return playersLog;
    }

    public void setPlayersLog(
            Map<String, List<String>> playerToAllDescription) {
        this.playersLog = playerToAllDescription;
    }

    public String getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(String currentPosition) {
        this.currentPosition = currentPosition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the loginSuccess
     */
    public boolean isLoginSuccess() {
        return loginSuccess;
    }

    /**
     * @param loginSuccess the loginSuccess to set
     */
    public void setLoginSuccess(boolean loginSuccess) {
        this.loginSuccess = loginSuccess;
    }

    public boolean isReconnectSuccess() {
        return reconnectSuccess;
    }

    public void setReconnectSuccess(boolean reconnectSuccess) {
        this.reconnectSuccess = reconnectSuccess;
    }

    public String getGameMap() {
        return gameMap;
    }

    public void setGameMap(String gameMap) {
        this.gameMap = gameMap;
    }

    public void setSignupSuccess(boolean b) {
        this.signupSuccess = b;
    }

    public boolean isSignupSuccess() {
        return signupSuccess;
    }

    public void setLoginDescription(String string) {
        this.loginDescription = string;
        
    }

    public String getLoginDescription() {
        return loginDescription;
    }

    public List<String> getPlayersName() {
        return playersName;
    }

}
