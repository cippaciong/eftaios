package it.polimi.ingsw.cg_33.communication;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This Class is an element of the communication layer, in details it is
 * necessary to <b>Publisher/Subscriber</b> functionality.
 * <p>
 * 
 * <p>
 * ToAllOutput is a sort of <b>"box"</b> in which <b>server</b> put the
 * information that <b>all client in the same match</b> need to know. This class
 * must be <b>shared</b> between server and client.
 * </p>
 * 
 * <p>
 * A ToAllOutput is composed by several parameters, and maybe can
 * increase/decrease in an update, cause this parameters represents different
 * informations you want to know.
 * </p>
 * 
 * <p>
 * Server send a ToAllOutput every time the match state is changed, so all
 * players are notified about this changing.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class ToAllOutput implements Serializable {

    private static final long serialVersionUID = 1L;
    private boolean matchBegin;
    private boolean matchFinished;
    private int numTurn;
    private String currentPlayer;
    private final Map<String, String> mapPlayerStatus = new HashMap<>();
    private String chatPlayer;
    private String chatMsg;
    private String description;
    private String noiseInSector;
    private String cliMap;
    private final Map<String, String> spottedPlayers = new HashMap<>();

    public int getNumTurn() {
        return numTurn;
    }

    public void setNumTurn(int numTurn) {
        this.numTurn = numTurn;
    }

    public String getChatMsg() {
        return chatMsg;
    }

    public void setChatMsg(String chatMsg) {
        this.chatMsg = chatMsg;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(String currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public boolean isMatchBegin() {
        return matchBegin;
    }

    public void setMatchBegin(boolean matchBegin) {
        this.matchBegin = matchBegin;
    }

    /**
     * @return the cliMap
     */
    public String getCliMap() {
        return cliMap;
    }

    /**
     * @param cliMap the cliMap to set
     */
    public void setCliMap(String cliMap) {
        this.cliMap = cliMap;
    }

    public String getChatPlayer() {
        return chatPlayer;
    }

    public void setChatPlayer(String chatPlayer) {
        this.chatPlayer = chatPlayer;
    }

    public boolean isMatchFinished() {
        return matchFinished;
    }

    public void setMatchFinished(boolean matchFinished) {
        this.matchFinished = matchFinished;
    }

    public Map<String, String> getMapPlayerStatus() {
        return mapPlayerStatus;
    }

    public String getNoiseSector() {
        return noiseInSector;
    }

    public void setNoiseSector(String noiseSector) {
        this.noiseInSector = noiseSector;
    }

    public Map<String, String> getSpottedPlayers() {
        return spottedPlayers;
    }

}
