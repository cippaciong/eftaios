package it.polimi.ingsw.cg_33.controller.match;

import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.card.item.Defence;
import it.polimi.ingsw.cg_33.model.card.item.Item;
import it.polimi.ingsw.cg_33.model.map.GameMap;
import it.polimi.ingsw.cg_33.model.match.GameState;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.player.Alien;
import it.polimi.ingsw.cg_33.model.player.Human;
import it.polimi.ingsw.cg_33.model.player.Player;
import it.polimi.ingsw.cg_33.model.player.PlayerState;
import it.polimi.ingsw.cg_33.model.sector.Sector;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * MatchController is an element of the <b>controller</b> layer in terms of
 * <b>MVC</b> pattern. This class have the duty of control the match and its
 * behavior.
 * <p>
 * 
 * <p>
 * A MatchController set <b>Match Model</b> fields and also manages the
 * initialization of a new match. It also takes care of a few Actions that are
 * related to the turn management like the the CountDown or the notification of
 * players changing state.
 * <p>
 * 
 * <p>
 * The main Class in which you can find a reference to a MatchController is
 * <b>Server</b>, this is made with the focus of storing a link between players
 * and match, so there is a Map<k,v> that linked all players with its
 * MatchController. This allow some useful features like reconnection and
 * management of requests.
 * </p>
 * 
 * @see it.polimi.ingsw.cg_33.model.match
 * @see it.polimi.ingsw.cg_33.model.server.Server
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class MatchController {

    private Match match;
    private GameMap map;
    private Timer timer = new Timer();
    private final List<User> users;
    private static final Logger LOGGER = Logger.getLogger(MatchController.class.getName());

    /**
     * Constructor that received a list of users and a map. It also shuffle the
     * list of users to randomize it.
     * 
     * @param users
     * @param new GameMap(map)
     */
    public MatchController(List<User> users, GameMap map) {
        this.users = users;
        this.map = map;
        Collections.shuffle(this.users);
    }

    /**
     * method that start a TimerTurnTask and reset timer
     */
    public void manualEndTurn() {
        TimerTask t = new TimerTurnTask(match);
        // new Thread(t).start();
        timer.schedule(t, 0);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, "Error during sleep", e);
        }
        timer.cancel();
        timer = new Timer();
        timer.schedule(new TimerTurnTask(match), match.getTimeOut(), match.getTimeOut());
    }

    /**
     * Create a new Match and notify all players
     * 
     * @return match - the match created.
     */
    public void initializeMatch(String matchId) {
        final List<Player> players = Player.createRandomPlayersList(users.size(), map.getAlienStartSector(), map.getHumanStartSector());
        match = new Match(matchId, map, players, users);
        LOGGER.info("A new match has begun, the current player is: " + match.getCurrentPlayer());
        timer.schedule(new TimerTurnTask(match), match.getTimeOut(), match.getTimeOut());
        dispatchToAllOutputMatchBegin();
        notifyAllNewStatusPlayers();
    }

    /**
     * notify all players that match is begun
     */
    private void dispatchToAllOutputMatchBegin() {
        ToAllOutput o = new ToAllOutput();
        o.setDescription("A new match has begun. The first player to move will be: " + match.getCurrentPlayer());
        o.setMatchBegin(true);
        o.setCurrentPlayer(match.getCurrentPlayer().toString());
        o.setCliMap(map.printMapToCli());
        Publisher.publish(match.getId(), o);
    }

    /**
     * set game state to end
     */
    public void endMatch() {
        match.setGameState(GameState.END);

    }

    /**
     * @return the match linked with this controller
     */
    public Match getMatch() {
        return match;
    }

    /**
     * 
     * remove all items from this player and put in discarded items and set his
     * state to DEAD
     * 
     * @param p
     *            the player that is going to be killed
     */
    public void playerKilledRoutine(Player p) {
        p.setPlayerState(PlayerState.DEAD);
        match.getDiscardedItems().addAll(p.getItems());
        p.getItems().clear();
        notifyAllPlayerKilled(p);
        notifyAllNewStatusPlayers();
        checkAlienWin();
    }

    /**
     * send to all players a notification that a player was killed
     * 
     * @param p
     */
    private void notifyAllPlayerKilled(Player p) {
        String description = p.toString() + " has been murdered. ";
        ToAllOutput o = new ToAllOutput();
        o.setCurrentPlayer(match.getCurrentPlayer().toString());
        o.setDescription(description);
        if (match.getCurrentPlayer() instanceof Alien && p instanceof Human && match.getCurrentPlayer().getSpeed() != 3) {
            match.getCurrentPlayer().setSpeed(3);
            o.setDescription(description + match.getCurrentPlayer().toString() + " can now do 3 steps at time!");
        }
        Publisher.publish(match.getId(), o);
    }

    /**
     * send a notification to all players with the status of all players
     */
    public void notifyAllNewStatusPlayers() {
        ToAllOutput o = new ToAllOutput();
        for (Player player : match.getPlayers())
            o.getMapPlayerStatus().put(player.getPlayerTitle(), player.getPlayerState().toString());
        Publisher.publish(match.getId(), o);
    }

    /**
     * this method check if aliens win the game by counting living humans
     */
    private void checkAlienWin() {
        int i = 0;
        for (Player p : match.getActivePlayers())
            if (p instanceof Human)
                i++;
        if (i == 0) {
            ToAllOutput o = new ToAllOutput();
            o.setDescription("Aliens win the game!");
            o.setMatchFinished(true);
            Publisher.publish(match.getId(), o);
        }
    }

    /**
     * this method search if there are players in Sector target
     * 
     * @param target
     *            the sector in which you want find player
     * 
     * @return a list of players that was in Sector target
     */
    public List<Player> getPlayersOnSector(Sector target) {
        match.getSightedPlayers().clear();
        for (Player p : match.getActivePlayers())
            if (p.getPosition().equals(target))
                match.getSightedPlayers().add(p);
        return match.getSightedPlayers();
    }

    /**
     * do an attack
     */
    public void attackRoutine() {
        notifyAllAttack();
        Collection<Player> targetPlayers = getPlayersOnSector(match.getCurrentPlayer().getPosition());
        targetPlayers.remove(match.getCurrentPlayer());
        for (Player p : targetPlayers)
            if (p.hit())
                playerKilledRoutine(p);
            else
                humanDefenceRoutine(p);
    }

    /**
     * use defence routine
     * 
     * @param p
     *            player that is going to use defence
     */
    private void humanDefenceRoutine(Player p) {
        ToAllOutput o = new ToAllOutput();
        Item i = new Defence();
        match.getDiscardedItems().add(i);
        o.setCurrentPlayer(match.getCurrentPlayer().toString());
        o.setDescription(p + i.toAllDescription());
        Publisher.publish(match.getId(), o);
    }

    /**
     * notify all players about an attack
     */
    private void notifyAllAttack() {
        ToAllOutput o = new ToAllOutput();
        o.setCurrentPlayer(match.getCurrentPlayer().toString());
        o.setDescription(match.getCurrentPlayer() + " is going to attack in " + match.getCurrentPlayer().getPosition().toString());
        o.setNoiseSector(match.getCurrentPlayer().getPosition().toString());
        Publisher.publish(match.getId(), o);
    }
}
