package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.card.escapehatchcard.EscapeHatchCard;
import it.polimi.ingsw.cg_33.model.player.Alien;
import it.polimi.ingsw.cg_33.model.player.Human;
import it.polimi.ingsw.cg_33.model.player.Player;
import it.polimi.ingsw.cg_33.model.player.PlayerState;
import it.polimi.ingsw.cg_33.model.sector.AlienStartingSector;
import it.polimi.ingsw.cg_33.model.sector.EscapeHatchSector;
import it.polimi.ingsw.cg_33.model.sector.HumanStartingSector;
import it.polimi.ingsw.cg_33.model.sector.Sector;

import java.awt.Point;

/**
 * <p>
 * This Action does some check and than call a move routine. It also check if
 * you activate some particular routine like if your destination is an escape
 * hatch or a starting sector..
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Move extends Action {

    private String target;

    public Move(String clientId, String target) {
        super(clientId);
        this.target = target;
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        // PREPROCESSING
        if (!canDoCurrentPlayer())
            return getOutput();
        else if (!getMatch().getPossibleActions().contains("move"))
            getOutput().getDescriptions().add("You can't move now! Remember, only one move for each turn");
        // ACTION
        else
            tryToMove();
        return getOutput();
    }

    private void tryToMove() {
        try {
            int x = Integer.parseInt(target.split("-")[0]);
            int y = Integer.parseInt(target.split("-")[1]);
            Sector destination = getMatch().getMap().getSector(new Point(x, y));
            moveRoutine(destination);
        } catch (NumberFormatException e) {
            getOutput().getDescriptions().add("Devi inserire il targeti nel formato x-y dove x e y sono due interi");
        }
    }

    private void moveRoutine(Sector destination) {
        if (destination == null)
            getOutput().getDescriptions().add("This map haven't this sector");
        else if (getPlayer().getPosition().equals(destination))
            getOutput().getDescriptions().add("You're already there, choose another destination");
        else if (!getPlayer().canMoveTo(getMatch().getMap(), destination))
            getOutput().getDescriptions().add("Can't reach destination!");
        else if (destination instanceof AlienStartingSector || destination instanceof HumanStartingSector)
            getOutput().getDescriptions().add("You can't move on a starting sector");
        else if (destination instanceof EscapeHatchSector) {
            EscapeHatchSector e = (EscapeHatchSector) destination;
            escapeHatchRoutine(e);
        } else
            move(destination);
    }

    private void escapeHatchRoutine(EscapeHatchSector e) {
        if (getPlayer() instanceof Alien)
            getOutput().getDescriptions().add("Aliens can't use escape hatches!");
        else {
            getOutput().getDescriptions().add(e.toOneDescription());
            if (e.isActive()) {
                getMatch().getPossibleActions().clear();
                getMatch().getPossibleActions().add(e.getLinkedPossibleAction());
                humanOnHatchRoutine(e);
            }
        }
    }

    private void humanOnHatchRoutine(EscapeHatchSector h) {
        EscapeHatchCard c = (EscapeHatchCard) getMatch().getEscapeHatchesDeck().draw();
        getOutput().getDescriptions().add(c.toOneDescription());
        ToAllOutput o = new ToAllOutput();
        o.setCurrentPlayer(getPlayer().toString());
        o.setDescription(getPlayer() + c.toAllDescription());
        Publisher.publish(getMatch().getId(), o);
        checkHumanEscape(c);
        h.setActive(false);
    }

    private void checkHumanEscape(EscapeHatchCard c) {
        if (c.isWin()) {
            ToAllOutput o = new ToAllOutput();
            int i = 0;
            getPlayer().setPlayerState(PlayerState.ESCAPED);
            for (Player player : getMatch().getPlayers())
                o.getMapPlayerStatus().put(player.getPlayerTitle(), player.getPlayerState().toString());
            for (Player p : getMatch().getActivePlayers())
                if (p instanceof Human)
                    i++;
            if (i == 0) {
                o.setCurrentPlayer(getPlayer().toString());
                o.setMatchFinished(true);
                o.setDescription("All Humans are escaped!!!");
                Publisher.publish(getMatch().getId(), o);
            }
        } else {
            getMatch().getPossibleActions().clear();
            getMatch().getPossibleActions().add("endturn");
        }
    }

    private void move(Sector destination) {
        getMatch().getPossibleActions().clear();
        if (getPlayer() instanceof Alien)
            getMatch().getPossibleActions().add("attack");
        getMatch().getPossibleActions().add(destination.getLinkedPossibleAction());
        getPlayer().setPosition(destination);
        getOutput().getDescriptions().add(destination.toOneDescription());
        getOutput().setPossibleActions(getMatch().getPossibleActions());
        getOutput().setCurrentPosition(destination.toString());
        getToAllOutput().setDescription(getPlayer() + destination.toAllDescription());
        Publisher.publish(getMatch().getId(), getToAllOutput());
    }

}
