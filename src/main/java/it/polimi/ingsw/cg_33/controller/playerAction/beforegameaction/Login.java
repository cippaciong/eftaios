package it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.controller.match.RoomController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.model.match.Room;
import it.polimi.ingsw.cg_33.model.player.Player;
import it.polimi.ingsw.cg_33.model.player.PlayerState;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.List;

/**
 * <p>
 * This Class have the duty of managing player's login. It parse a request from
 * a user and check that is a registered user. Than check check password and
 * have different behavior for this kind of situation:
 * <ul>
 * <li>if user was already logged in check if it was in a match, in a room or if
 * it does simply a double login and reput it in the right place
 * <li>if is a new login, register user in server.
 * </ul>
 * </p>
 * 
 * @see it.polimi.ingsw.cg_33.controller.playerAction
 * @see it.polimi.ingsw.cg_33.model.server.Server
 * @see it.polimi.ingsw.cg_33.gameengine
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Login extends Action {

    private String username = "";
    private String pass = "";
    private Server server = Server.getInstance();

    public Login(String clientId, List<String> args) {
        super(clientId);
        if (args != null && args.size() == 2) {
            this.username = args.get(0);
            this.pass = args.get(1);
        }
    }

    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        if (!server.getUsernamePasswordMap().containsKey(username)) {
            getOutput().setLoginDescription("Invalid username! Sign up and then try again");
            return getOutput();
        } else if (!server.getUsernamePasswordMap().get(username).equals(pass)) {
            getOutput().setLoginDescription("Wrong Password");
            return getOutput();
        } else
            loginRoutine();
        return getOutput();
    }

    /**
     * do a login only if you are not alrady logged in, else try to reconnect
     */
    private void loginRoutine() {
        // se non eri già loggato
        if (!server.getClientidUsernameMap().containsValue(username)) {
            getOutput().setLoginDescription("Login Complete!");
            server.getClientidUsernameMap().put(getClientId(), username);
            getOutput().setLoginSuccess(true);
        }
        // se eri già loggato
        else
            reconnectRoutine();
    }

    /**
     * re put user in match if he was in one, same with room
     */
    private void reconnectRoutine() {
        // azioni comuni a tutti i tipi di login che avevi fatto
        String oldId = retrieveOldId();
        MatchController matchController = server.getUserIdMatchControllerMap().get(oldId);
        // se eri in una partita:
        if (matchController != null)
            resumeMatchRoutine(matchController, oldId);
        else {
            // setto come se avessi già fatto il login ma senza enterRoom
            getOutput().setLoginDescription("You're logged in again now, nothing has changed!");
            server.getClientidUsernameMap().remove(oldId);// aggiunto dopo
            server.getClientidUsernameMap().put(getClientId(), username);
            // controllo se eri in una stanza e nel caso cambio la description
            reEnterRoom(oldId);
        }
    }

    /**
     * re put user in room
     * 
     * @param oldId
     *            the old userId before disconnection
     */
    private void reEnterRoom(String oldId) {
        for (Room r : RoomController.getInstance().getActiveroommap().values())
            for (User u : r.getUsers())
                if (u.getMyId().equals(oldId)) {
                    getOutput().setTopic(r.getTopic());
                    u.setMyId(getClientId());
                    getOutput().setGameMap(r.getMap().jsonizeMap());
                    getOutput().setLoginDescription("You're now in your old room");
                    getOutput().setReconnectSuccess(true);
                }
    }

    /**
     * retrieve all match field and set new id
     * 
     * @param matchController
     *            the matchControlled linked to player's match
     * @param oldId
     *            the old userId before disconnection
     */
    private void resumeMatchRoutine(MatchController matchController,
            String oldId) {
        if (!matchController.getMatch().getPlayerFromId(oldId).getPlayerState().equals(PlayerState.INACTIVE))
            getOutput().setLoginDescription("You can't login twince, keep playing instead!");
        else {
            server.getUserIdMatchControllerMap().remove(oldId);
            server.getUserIdMatchControllerMap().put(getClientId(), matchController);
            Player player = matchController.getMatch().getPlayerFromId(oldId);
            for (User u : matchController.getMatch().getMapUserPlayer().keySet())
                if (u.getMyId().equals(oldId))
                    u.setMyId(getClientId());
            player.setPlayerState(PlayerState.ACTIVE);
            buildOutput(matchController);
            server.getClientidUsernameMap().put(getClientId(), username);
            server.getClientidUsernameMap().remove(oldId);// aggiunto dopo
        }
    }

    /**
     * create output
     * 
     * @param matchController
     *            the matchControlled linked to player's match
     */
    private void buildOutput(MatchController matchController) {
        getOutput().setTopic(matchController.getMatch().getId());
        getOutput().setGameMap(matchController.getMatch().getMap().jsonizeMap());
        getOutput().setLoginDescription("You're going to resume your match!");
        getOutput().setReconnectSuccess(true);
    }

    /**
     * search and retrieve the id before disconnection
     * 
     * @return the oldId or null if no id was found
     */
    private String retrieveOldId() {
        for (String oldId : server.getClientidUsernameMap().keySet())
            if (server.getClientidUsernameMap().get(oldId).equals(username))
                return oldId;
        return null;
    }
}
