package it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.RoomController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.model.match.Room;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * <p>
 * This Class simply create a list of available room, put in a output and send it to a user
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class RoomList extends Action {

    private ConcurrentMap<String, Room> activeRooms = RoomController.getInstance().getActiveroommap();
    private ConcurrentMap<String, Integer> mapListClient = new ConcurrentHashMap<String, Integer>();

    public RoomList(String clientId) {
        super(clientId);
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        for (String map : activeRooms.keySet()) {
            mapListClient.put(map, activeRooms.get(map).getNumberOfUsers());
        }
        getOutput().getDescriptions().add(mapListClient.toString());
        return getOutput();
    }

}
