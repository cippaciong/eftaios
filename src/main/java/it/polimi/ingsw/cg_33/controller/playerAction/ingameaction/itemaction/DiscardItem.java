package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.card.item.Item;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * A DiscardItem Action allow users to discard their item if all condition are
 * respected.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class DiscardItem extends InvolveItemAction {

    private Item item;
    private Map<String, Item> mapStringItem = new HashMap<>();

    public DiscardItem(String clientId, String item) {
        super(clientId);
        mapStringItem.put("attack", new it.polimi.ingsw.cg_33.model.card.item.Attack());
        mapStringItem.put("defence", new it.polimi.ingsw.cg_33.model.card.item.Defence());
        mapStringItem.put("adrenaline", new it.polimi.ingsw.cg_33.model.card.item.Adrenaline());
        mapStringItem.put("sedatives", new it.polimi.ingsw.cg_33.model.card.item.Sedatives());
        mapStringItem.put("teleport", new it.polimi.ingsw.cg_33.model.card.item.Teleport());
        mapStringItem.put("spotlight", new it.polimi.ingsw.cg_33.model.card.item.Spotlight());
        this.item = mapStringItem.get(item.toLowerCase());
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        // PREPROCESSING
        if (!super.canDoItem(item))
            return getOutput();
        else if (!getMatch().getPossibleActions().contains("discarditem"))
            getOutput().getDescriptions().add("Non è questo il momento di scartare un oggetto!");
        // ACTION
        else
            discardItemRoutine();
        return getOutput();
    }

    private void discardItemRoutine() {
        removeItem(item);
        getMatch().getPossibleActions().clear();
        getMatch().getPossibleActions().addAll(getMatch().getPossibleActionsBackUp());
        getOutput().getDescriptions().add("Oggetto scartato con successo!");
        getOutput().setPossibleActions(getMatch().getPossibleActions());
        getToAllOutput().setDescription(getPlayer() + " ha scartatato l'oggetto " + item);
        Publisher.publish(getMatch().getId(), getToAllOutput());
    }

}
