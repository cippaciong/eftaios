package it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.model.card.item.Item;
import it.polimi.ingsw.cg_33.model.player.Player;

/**
 * <p>
 * This Action fill the output field with some importants informations
 * that client have to know to start a new match such his player race, his
 * position on map ecc..
 * </p>
 * @see it.polimi.ingsw.cg_33.controller.playerAction
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class GetInitialInformations extends Action {

    public GetInitialInformations(String clientId) {
        super(clientId);
    }

    @Override
    public Output execute() {
        if (getMatch() == null)
            getOutput().getDescriptions().add("You can't ask me this before starting match!");
        else {
            getOutput().setCurrentPosition(getPlayer().getPosition().toString());
            getOutput().setName(getPlayer().getName());
            getOutput().setTitle(getPlayer().getPlayerTitle());
            getOutput().addPossibleActions("move");
            getOutput().getDescriptions().add("Sei un: " + getPlayer().getPlayerTitle());
            for (Item item : getPlayer().getItems())
                getOutput().getHaveItems().add(item.toString());
            for (Player p : getMatch().getActivePlayers())
                getOutput().getPlayersName().add(p.getName());
        }
        return getOutput();
    }
}
