package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;

/**
 * <p>
 * Handle all actions done by users and put in a map to show players some useful
 * informations
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class GetPlayersHistory extends Action {

    public GetPlayersHistory(String clientId) {
        super(clientId);
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        if (getMatch() != null) {
            getOutput().setPlayersLog(getMatch().getMapPlayerToAllDescription());
        } else
            getOutput().getDescriptions().add("non puoi avere un log se non sei in partita");
        return getOutput();
    }

}
