package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction;

import it.polimi.ingsw.cg_33.model.card.item.Item;
import it.polimi.ingsw.cg_33.model.player.Alien;

/**
 * <p>
 * This class is a super class of every actions that involve item use. Do some
 * simply some common checks.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public abstract class UseItem extends InvolveItemAction {

    private final Item item;

    public UseItem(String clientId, Item item) {
        super(clientId);
        this.item = item;
    }

    public boolean canUseItem(Item item) {
        if (!super.canDoItem(item))
            return false;
        else if (getPlayer() instanceof Alien) {
            getOutput().getDescriptions().add("Gli alieni non possono usare oggetti!");
            return false;
        }
        return true;
    }

    protected Item getItem() {
        return item;
    }
}
