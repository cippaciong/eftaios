package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.card.item.Teleport;

/**
 * <p>
 * This class simply acticate teleport if you have one and if you are human set
 * your position to the starting sector.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class UseTeleport extends UseItem {

    public UseTeleport(String clientId) {
        super(clientId, new Teleport());
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        if (!canUseItem(getItem()))
            return getOutput();
        getPlayer().setPosition(getMatch().getMap().getHumanStartSector());
        getOutput().getDescriptions().add(getItem().toOneDescription());
        getOutput().setPossibleActions(getMatch().getPossibleActions());
        getOutput().setCurrentPosition(getPlayer().getPosition().toString());
        removeItem(getItem());
        getToAllOutput().setDescription(getPlayer() + getItem().toAllDescription());
        Publisher.publish(getMatch().getId(), getToAllOutput());
        return getOutput();
    }

}
