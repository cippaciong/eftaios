package it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.model.server.Server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * This Class is made to handle new user, store in a database and sending
 * confirmation to user.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Signup extends Action {

    private String username;
    private String password;
    private Server server = Server.getInstance();
    private static final Logger LOGGER = Logger.getLogger(Signup.class.getName());

    public Signup(String clientId, List<String> args) {
        super(clientId);
        if (args != null && args.size() == 2) {
            this.username = args.get(0);
            this.password = args.get(1);
        }
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        if (server.getUsernamePasswordMap().containsKey(username))
            getOutput().getDescriptions().add("Username already exist");
        else if (password.length() < 8)
            getOutput().getDescriptions().add("Password should have at least 8 chars");
        else
            signupRoutine();
        return getOutput();
    }

    private void signupRoutine() {
        server.getUsernamePasswordMap().put(username, password);
        server.getClientidUsernameMap().put(getClientId(), username);
        getOutput().setLoginDescription("Your account is now registered!");
        getOutput().setSignupSuccess(true);
        storedb();
    }

    private void storedb() {
        Properties properties = new Properties();
        properties.putAll(server.getUsernamePasswordMap());
        try {
            properties.store(new FileOutputStream("src" + File.separator + "main" + File.separator + "resources" + File.separator + "userpassdb.prop"), null);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Can't save db in disk!", e);
        }
    }
}