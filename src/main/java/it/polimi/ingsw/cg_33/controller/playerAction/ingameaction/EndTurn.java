package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;

/**
 * <p>
 * This class is a manual endTurn and call the TimerTask without waiting the
 * timeout. Then create a new timer and start it.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class EndTurn extends Action {

    public EndTurn(String clientId) {
        super(clientId);
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        // PREPROCESSING
        if (!super.canDoCurrentPlayer())
            return getOutput();
        else if (!getMatch().getPossibleActions().contains("endturn"))
            getOutput().getDescriptions().add("Non puoi passare il turno ora");
        // ACTION
        else
            endTurnRoutine();
        return getOutput();
    }

    private void endTurnRoutine() {
        getMatchController().manualEndTurn();
        getOutput().getDescriptions().add("Turno Passato, aspetta fin quando ritocca a te");
        getMatch().getPossibleActions().clear();
        getMatch().getPossibleActions().add("move");
        getOutput().setPossibleActions(getMatch().getPossibleActions());
    }
}
