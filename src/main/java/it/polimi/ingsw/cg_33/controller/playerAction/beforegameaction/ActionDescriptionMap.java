package it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * This Class is a sort of db that contains all of the behavior that calling an
 * action involves. The only field of this class is actionDescription, a
 * key-value map that have like key the action name and value a list of
 * arguments. There are few actions that doesn't need arguments, in that case a
 * new empty list is put.
 * </p>
 * 
 * <p>
 * The main focus is to allow client to play game without needing an update of
 * client. In details if in a future release a new Action is made, you can put
 * easily here without touching client code (almost for CLI clients).
 * </p>
 * 
 * @see it.polimi.ingsw.cg_33.controller.playerAction
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class ActionDescriptionMap extends Action {

    private final Map<String, List<String>> actionDescription = new HashMap<>();

    public ActionDescriptionMap(String clientId) {
        super(clientId);

        actionDescription.put("getLog", new ArrayList<String>());
        actionDescription.put("endTurn", new ArrayList<String>());
        actionDescription.put("attack", new ArrayList<String>());
        actionDescription.put("noise", new ArrayList<String>());
        actionDescription.put("roomList", new ArrayList<String>());
        actionDescription.put("draw", new ArrayList<String>());
        actionDescription.put("useAttack", new ArrayList<String>());
        actionDescription.put("useTeleport", new ArrayList<String>());
        actionDescription.put("useSedative", new ArrayList<String>());
        actionDescription.put("useAdrenaline", new ArrayList<String>());
        actionDescription.put("move", Collections.singletonList("Scegli il settore in cui vuoi fare lo spostamento"));
        actionDescription.put("noiseInAnySector", Collections.singletonList("Scegli il settore in cui vuoi fare rumore"));
        actionDescription.put("enterRoom", Collections.singletonList("Scegli la stanza in cui vuoi giocare"));
        actionDescription.put("sendChatMessage", Collections.singletonList("Scrivi il messaggio: "));
        actionDescription.put("discardItem", Collections.singletonList("Scegli l'oggetto da scartare"));
        actionDescription.put("useSpotlight", Collections.singletonList("Scegli dove puntare la torcia"));

        List<String> loginArgs = new ArrayList<>();
        loginArgs.add("Username: ");
        loginArgs.add("Password: ");
        actionDescription.put("login", loginArgs);
        actionDescription.put("signup", loginArgs);
    }

    @Override
    public Output execute() {
        getOutput().setActionDescription(actionDescription);
        return getOutput();
    }

}
