package it.polimi.ingsw.cg_33.controller.match;

import it.polimi.ingsw.cg_33.model.match.Room;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * RoomController is the class make to manage the Room. The main focuses are:
 * <ul>
 * <li>Tracking the various waiting Rooms (one for each kind of Map)
 * <li>Call a method of MatchController to start a new Match when a room is
 * full.
 * <li>Call a method of MatchController to start a new Match when a specific
 * lapse of time is passed (with the help of CountDownThread)
 * </ul>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 */
public class RoomController {

    private final ConcurrentMap<String, Timer> countDownThreadMap = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, Room> activeRoomMap = new ConcurrentHashMap<>();

    private static final RoomController INSTANCE = new RoomController();

    private RoomController() {
        activeRoomMap.put("Galvani", new Room(UUID.randomUUID().toString()));
        activeRoomMap.put("Fermi", new Room(UUID.randomUUID().toString()));
        activeRoomMap.put("Galilei", new Room(UUID.randomUUID().toString()));
    }

    /**
     * @return the instance
     */
    public static RoomController getInstance() {
        return INSTANCE;
    }

    /**
     * start the timer
     * 
     * @param map
     *            a string that represent the chosen map
     */
    public void startCountDown(final String map) {
        countDownThreadMap.put(map, new Timer());
        countDownThreadMap.get(map).schedule(new TimerTask() {
            @Override
            public void run() {
                if (activeRoomMap.get(map).getNumberOfUsers() > 1) {
                    startNewMatch(map);
                    cleanRoom(map);
                }
            }
        }, 60000, 60000);
    }

    /**
     * set all players into a Map<k,v> in Server and call initializeMatch in
     * MatchController
     * 
     * @param map
     *            a string that represent the chosen map
     */
    public void startNewMatch(String map) {
        Room room = activeRoomMap.get(map);
        List<User> users = room.getUsers();
        String matchId = room.getTopic();
        MatchController matchController = new MatchController(users, room.getMap());
        ConcurrentMap<String, MatchController> matchControllerUsersMap = Server.getInstance().getUserIdMatchControllerMap();
        for (User user : users)
            matchControllerUsersMap.put(user.getMyId(), matchController);
        matchController.initializeMatch(matchId);
    }

    /**
     * create a new room and put into activeRoomMap than cancel the timer
     * 
     * @param map
     */
    public void cleanRoom(String map) {
        activeRoomMap.replace(map, new Room(UUID.randomUUID().toString()));
        countDownThreadMap.get(map).cancel();
    }

    /**
     * @return the countdownthreadmap
     */
    public ConcurrentMap<String, Timer> getCountdownthreadmap() {
        return countDownThreadMap;
    }

    /**
     * @return the activeroommap
     */
    public ConcurrentMap<String, Room> getActiveroommap() {
        return activeRoomMap;
    }

}