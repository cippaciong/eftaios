package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.card.item.Sedatives;

/**
 * <p>
 * This class allow currentPlayer to use Sedatives. It does some checks and then
 * allow you to endTurn.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class UseSedatives extends UseItem {

    public UseSedatives(String clientId) {
        super(clientId, new Sedatives());
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        if (!canUseItem(getItem()))
            return getOutput();
        getOutput().getDescriptions().add(getItem().toOneDescription());
        getOutput().setPossibleActions(getMatch().getPossibleActions());
        removeItem(getItem());
        if (getMatch().getPossibleActions().contains("draw")) {
            getMatch().getPossibleActions().remove("draw");
            getMatch().getPossibleActions().add("endturn");
        }
        getToAllOutput().setDescription(getPlayer() + getItem().toAllDescription());
        Publisher.publish(getMatch().getId(), getToAllOutput());
        return getOutput();
    }

}
