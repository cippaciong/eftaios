package it.polimi.ingsw.cg_33.controller.match;

import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.card.item.Item;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.player.Player;
import it.polimi.ingsw.cg_33.model.player.PlayerState;

import java.util.List;
import java.util.TimerTask;

/**
 * <p>
 * TimerTurnTask is the Class that does all the management of a <b>changing
 * turn</b> request. A turn can finished if a lapse of time is finished or if a
 * player call directly an endTurn action. In both case this code is executed
 * and without difference.
 * <p>
 * 
 * <p>
 * If a turn change is requests while the current player is in a turn state that
 * don't allow the turn changes, the player is <b>disconnect</b> and can resume
 * match by logging in again
 * <p>
 * 
 * @see it.polimi.ingsw.cg_33.controller.match.MatchController
 * @see it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.EndTurn
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class TimerTurnTask extends TimerTask {

    private Match match;

    /**
     * public constructor to receive match field
     * 
     * @param match
     *            the match to be started
     */
    public TimerTurnTask(Match match) {
        this.match = match;
    }

    /**
     * if possibleAction contains endturn start changeCurrentPlayer routine else
     * disconnect player
     */
    @Override
    public void run() {
        if (!match.getPossibleActions().contains("endturn"))
            disconnectedCurrentPlayer();
        changeCurrentPlayerRoutine();
        toAllOutputTurnFinished();
        notify();
    }

    /**
     * discard all items of players and notify all players that currentPlayer
     * has been disconnected
     */
    private void disconnectedCurrentPlayer() {
        ToAllOutput o = new ToAllOutput();
        o.setDescription(match.getCurrentPlayer() + " haven't finished his turn properly. Close view and Re log in to resume match!");
        for (Item item : match.getCurrentPlayer().getItems()) {
            match.getDiscardedItems().add(item);
            match.getCurrentPlayer().removeItem(item);
        }
        match.getCurrentPlayer().setPlayerState(PlayerState.INACTIVE);
        Publisher.publish(match.getId(), o);
    }

    /**
     * set new currentPlayer
     */
    private void changeCurrentPlayerRoutine() {
        List<Player> activePlayers = match.getActivePlayers();
        if (!activePlayers.isEmpty()) {
            int i = activePlayers.indexOf(match.getCurrentPlayer());
            if (i == activePlayers.size() - 1) {
                changeCurrentTurnRoutine();
                i = 0;
            } else
                i++;
            match.setCurrentPlayer(activePlayers.get(i));
            // match.getPossibleActions().clear();
            // match.getPossibleActions().add("move");
        }
    }

    /**
     * change turn
     */
    private void changeCurrentTurnRoutine() {
        match.setCurrentTurn(match.getCurrentTurn() + 1);
        toAllOutputNumTurnChanged();
        if (match.getCurrentTurn() == 40)
            toAllOutputMatchFinisched();
    }

    /**
     * notify all players that turn is changed
     */
    private void toAllOutputNumTurnChanged() {
        ToAllOutput o = new ToAllOutput();
        o.setNumTurn(match.getCurrentTurn());
        o.setDescription("Now is turn: " + match.getCurrentTurn());
        Publisher.publish(match.getId(), o);
    }

    /**
     * notify to all players that match is over
     */
    private void toAllOutputMatchFinisched() {
        ToAllOutput o = new ToAllOutput();
        o.setMatchFinished(true);
        o.setDescription("Game Over");
        Publisher.publish(match.getId(), o);
    }

    /**
     * notify to all players that turn is changed
     */
    private void toAllOutputTurnFinished() {
        ToAllOutput o = new ToAllOutput();
        o.setDescription("Cambio turno. Tocca ora a: " + match.getCurrentPlayer());
        for (Player player : match.getPlayers())
            o.getMapPlayerStatus().put(player.getPlayerTitle(), player.getPlayerState().toString());
        Publisher.publish(match.getId(), o);
    }
}
