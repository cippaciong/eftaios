package it.polimi.ingsw.cg_33.controller.playerAction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.controller.match.MatchController;
import it.polimi.ingsw.cg_33.model.match.Match;
import it.polimi.ingsw.cg_33.model.player.Player;
import it.polimi.ingsw.cg_33.model.server.Server;

/**
 * <p>
 * This is Class is an essential Class. This is the superClass of all actions.
 * The canDoCurrentPlayer method parse clientid from requests and check if you
 * are the current Player. If it so, you can do action, else a warning is
 * sending to user.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public abstract class Action implements Command {

    private MatchController matchController = null;
    private String clientId;
    private Match match = null;
    private Player player = null;
    private Output output = new Output();
    private ToAllOutput toAllOutput = new ToAllOutput();

    public Action(String clientId) {
        this.clientId = clientId;
        matchController = Server.getInstance().getUserIdMatchControllerMap().get(clientId);
        if (matchController != null) {
            match = matchController.getMatch();
            player = match.getPlayerFromId(clientId);
        }
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public abstract Output execute();

    protected boolean canDoCurrentPlayer() {
        if (match == null) {
            output.getDescriptions().add("Azione disponibile solo a partita iniziata!");
            return false;
        } else if (!match.getCurrentPlayer().equals(player)) {
            output.getDescriptions().add("Questa azione la puoi fare solo quando è il tuo turno");
            return false;
        } else {
            toAllOutput.setCurrentPlayer(match.getCurrentPlayer().toString());
            return true;
        }
    }

    protected MatchController getMatchController() {
        return matchController;
    }

    protected Match getMatch() {
        return match;
    }

    protected Output getOutput() {
        return output;
    }

    protected Player getPlayer() {
        return player;
    }

    public String getClientId() {
        return clientId;
    }

    protected ToAllOutput getToAllOutput() {
        return toAllOutput;
    }

}
