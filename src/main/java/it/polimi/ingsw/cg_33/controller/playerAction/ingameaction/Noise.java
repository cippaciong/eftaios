package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;

/**
 * <p>
 * A noise action is a notification to all players that you are in your sector.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Noise extends Action {
    public Noise(String clientId) {
        super(clientId);
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        // PREPROCESSING
        if (!super.canDoCurrentPlayer())
            return getOutput();
        else if (!getMatch().getPossibleActions().contains("noise"))
            getOutput().getDescriptions().add("Non puoi fare rumore ora!");
        // ACTION
        else
            noiseRoutine();
        return getOutput();
    }

    private void noiseRoutine() {
        getMatch().getPossibleActions().clear();
        getMatch().getPossibleActions().add("endturn");
        getOutput().getDescriptions().add("Speriamo che nessuno ci creda!");
        getToAllOutput().setDescription(getPlayer() + " ha fatto rumore in " + getPlayer().getPosition());
        getToAllOutput().setNoiseSector(getPlayer().getPosition().toString());
        Publisher.publish(getMatch().getId(), getToAllOutput());
        getOutput().setPossibleActions(getMatch().getPossibleActions());
    }

}
