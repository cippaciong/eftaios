package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.model.player.Human;

/**
 * <p>
 * This is the Attack action for Aliens. It does some checks and then call
 * attackRoutine in MatchController
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class Attack extends Action {

    public Attack(String clientId) {
        super(clientId);
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        // PREPROCESSING
        if (!canDoCurrentPlayer())
            return getOutput();
        else if (!getMatch().getPossibleActions().contains("attack"))
            getOutput().getDescriptions().add("Non è il momento di attaccare!");
        else if (getPlayer() instanceof Human)
            getOutput().getDescriptions().add("Solo gli alieni possono attaccare senza usare oggetti!");
        // ACTION
        else
            attackRoutine();
        return getOutput();
    }

    private void attackRoutine() {
        getMatchController().attackRoutine();
        getMatch().getPossibleActions().clear();
        getMatch().getPossibleActions().add("endturn");
        getOutput().getDescriptions().add("Hai attaccato con successo!");
        getOutput().setPossibleActions(getMatch().getPossibleActions());
    }

}
