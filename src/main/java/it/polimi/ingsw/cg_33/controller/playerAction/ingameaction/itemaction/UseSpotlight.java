package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.card.item.Spotlight;
import it.polimi.ingsw.cg_33.model.player.Player;
import it.polimi.ingsw.cg_33.model.sector.Sector;

import java.awt.Point;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * This is one of the items with most particular behavior: a spotlight takes a
 * target in x-y format and if these are int do a routine to spot players in
 * that target.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class UseSpotlight extends UseItem {

    private String target = "";

    public UseSpotlight(String clientId, List<String> args) {
        super(clientId, new Spotlight());
        if (args != null)
            this.target = args.get(0);
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        // PREPROCESSING
        if (!canUseItem(getItem()))
            return getOutput();
        // ACTION
        try {
            spotlightRoutine();
        } catch (NumberFormatException e) {
            getOutput().getDescriptions().add("Devi inserire il targeti nel formato x-y dove x e y sono due interi");
            getOutput().setPossibleActions(getMatch().getPossibleActions());
        }
        return getOutput();
    }

    private void spotlightRoutine() {
        Sector sector = getSector(target);
        if (sector == null)
            getOutput().getDescriptions().add("This map haven't this sector");
        else {
            notifyAllSpotlightOn();
            aimSpotlight(sector);
            getOutput().getDescriptions().add(getItem().toOneDescription());
            getOutput().setPossibleActions(getMatch().getPossibleActions());
            removeItem(getItem());
        }
    }

    private void aimSpotlight(Sector sector) {
        ToAllOutput o = new ToAllOutput();
        Map<String, String> spotted = spottedPlayer(sector);
        if (spotted.isEmpty()) {
            getOutput().getDescriptions().add("Nessun giocatore avvistato");
            o.setDescription("Tutti tranquilli, nessun giocatore è stato illuminato dalla torcia");
        } else {
            getOutput().getDescriptions().add("Giocatori avvistati: " + spotted);
            o.setDescription("ATTENZIONE: sono stati avvistati i seguenti giocatori:" + spotted);
            o.getSpottedPlayers().putAll(spotted);
        }
        Publisher.publish(getMatch().getId(), o);
    }

    private void notifyAllSpotlightOn() {
        ToAllOutput o = new ToAllOutput();
        o.setCurrentPlayer(getPlayer().toString());
        o.setDescription(getPlayer() + getItem().toAllDescription());
        Publisher.publish(getMatch().getId(), o);
    }

    private Sector getSector(String target) {
        System.err.println("target");
        System.err.println(target);
        int x = Integer.parseInt(target.split("-")[0]);
        int y = Integer.parseInt(target.split("-")[1]);
        return getMatch().getMap().getSector(new Point(x, y));
    }

    private Map<String, String> spottedPlayer(Sector sector) {
        System.err.println("sector");
        System.err.println(sector.toString());
        Map<String, String> spotted = new HashMap<>();
        for (Player p : getMatchController().getPlayersOnSector(sector))
            spotted.put(p.toString(), sector.toString());
        for (Sector s : getMatch().getMap().sectorNeighbors(sector))
            for (Player p : getMatchController().getPlayersOnSector(s))
                spotted.put(p.toString(), s.toString());
        System.err.println(spotted);
        return spotted;
    }
}
