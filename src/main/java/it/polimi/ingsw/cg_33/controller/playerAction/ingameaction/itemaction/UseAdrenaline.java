package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.card.item.Adrenaline;

/**
 * <p>
 * This class allow currentPlayer to use Adrenaline. It does some checks and
 * then set speed to 2.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class UseAdrenaline extends UseItem {

    public UseAdrenaline(String clientId) {
        super(clientId, new Adrenaline());
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        if (!canUseItem(getItem()))
            return getOutput();
        else if (!getMatch().getPossibleActions().contains("move"))
            getOutput().getDescriptions().add("Mi spiace dovevi usarla prima di muoverti, conservala per il prossimo turno!");
        else
            useAdrenalineRoutine();
        return getOutput();
    }

    private void useAdrenalineRoutine() {
        getPlayer().setSpeed(2);
        getOutput().getDescriptions().add(getItem().toOneDescription());
        getOutput().setPossibleActions(getMatch().getPossibleActions());
        removeItem(getItem());
        getToAllOutput().setDescription(getPlayer() + getItem().toAllDescription());
        Publisher.publish(getMatch().getId(), getToAllOutput());
    }

}
