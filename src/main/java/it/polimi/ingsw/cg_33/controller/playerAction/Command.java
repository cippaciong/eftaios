package it.polimi.ingsw.cg_33.controller.playerAction;

import it.polimi.ingsw.cg_33.communication.Output;

/**
 * <p>
 * Simply an interface that all action implements to fit the Command Pattern.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public interface Command {
    Output execute();
}
