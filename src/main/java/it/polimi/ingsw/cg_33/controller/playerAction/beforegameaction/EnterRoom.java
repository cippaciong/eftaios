package it.polimi.ingsw.cg_33.controller.playerAction.beforegameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.match.RoomController;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.map.GameMap;
import it.polimi.ingsw.cg_33.model.match.Room;
import it.polimi.ingsw.cg_33.model.server.Server;
import it.polimi.ingsw.cg_33.model.user.User;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * enterRoom handle new users that want to play in a specific map. When a user
 * is the first for its specific kind of map, a new room is created and is
 * stored in a ConcurrentMap (activeRoomMap) where map is the key and the new
 * room is the value. After that, the number of available room for this specific
 * map is set to 0 in numRoomsLeft that is another ConcurrentMap. So a
 * countDownThreadMap is started and when timeout is reach, the thread call a
 * method of MatchController to start a new Match. Then clean the room with a
 * call to cleanRoom to make it available for other users. If eight players join
 * the room before the timeout, a new Match is created the same way described
 * before.
 * 
 * @param user
 *            the user that join the room
 * @param map
 *            the specific map that will be played when match start
 */
public class EnterRoom extends Action {

    private RoomController roomController;
    private String map;
    private Room room;
    private String name;
    private static final Logger LOGGER = Logger.getLogger(EnterRoom.class.getName());

    public EnterRoom(String clientId, String map) {
        super(clientId);
        this.map = map;
        this.roomController = RoomController.getInstance();
        this.room = roomController.getActiveroommap().get(map);
    }

    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        if (isAlreadyInRoom() || isAlreadyInGame() || !isLoggedIn())
            return getOutput();
        else {
            try {
                enterRoomRoutine();
            } catch (NullPointerException e) {
                LOGGER.log(Level.SEVERE, "An invalid room has been required", e);
                getOutput().getDescriptions().add("An invalid room has been required, write roomList and choose on of available room!");
            }
            return getOutput();
        }
    }

    /**
     * check if user is logged in
     * 
     * @return a false is is not log, true instead
     */
    private boolean isLoggedIn() {
        if (!Server.getInstance().getClientidUsernameMap().containsKey(getClientId())) {
            getOutput().getDescriptions().add("You had to logIn to play a match!");
            return false;
        }
        return true;
    }

    /**
     * check if user is already in room
     * 
     * @return true if already in room, false instead
     */
    private boolean isAlreadyInRoom() {
        for (Room r : roomController.getActiveroommap().values())
            for (User u : r.getUsers())
                if (getClientId().equals(u.getMyId())) {
                    getOutput().getDescriptions().add("Sei già in una stanza!");
                    return true;
                }
        return false;
    }

    private boolean isAlreadyInGame() {
        if (getMatch() != null) {
            getOutput().getDescriptions().add("Stai già giocando!");
            return true;
        }
        return false;
    }

    private void enterRoomRoutine() throws NullPointerException {
        name = Server.getInstance().getClientidUsernameMap().get(getClientId());
        room.addUser(new User(getClientId(), name)); // nullPointerException
        checkFirstLastUser();
        getOutput().setTopic(room.getTopic());
        getOutput().getDescriptions().add("You are now in Room. Players in room:" + room.getUsers().toString());
        getOutput().setGameMap(room.getMap().jsonizeMap());
        notifyOthersInRoom();
    }

    private void checkFirstLastUser() {
        // if first player start room countDown
        if (room.getNumberOfUsers() == 1) {
            room.setMap(new GameMap(map));
            roomController.startCountDown(map);
        }
        // if eighth player start new match
        else if (room.getNumberOfUsers() == 8) {
            roomController.startNewMatch(map);
            roomController.cleanRoom(map);
        }
    }

    private void notifyOthersInRoom() {
        getToAllOutput().setDescription(name + "è entrato nella stanza");
        Publisher.publish(room.getTopic(), getToAllOutput());
    }
}
