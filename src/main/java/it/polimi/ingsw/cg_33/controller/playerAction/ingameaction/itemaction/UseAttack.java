package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;

/**
 * <p>
 * This class simply handle humans that want to attack. It check some properties
 * and than call a routine in MatchController
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class UseAttack extends UseItem {

    public UseAttack(String clientId) {
        super(clientId, new it.polimi.ingsw.cg_33.model.card.item.Attack());
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        if (!canUseItem(getItem()))
            return getOutput();
        getOutput().getDescriptions().add(getItem().toOneDescription());
        getMatchController().attackRoutine();
        getOutput().setPossibleActions(getMatch().getPossibleActions());
        removeItem(getItem());
        getToAllOutput().setDescription(getPlayer() + getItem().toAllDescription());
        Publisher.publish(getMatch().getId(), getToAllOutput());
        return getOutput();
    }

}
