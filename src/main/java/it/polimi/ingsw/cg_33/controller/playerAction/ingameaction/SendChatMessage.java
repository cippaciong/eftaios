package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;

/**
 * <p>
 * A SendChatMessage Action simply allow you to communicate with other players
 * in your match.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class SendChatMessage extends Action {

    private String message;

    public SendChatMessage(String clientId, String message) {
        super(clientId);
        this.message = message;
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        if (getMatch() != null)
            sendMessageRoutine();
        else
            getOutput().getDescriptions().add("non puoi mandare messaggi se non sei in una partita");
        return getOutput();
    }

    private void sendMessageRoutine() {
        getMatch().getChat().add(message);
        getToAllOutput().setCurrentPlayer(getPlayer().toString());
        getToAllOutput().setChatMsg(getPlayer() + "  -  " + message);
        getToAllOutput().setDescription("qualcuno ha scritto in chat, digita openChat per leggere il messaggio");
        Publisher.publish(getMatch().getId(), getToAllOutput());
        getOutput().getDescriptions().add("Messaggio inviato con successo");
    }

}
