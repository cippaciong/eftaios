package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;

import java.awt.Point;

/**
 * <p>
 * This action is a notification to all players that you are in a sector that
 * you choose like target.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class NoiseInAnySector extends Action {
    private String target;

    public NoiseInAnySector(String clientId, String target) {
        super(clientId);
        this.target = target;
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        // PREPROCESSING
        if (!super.canDoCurrentPlayer())
            return getOutput();
        else if (!getMatch().getPossibleActions().contains("noiseinanysector"))
            getOutput().getDescriptions().add("Non puoi fare rumore ora!");
        // ACTION
        else
            tryDoNoise();
        return getOutput();
    }

    private void tryDoNoise() {
        try {
            noiseInAnySectorRoutine();
        } catch (NumberFormatException e) {
            getOutput().getDescriptions().add("Devi inserire il target nel formato x-y dove x e y sono due interi");
            getOutput().setPossibleActions(getMatch().getPossibleActions());
        }
    }

    private void noiseInAnySectorRoutine() {
        int x = Integer.parseInt(target.split("-")[0]);
        int y = Integer.parseInt(target.split("-")[1]);
        String fakePosition = getMatch().getMap().getSector(new Point(x, y)).toString();
        getMatch().getPossibleActions().clear();
        getMatch().getPossibleActions().add("endturn");
        getOutput().setPossibleActions(getMatch().getPossibleActions());
        getOutput().getDescriptions().add("Speriamo che ci siano cascati");
        getToAllOutput().setDescription(getPlayer() + " ha fatto rumore in " + fakePosition);
        getToAllOutput().setNoiseSector(fakePosition);
        Publisher.publish(getMatch().getId(), getToAllOutput());
    }

}
