package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction.itemaction;

import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.model.card.item.Item;

/**
 * <p>
 * This class is a super class to all actions that need to handle item, it have a
 * method to remove item and one to check if requests are valid.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public abstract class InvolveItemAction extends Action {

    public InvolveItemAction(String clientId) {
        super(clientId);
    }

    protected void removeItem(Item item) {
        getPlayer().getItems().remove(item);
        getMatch().getDiscardedItems().add(item);
        for (Item i : getPlayer().getItems())
            getOutput().getHaveItems().add(i.toString());
        getOutput().getHaveItems().add("dummyItem");
    }

    protected boolean canDoItem(Item item) {
        if (!canDoCurrentPlayer())
            return false;
        else if (!getPlayer().getItems().contains(item)) {
            getOutput().getDescriptions().add("Non possiedi questo oggetto!");
            return false;
        }
        return true;
    }
}
