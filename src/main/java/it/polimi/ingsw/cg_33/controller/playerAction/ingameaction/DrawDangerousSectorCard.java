package it.polimi.ingsw.cg_33.controller.playerAction.ingameaction;

import it.polimi.ingsw.cg_33.communication.Output;
import it.polimi.ingsw.cg_33.communication.ToAllOutput;
import it.polimi.ingsw.cg_33.controller.playerAction.Action;
import it.polimi.ingsw.cg_33.gameengine.pub.Publisher;
import it.polimi.ingsw.cg_33.model.card.item.Item;
import it.polimi.ingsw.cg_33.model.card.sectorcard.DangerousSectorCard;

/**
 * <p>
 * This class manage drawing action. Does some check and then activate actions
 * linked with card you have drew.
 * </p>
 * 
 * @author Francesco Rotondo
 * @author Tommaso Sardelli
 *
 */
public class DrawDangerousSectorCard extends Action {

    public DrawDangerousSectorCard(String clientId) {
        super(clientId);
    }


    /**
     * execute method to implement command pattern
     */
    @Override
    public Output execute() {
        // PREPROCESSING
        if (!super.canDoCurrentPlayer())
            return getOutput();
        else if (!getMatch().getPossibleActions().contains("draw"))
            getOutput().getDescriptions().add("Non puoi pescare ora!");
        // ACTION
        else
            drawRoutine();
        return getOutput();
    }

    private void drawRoutine() {
        if (getMatch().getCardsDeck().getCards().isEmpty())
            getMatch().getCardsDeck().clone(getMatch().getDiscardedCards());
        DangerousSectorCard card = (DangerousSectorCard) getMatch().getCardsDeck().draw();
        getMatch().getDiscardedCards().add(card);
        getMatch().getPossibleActions().clear();
        getMatch().getPossibleActions().add(card.getLinkedPossibleAction());
        getOutput().getDescriptions().add(card.toOneDescription());
        itemCardRoutine(card);
        getOutput().setPossibleActions(getMatch().getPossibleActions());
    }

    private void checkAndShuffleItemsDeck() {
        if (getMatch().getItemsDeck().getCards().isEmpty()) {
            getMatch().getItemsDeck().clone(getMatch().getDiscardedItems());
            getMatch().getDiscardedItems().clear();
        }
    }

    private void drawItem() {
        try {
            Item item = (Item) getMatch().getItemsDeck().draw();
            getPlayer().addItem(item);
            getOutput().getDescriptions().add("Hai trovo l'oggetto: " + item.toString());
            for (Item i : getPlayer().getItems())
                getOutput().getHaveItems().add(i.toString());
            getOutput().getHaveItems().add("dummyItem");
            checkTooItem();
            notifyAllDrewItem();
        } catch (NullPointerException e) {
            getOutput().getDescriptions().add("Non ci sono più oggetti da pescare!");
        }
    }

    private void checkTooItem() {
        if (getPlayer().getItems().size() == 4) {
            managePossibleActions();
            getOutput().getDescriptions().add("Hai 4 oggetti, devi scartare o usare un oggetto!");
            getOutput().getDescriptions().add("Tuoi oggetti: " + getPlayer().getItems());
        }
    }

    private void managePossibleActions() {
        getMatch().getPossibleActionsBackUp().clear();
        getMatch().getPossibleActionsBackUp().addAll(getMatch().getPossibleActions());
        getMatch().getPossibleActions().clear();
        getMatch().getPossibleActions().add("discarditem");
    }

    private void notifyAllDrewItem() {
        ToAllOutput o = new ToAllOutput();
        o.setCurrentPlayer(getPlayer().toString());
        o.setDescription(getPlayer() + " ha pescato un oggetto. ");
        Publisher.publish(getMatch().getId(), o);
    }

    private void itemCardRoutine(DangerousSectorCard card) {
        if (card.getHasItem()) {
            checkAndShuffleItemsDeck();
            drawItem();
        }
    }

}
